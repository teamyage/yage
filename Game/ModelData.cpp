#include "stdafx.h"
#include "ModelData.h"
#include "ContentManager.h"

MeshPart::MeshPart()
{
	isAlpha = false;
	m_vertexShader = G->m_contentManager->load<VertexShader<VERTEXTYPE_MODEL>>("Shaders\\VertexShader.cso");
	m_pixelShader = G->m_contentManager->load<PixelShader>("Shaders\\PixelShader.cso");
	m_texture = G->m_contentManager->load<Texture>("Textures\\Dirt_Diffuse.DDS");
}

int MeshPart::Render()
{	
	m_vertexShader->Bind();
	m_pixelShader->Bind();
	//FIXME THIS SURELY IS WRONG probably 5
	m_texture->Bind(PIXELSHADER, 0);
	m_vertexBuffer->Bind();
	m_indexBuffer->Bind();
	D3D->devcon->DrawIndexed(m_indexBuffer->getNumIndices(), 0, 0);
	return m_indexBuffer->getNumIndices()/3;
}

void MeshPart::RenderZ()
{	
	m_vertexShader->Bind();
	m_vertexBuffer->Bind();
	m_indexBuffer->Bind();
	D3D->devcon->DrawIndexed(m_indexBuffer->getNumIndices(), 0, 0);
}

Mesh::Mesh()
{

}
