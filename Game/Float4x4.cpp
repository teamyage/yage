#include "stdafx.h"
#include "Float4x4.h"

Float4x4::Float4x4()
{
	_11 = _22 = _33 = _44 = 1.00f;
	_12 = _13 = _14 = 0.0f;
	_21 = _23 = _24 = 0.0f;
	_31 = _32 = _34 = 0.0f;
	_41 = _42 = _43 = 0.0f;
}

Float4x4::Float4x4(const XMFLOAT4X4& m)
{
	*static_cast<XMFLOAT4X4*>(this) = m;
}

Float4x4::Float4x4(CXMMATRIX m)
{
	XMStoreFloat4x4(this, m);
}

Float4x4& Float4x4::operator*=(const Float4x4& other)
{
	XMMATRIX result = this->ToSIMD() * other.ToSIMD();
	XMStoreFloat4x4(this, result);
	return *this;
}

Float4x4 Float4x4::operator*(const Float4x4& other) const
{
	XMMATRIX result = this->ToSIMD() * other.ToSIMD();
	return Float4x4(result);
}

Float3 Float4x4::Up() const
{
	return Float3(_21, _22, _23);
}

Float3 Float4x4::Down() const
{
	return Float3(-_21, -_22, -_23);
}

Float3 Float4x4::Left() const
{
	return Float3(-_11, -_12, -_13);
}

Float3 Float4x4::Right() const
{
	return Float3(_11, _12, _13);
}

Float3 Float4x4::Forward() const
{
	return Float3(_31, _32, _33);
}

Float3 Float4x4::Back() const
{
	return Float3(-_31, -_32, -_33);
}

Float3 Float4x4::Translation() const
{
	return Float3(_41, _42, _43);
}

void Float4x4::SetTranslation(const Float3& t)
{
	_41 = t.x;
	_42 = t.y;
	_43 = t.z;
}

void Float4x4::SetTranslation(const btVector3& t)
{
	_41 = t.getX();
	_42 = t.getY();
	_43 = t.getZ();
}

void Float4x4::SetXBasis(const Float3& x)
{
	_11 = x.x;
	_12 = x.y;
	_13 = x.z;
}

void Float4x4::SetYBasis(const Float3& y)
{
	_21 = y.x;
	_22 = y.y;
	_23 = y.z;
}

void Float4x4::SetZBasis(const Float3& z)
{
	_31 = z.x;
	_32 = z.y;
	_33 = z.z;
}

Float4x4 Float4x4::Transpose(const Float4x4& m)
{
	return XMMatrixTranspose(m.ToSIMD());
}

Float4x4 Float4x4::Invert(const Float4x4& m)
{
	XMVECTOR det;
	return XMMatrixInverse(&det, m.ToSIMD());
}

Float4x4 Float4x4::ScaleMatrix(float s)
{
	Float4x4 m;
	m._11 = m._22 = m._33 = s;
	return m;
}

Float4x4 Float4x4::ScaleMatrix(const Float3& s)
{
	Float4x4 m;
	m._11 = s.x;
	m._22 = s.y;
	m._33 = s.z;
	return m;
}

Float4x4 Float4x4::TranslationMatrix(const Float3& t)
{
	Float4x4 m;
	m.SetTranslation(t);
	return m;
}

XMMATRIX Float4x4::ToSIMD() const
{
	return XMLoadFloat4x4(this);
}

Float4x4 Float4x4::Identity()
{
	Float4x4 m;	
	return m;
}