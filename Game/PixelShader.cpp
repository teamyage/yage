#include "stdafx.h"
#include "PixelShader.h"

PixelShader::PixelShader(std::string filename) : BinaryLoader(filename)
{
	if(m_data)
	{
		BOOST_LOG_TRIVIAL(trace) << "Creating PixelShader for " << m_filename;
		HRESULT hr = D3D->dev->CreatePixelShader(m_data, m_dataLength, nullptr, &m_pixelShader);

		if (FAILED(hr)) 
			BOOST_LOG_TRIVIAL(error) << "Error while creating PixelShader: " << m_filename;
	}
}


PixelShader::~PixelShader()
{
	BOOST_LOG_TRIVIAL(trace) << "Releasing PixelShader for " << m_filename;
	SAFE_RELEASE(m_pixelShader);
}

void PixelShader::Bind()
{
	D3D->devcon->PSSetShader(m_pixelShader, 0, 0);
}

void PixelShader::Unbind()
{
	D3D->devcon->PSSetShader(NULL, 0, 0);
}

void PixelShader::Release()
{
	m_pixelShader->Release();
}