#pragma once
#include <string>
#include "PixelShader.h"
#include "VertexShader.h"
#include "Texture.h"

class Material 
{
public:
	Material(std::string materialName)
	{
		m_materialName = materialName;
		m_pixelShader = NULL;
		m_vertexShader = NULL;
		m_diffuse = NULL;
		m_normal = NULL;
		m_specular = NULL;
	}
	~Material();

	void Bind()
	{
		m_vertexShader->Bind();
		m_pixelShader->Bind();

		//m_diffuse->SetAsPSResource(0, 1);
		//m_normal->SetAsPSResource(0, 1);
		//m_specular->SetAsPSResource(0, 1);
	}

	std::string			m_materialName;
	std::string			m_vertexLayoutType;
	PixelShader*		m_pixelShader;
	IVertexShader*		m_vertexShader;
	Texture*			m_diffuse;
	Texture*			m_normal;
	Texture*			m_specular;
};

