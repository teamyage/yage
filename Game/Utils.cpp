#include "stdafx.h"
#include "Utils.h"

float Utils::random01()
{
	static boost::mt19937 generator((const uint32_t)time(0));
	static boost::uniform_01<boost::mt19937> dist(generator);
	return (float)dist();
}

DirectX::XMMATRIX Utils::convert(const btTransform& transform)
{
	btScalar data[16];
	transform.getOpenGLMatrix(data);	
	DirectX::XMMATRIX result(data);
	return DirectX::XMMatrixTranspose(result);
}

btVector3 Utils::convert(const DirectX::XMFLOAT3 &v)
{
	return btVector3(v.x,v.y,v.z);
}

Quaternion Utils::convert(const btQuaternion &q)
{
	return Float4(q.getX(),q.getY(),q.getZ(),q.getW());
}