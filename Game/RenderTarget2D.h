#pragma once

#include "SRV.h"
#include "RTV.h"
#include "UAV.h"

struct RenderTarget2D : public SRV, public RTV, public UAV
{
	RenderTarget2D();
	ID3D11Texture2D *m_texture;
	UINT m_width, m_height, m_numMipLevels, m_multiSamples, m_msQuality, m_arraySize;
	DXGI_FORMAT Format;
	bool AutoGenMipMaps;

	void Initialize(    UINT width,
		UINT height,
		DXGI_FORMAT format,
		UINT numMipLevels = 1,
		UINT multiSamples = 1,
		UINT msQuality = 0,
		bool autoGenMipMaps = false,
		bool createUAV = false,
		UINT arraySize = 1);

	void Release();
};