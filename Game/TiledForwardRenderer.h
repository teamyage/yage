#pragma once

#include "System.h"
#include "D3DSystem.h"
#include "CommonStates.h"
#include "DirectXColors.h"
#include "Renderable.h"
#include "ComputeShader.h"
#include "ConstantBuffer.h"
#include "ConstantBufferDefinitions.h"
#include "Camera.h"
#include "LightingSystem.h"
#include "RendererStates.h"
#include "WindowManager.h"
#include "ContentManager.h"
#include "VoxelTerrain.h"
#include "values.h"
#include "CSMRenderer.h"
#include "DepthStencilBuffer.h"
#include "RWBuffer.h"
#include "RTV.h"
#include "UAV.h"
#include "SRV.h"
#include "RenderTarget2D.h"

class TiledForwardRenderer : public System, public D3DSystem
{
public:
	TiledForwardRenderer();
	~TiledForwardRenderer();

	void RenderFrame(std::vector<IRenderable*> &renderables, std::vector<IRenderableSystem*> &renderableSystems);	
	void ToggleRenderOutputMode();

private:
	//Normal ViewPort
	D3D11_VIEWPORT m_normalViewPort;

	//States
	ID3D11BlendState *m_blendStates[BSLAST];
	ID3D11RasterizerState *m_rasterizerStates[RSLAST];
	ID3D11SamplerState *m_samplerStates[SSLAST];
	ID3D11DepthStencilState *m_depthStensilStates[DSLAST];

	//Tiled Lighting Shader
	ComputeShader *m_computeShader;	
	ComputeShader *m_postProcessingShader;

	//Result of Z-Prepass
	DepthStencilBuffer *m_depthBuffer;

	//Results of Tiled Lighting Compute Shader
	RWBuffer *m_numLightsThisTile;
	RWBuffer *m_lightsThisTile;

	ConstantBuffer<PerFrameConstantBuffer> *m_perFrameConstantBuffer;
	ConstantBuffer<PerRenderableConstantBuffer> *m_perRenderableConstantBuffer;	
	RenderSettingsConstantBuffer m_renderSettings;
	ConstantBuffer<RenderSettingsConstantBuffer> *m_renderSettingsConstantBuffer;	

	RenderTarget2D *preProcessingBuffer;

	CSMRenderer *m_shadowRenderer;
};

