#pragma once

#include "D3DSystem.h"
#include "DSV.h"

struct RTV : public D3DSystem
{
	RTV();
	static void BindRTV(UINT NumViews, ID3D11RenderTargetView *const *ppRenderTargetViews, DSV *dsv);
	static void BindRTV(ID3D11RenderTargetView *const *ppRenderTargetViews, DSV *dsv);
	void BindRTV(DSV *dsv);
	static void BindNULLRTV(UINT NumViews);
	void CreateRTV(ID3D11Resource *pResource, const D3D11_RENDER_TARGET_VIEW_DESC *pDesc);
//private:
	ID3D11RenderTargetView *m_RTV;
};