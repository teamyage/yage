#pragma once
#include "Renderable.h"
#include "D3DSystem.h"
#include "CellId.h"
#include "gtc/noise.hpp"

using namespace DirectX;

#define CELLWIDTH 64
#define CELLWIDTHP1 65

enum iorder
{
	XP=0,
	XN=1,
	YP=2,
	YN=3,
	ZP=4,
	ZN=5
};

class Cell : public D3DSystem
{
public:
	Cell();
	~Cell();

	bool Generate();
	void UpdateConstantBuffer();
	void Render();
	PerCellConstantBuffer& GetConstantBuffer();
	void SetCellId(CellId cid);
	inline bool hasDensityChange(BYTE A,BYTE B);
	CellId getId();
	void PrepareForRendering();
	int polycount;

private:	
	void AddQuad(unsigned int x, unsigned int y, unsigned int z, iorder direction);
	XMVECTOR cellWidth;
	CellId m_cid;
	PerCellConstantBuffer m_perCellConstantBuffer;
	VertexBuffer<VertexPositionNormal> *m_vertexBuffer;
	IndexBuffer<USHORT> *m_indexBuffer;
	std::vector<VertexPositionNormal> verts;
	std::vector<USHORT> indices;
	BYTE ***data;
};
