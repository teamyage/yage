#pragma once
#include "system.h"
#include "StructuredBuffer.h"
#include "ConstantBuffer.h"
#include "ConstantBufferDefinitions.h"
#include "Lights.h"
#include "Processable.h"
#include "Timer.h"
#include "Utils.h"

using namespace DirectX;

class LightingSystem : public System, public IProcessableSystem
{
public:
	LightingSystem();
	~LightingSystem();

	void AddPointLight(Float3 position, float radius, Float3 color, float intensity);
	void AddDirectionalLight(Float3 rayDirection, Float3 color, float intensity);
	void Process();
	DirectionalLight& GetShadowCastingDirectionalLight();
	void UpdateDirectionalLightDirection(unsigned int id, Float3 rayDirection);
	void UpdateDirectionalLightColor(unsigned int id, Float3 color);
	void UpdateDirectionalLightIntensity(unsigned int id, float intensity);
	void UpdateDirectionalLightDirectionAndColor(unsigned int id, Float3 rayDirection, Float3 color);
	void UpdateDirectionalLightDirectionAndIntensity(unsigned int id, Float3 rayDirection, float intensity);
	void UpdateDirectionalLightColorAndIntensity(unsigned int id, Float3 color, float intensity);
	void UpdateDirectionalLight(unsigned int id, Float3 rayDirection, Float3 color, float intensity);

	StructuredBuffer<PointLight> *m_pointLightsStructuredBuffer;
	StructuredBuffer<DirectionalLight> *m_directionalLightsStructuredBuffer;
	ConstantBuffer<LightingConstantBuffer> *m_lightingCB;
	
private:	
	LightingConstantBuffer m_lightingConstantBufferData;
	std::vector<PointLight> m_pointLights;
	std::vector<DirectionalLight> m_directionalLights;
};

