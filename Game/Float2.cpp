#include "stdafx.h"

#include "Float2.h"

Float2::Float2()
{
	x = y = 0.0f;
}

Float2::Float2(float x_)
{
	x = y = x_;
}

Float2::Float2(float x_, float y_)
{
	x = x_;
	y = y_;
}

Float2::Float2(const XMFLOAT2& xy)
{
	x = xy.x;
	y = xy.y;
}

Float2::Float2(FXMVECTOR xy)
{
	XMStoreFloat2(this, xy);
}

Float2& Float2::operator+=(const Float2& other)
{
	x += other.x;
	y += other.y;
	return *this;
}

Float2 Float2::operator+(const Float2& other) const
{
	Float2 result;
	result.x = x + other.x;
	result.y = y + other.y;
	return result;
}

Float2& Float2::operator-=(const Float2& other)
{
	x -= other.x;
	y -= other.y;
	return *this;
}

Float2 Float2::operator-(const Float2& other) const
{
	Float2 result;
	result.x = x - other.x;
	result.y = y - other.y;
	return result;
}

Float2& Float2::operator*=(const Float2& other)
{
	x *= other.x;
	y *= other.y;
	return *this;
}

Float2 Float2::operator*(const Float2& other) const
{
	Float2 result;
	result.x = x * other.x;
	result.y = y * other.y;
	return result;
}

Float2& Float2::operator/=(const Float2& other)
{
	x /= other.x;
	y /= other.y;
	return *this;
}

Float2 Float2::operator/(const Float2& other) const
{
	Float2 result;
	result.x = x / other.x;
	result.y = y / other.y;
	return result;
}

bool Float2::operator==(const Float2& other) const
{
	return x == other.x && y == other.y;
}

bool Float2::operator!=(const Float2& other) const
{
	return x != other.x || y != other.y;
}

Float2 Float2::operator-() const
{
	Float2 result;
	result.x = -x;
	result.y = -y;

	return result;
}

XMVECTOR Float2::ToSIMD() const
{
	return XMLoadFloat2(this);
}

Float2 Float2::Clamp(const Float2& val, const Float2& min, const Float2& max)
{
	Float2 retVal;
	retVal.x = glm::clamp(val.x, min.x, max.x);
	retVal.y = glm::clamp(val.y, min.y, max.y);
	return retVal;
}

float Float2::Length(const Float2& val)
{
	return std::sqrtf(val.x * val.x + val.y * val.y);
}