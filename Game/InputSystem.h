#pragma once

#include "System.h"
#include "Camera.h"
#include "Timer.h"
#include "Float2.h"
#include "Processable.h"

class InputSystem : public System, public IProcessableSystem
{
public:
	InputSystem();
	~InputSystem();

	void SetupRawInput();
	bool IsKeyDown(int key);
	bool IsKeyDownThisFrame(int key);
	void Process();
	void RecenterMouseCursor();
	void ShowMouseCursor();
	void HideMouseCursor();
	void ToggleMouseCursor();

	LRESULT WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	
private:
	bool m_keysDown[256];
	bool m_keysDownThisFrame[256];
	bool m_cursorShowing;
};

