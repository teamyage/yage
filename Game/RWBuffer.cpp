#include "stdafx.h"
#include "RWBuffer.h"

RWBuffer::RWBuffer() : Size(0), Stride(0), NumElements(0), Format(DXGI_FORMAT_UNKNOWN), RawBuffer(FALSE)
{

}

void RWBuffer::Initialize(DXGI_FORMAT format, UINT stride, UINT numElements, BOOL rawBuffer)
{
	Format = format;
	Size = stride * numElements;
	Stride = stride;
	NumElements = numElements;
	RawBuffer = rawBuffer;

	if(rawBuffer)
	{
		_ASSERT(stride == 4);
		Format = DXGI_FORMAT_R32_TYPELESS;
	}

	D3D11_BUFFER_DESC bufferDesc;
	bufferDesc.ByteWidth = stride * numElements;
	bufferDesc.Usage = D3D11_USAGE_DEFAULT;
	bufferDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
	bufferDesc.CPUAccessFlags = 0;
	bufferDesc.MiscFlags = rawBuffer ? D3D11_RESOURCE_MISC_BUFFER_ALLOW_RAW_VIEWS : 0;
	bufferDesc.StructureByteStride = 0;
	D3D->dev->CreateBuffer(&bufferDesc, NULL, &Buffer);

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.Format = Format;

	if(rawBuffer)
	{
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
		srvDesc.BufferEx.FirstElement = 0;
		srvDesc.BufferEx.NumElements = numElements;
		srvDesc.BufferEx.Flags = D3D11_BUFFEREX_SRV_FLAG_RAW;
	}
	else
	{
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
		srvDesc.Buffer.ElementOffset = 0;
		srvDesc.Buffer.ElementWidth = numElements;
	}

	CreateSRV(Buffer, &srvDesc);

	D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
	uavDesc.Format = format;
	uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
	uavDesc.Buffer.FirstElement = 0;
	uavDesc.Buffer.NumElements = numElements;
	uavDesc.Buffer.Flags = rawBuffer ? D3D11_BUFFER_UAV_FLAG_RAW : 0;

	CreateUAV(Buffer, &uavDesc);
}