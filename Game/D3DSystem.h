#pragma once

#include "D3DGlobals.h"

class D3DSystem
{
public:	
	D3DSystem();
	~D3DSystem();
	static D3DGlobals *D3D;
};