#pragma once

class InputSystem;
class WindowManager;
class Timer;
class TiledForwardRenderer;
class ContentManager;
class MaterialLibrary;
class FirstPersonCamera;
class LightingSystem;
class PhysicsSystem;

class Globals
{
public:
	Globals();
	~Globals();

	InputSystem *m_inputSystem;
	WindowManager *m_windowManager;
	TiledForwardRenderer *m_renderer;
	ContentManager *m_contentManager;
	Timer *m_timer; 
	MaterialLibrary *m_materialsLibrary;
	FirstPersonCamera *m_camera;
	LightingSystem *m_lightingSystem;
	PhysicsSystem *m_physicsSystem;

	HINSTANCE applicationHandle;
	int nCmdShow;
};

