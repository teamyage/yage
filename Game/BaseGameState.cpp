#include "stdafx.h"
#include "BaseGameState.h"
#include "VertexBuffer.h"

BaseGameState::BaseGameState()
{
	BOOST_LOG_TRIVIAL(trace) << "Construction of BaseGameState";
}

BaseGameState::~BaseGameState()
{
	BOOST_LOG_TRIVIAL(trace) << "Destruction of BaseGameState";
}

void BaseGameState::ProcessSystems()
{
	for(IProcessableSystem* s : m_processableSystems)
		s->Process();
}