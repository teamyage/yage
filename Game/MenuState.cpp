#include "stdafx.h"
#include "MenuState.h"

MenuState::MenuState()
{
	BOOST_LOG_TRIVIAL(trace) << "Construction of Menu";
}

MenuState::~MenuState()
{
	BOOST_LOG_TRIVIAL(trace) << "Destruction of Menu";
}

void MenuState::Initialize()
{	
	//m_renderables.push_back(G->m_contentManager->load<Model>("Models\\teapot.cmo"));
	G->m_materialsLibrary = G->m_contentManager->load<MaterialLibrary>("Data\\Material.xml");

	G->m_lightingSystem = new LightingSystem();
	G->m_physicsSystem = new PhysicsSystem();
	m_processableSystems.push_back(G->m_lightingSystem);
	m_processableSystems.push_back(G->m_inputSystem);
	m_processableSystems.push_back(G->m_physicsSystem);
	
	m_renderables.push_back(new Axis());	

	m_renderableSystems.push_back(new VoxelTerrain());
	//m_renderableSystems.push_back(new HeightMapTerrain("HeightMaps\\heightmap.bmp", Float2(128)));	
}

void MenuState::Render()
{
	G->m_renderer->RenderFrame(m_renderables, m_renderableSystems);
}

void MenuState::Process()
{

}

void MenuState::ProcessInput()
{
	XMVECTOR position = G->m_camera->GetPosition().ToSIMD();
	XMVECTOR lookdirection = G->m_camera->GetLookAt().ToSIMD();
	XMVECTOR right = XMVector3Cross(lookdirection, Float3(0.0f, 1.0f, 0.0f).ToSIMD());
	XMVECTOR newPosition = XMVectorZero();

	//Looking around with arrows
	if(G->m_inputSystem->IsKeyDown(37))
		G->m_camera->SetLookDirectionDelta((float)G->m_timer->getLastFrameLength()*-1.0f, 0.0f, 0.0f);
	if(G->m_inputSystem->IsKeyDown(39))
		G->m_camera->SetLookDirectionDelta((float)G->m_timer->getLastFrameLength()*1.0f, 0.0f, 0.0f);
	if(G->m_inputSystem->IsKeyDown(38))
		G->m_camera->SetLookDirectionDelta(0.0f, (float)G->m_timer->getLastFrameLength()*-1.0f, 0.0f);
	if(G->m_inputSystem->IsKeyDown(40))
		G->m_camera->SetLookDirectionDelta(0.0f, (float)G->m_timer->getLastFrameLength()*1.0f, 0.0f);

	//Moving around with wasd
	if(G->m_inputSystem->IsKeyDown((int)'W'))
		newPosition += lookdirection;
	if(G->m_inputSystem->IsKeyDown((int)'S'))
		newPosition -= lookdirection;
	if(G->m_inputSystem->IsKeyDown((int)'A'))
		newPosition += right;
	if(G->m_inputSystem->IsKeyDown((int)'D'))
		newPosition -= right;

	if(G->m_inputSystem->IsKeyDownThisFrame((int)'B'))
		m_renderables.push_back(new Cube());
	
	G->m_camera->SetPosition(position + (float)G->m_timer->getLastFrameLength()*50.0f*XMVector3Normalize(newPosition));
	
	//Exit!
	if(G->m_inputSystem->IsKeyDown(27))
		PostQuitMessage(0);	

	//Extra controls
	if(G->m_inputSystem->IsKeyDownThisFrame((int)'L'))
		G->m_lightingSystem->AddPointLight(position + lookdirection*10, 10.0f, Float3(Utils::random01(),Utils::random01(),Utils::random01()), 1.0f);	
	if(G->m_inputSystem->IsKeyDownThisFrame((int)'O'))
		G->m_renderer->ToggleRenderOutputMode();
}