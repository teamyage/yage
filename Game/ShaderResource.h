#pragma once

#include "stdafx.h"

struct ShaderResource
{
	UINT m_startSlot;
	UINT m_numViews;
	ID3D11ShaderResourceView *m_srv;
};