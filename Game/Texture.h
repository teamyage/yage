#pragma once
#include "BaseContent.h"
#include "D3DSystem.h"
#include "WICTextureLoader.h"
#include "ShaderTypes.h"

class Texture : public BaseContent, public D3DSystem
{
public:
	Texture(std::string filename);
	~Texture();

	void Bind(ShaderTypes shaderType, UINT startslot);
	void Release();

private:
	ID3D11ShaderResourceView *m_srv;
	std::string m_filename;
};

