#include "stdafx.h"
#include "Float3x3.h"

Float3x3::Float3x3()
{
	_11 = _22 = _33 = 1.00f;
	_12 = _13 = 0.0f;
	_21 = _23 = 0.0f;
	_31 = _32 = 0.0f;
}

Float3x3::Float3x3(const XMFLOAT3X3& m)
{
	*static_cast<XMFLOAT3X3*>(this) = m;
}

Float3x3::Float3x3(CXMMATRIX m)
{
	XMStoreFloat3x3(this, m);
}

Float3x3& Float3x3::operator*=(const Float3x3& other)
{
	XMMATRIX result = this->ToSIMD() * other.ToSIMD();
	XMStoreFloat3x3(this, result);
	return *this;
}

Float3x3 Float3x3::operator*(const Float3x3& other) const
{
	XMMATRIX result = this->ToSIMD() * other.ToSIMD();
	return Float3x3(result);
}

Float3 Float3x3::Up() const
{
	return Float3(_21, _22, _23);
}

Float3 Float3x3::Down() const
{
	return Float3(-_21, -_22, -_23);
}

Float3 Float3x3::Left() const
{
	return Float3(-_11, -_12, -_13);
}

Float3 Float3x3::Right() const
{
	return Float3(_11, _12, _13);
}

Float3 Float3x3::Forward() const
{
	return Float3(_31, _32, _33);
}

Float3 Float3x3::Back() const
{
	return Float3(-_31, -_32, -_33);
}

void Float3x3::SetXBasis(const Float3& x)
{
	_11 = x.x;
	_12 = x.y;
	_13 = x.z;
}

void Float3x3::SetYBasis(const Float3& y)
{
	_21 = y.x;
	_22 = y.y;
	_23 = y.z;
}

void Float3x3::SetZBasis(const Float3& z)
{
	_31 = z.x;
	_32 = z.y;
	_33 = z.z;
}

Float3x3 Float3x3::Transpose(const Float3x3& m)
{
	return XMMatrixTranspose(m.ToSIMD());
}

Float3x3 Float3x3::Invert(const Float3x3& m)
{
	XMVECTOR det;
	return XMMatrixInverse(&det, m.ToSIMD());
}

Float3x3 Float3x3::ScaleMatrix(float s)
{
	Float3x3 m;
	m._11 = m._22 = m._33 = s;
	return m;
}

Float3x3 Float3x3::ScaleMatrix(const Float3& s)
{
	Float3x3 m;
	m._11 = s.x;
	m._22 = s.y;
	m._33 = s.z;
	return m;
}

XMMATRIX Float3x3::ToSIMD() const
{
	return XMLoadFloat3x3(this);
}
