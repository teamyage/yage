#pragma once

#include "xmlloader.h"
#include <boost/foreach.hpp>
#include "VertexShader.h"
#include "PixelShader.h"
#include "Texture.h"
#include "Material.h"
#include "System.h"

class MaterialLibrary : public XMLLoader, public System
{
public:
	MaterialLibrary(std::string filename);
	~MaterialLibrary();

	boost::unordered_map<std::string, Material*> m_materials;
};

