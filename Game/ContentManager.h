#pragma once

#include "system.h"
#include "BaseContent.h"

class ContentManager : public System
{
public:
	ContentManager();
	~ContentManager();

	template <class T>
	T* load(std::string filename)
	{
		auto asset = content.find(filename);

		T* result = NULL;

		if(asset == content.end())
		{
			content[filename] = result = new T(filename);
		}
		else
		{
			result = (T*)asset->second;
			BOOST_LOG_TRIVIAL(trace) << "Previously requested asset instanced";
		}

		return result;
	}

private:
	boost::unordered_map<std::string, BaseContent *> content;
};

