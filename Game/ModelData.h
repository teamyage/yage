#pragma once

#include "Renderable.h"

#define VERTEXTYPE_MODEL DirectX::VertexPositionNormalTangentColorTexture
#define INDEXTYPE_MODEL USHORT
#define INDEXFORMAT_MODEL DXGI_FORMAT_R16_UINT

class MeshPart : public Renderable<VERTEXTYPE_MODEL, INDEXTYPE_MODEL>
{
public:
	MeshPart();

	int Render();
	void RenderZ();

	bool isAlpha;
};


// A mesh consists of one or more model parts
class Mesh
{
public:
	Mesh();

	DirectX::BoundingSphere             boundingSphere;
	DirectX::BoundingBox                boundingBox;
	std::vector<MeshPart*>				meshParts;
	std::wstring						name;
	bool								ccw;
	bool								pmalpha;
};

struct MeshMaterial
{
	DirectX::XMFLOAT4   Ambient;
	DirectX::XMFLOAT4   Diffuse;
	DirectX::XMFLOAT4   Specular;
	float               SpecularPower;
	DirectX::XMFLOAT4   Emissive;
	DirectX::XMFLOAT4X4 UVTransform;
};

const uint32_t MAX_TEXTURE = 8;

struct SubMesh
{
	UINT MaterialIndex;
	UINT IndexBufferIndex;
	UINT VertexBufferIndex;
	UINT StartIndex;
	UINT PrimCount;
};

const uint32_t NUM_BONE_INFLUENCES = 4;

struct SkinningVertex
{
	UINT boneIndex[NUM_BONE_INFLUENCES];
	float boneWeight[NUM_BONE_INFLUENCES];
};

struct MeshExtents
{
	float CenterX, CenterY, CenterZ;
	float Radius;

	float MinX, MinY, MinZ;
	float MaxX, MaxY, MaxZ;
};

struct Bone
{
	INT ParentIndex;
	DirectX::XMFLOAT4X4 InvBindPos;
	DirectX::XMFLOAT4X4 BindPos;
	DirectX::XMFLOAT4X4 LocalTransform;
};

struct Clip
{
	float StartTime;
	float EndTime;
	UINT  keys;
};

struct Keyframe
{
	UINT BoneIndex;
	float Time;
	DirectX::XMFLOAT4X4 Transform;
};

struct MaterialRecordCMO
{
	const MeshMaterial*					pMaterial;
	std::wstring                    name;
	std::wstring                    pixelShader;
	std::wstring                    texture[MAX_TEXTURE];
	ID3D11InputLayout*				il;
};