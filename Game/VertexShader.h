#pragma once
#include "BinaryLoader.h"
#include "ConstantBuffer.h"

using namespace DirectX;

class IVertexShader : public D3DSystem
{
public:
	void Bind()
	{ 
		D3D->devcon->VSSetShader(m_vertexShader, 0, 0); 
		D3D->devcon->IASetInputLayout(m_inputLayout);
	}
	
	void Release()
	{ 
		SAFE_RELEASE(m_vertexShader);
		SAFE_RELEASE(m_inputLayout);
	}

protected:
	ID3D11InputLayout *m_inputLayout;
	ID3D11VertexShader *m_vertexShader;
};

template <class T>
class VertexShader : public IVertexShader, public BinaryLoader
{
public:
	VertexShader(std::string filename) : BinaryLoader(filename)
	{
		m_inputLayout = NULL;
		m_vertexShader = NULL;

		if(m_data)
		{
			BOOST_LOG_TRIVIAL(trace) << "Creating VertexShader for " << m_filename;
			HRESULT createVertexShaderResult = D3D->dev->CreateVertexShader(m_data, m_dataLength, nullptr, &m_vertexShader);

			if (FAILED(createVertexShaderResult)) 
				BOOST_LOG_TRIVIAL(error) << "Error while creating VertexShader: " << m_filename;
			else
			{
				HRESULT createInputLayoutResult = D3D->dev->CreateInputLayout(T::InputElements,
					T::InputElementCount,
					m_data, 
					m_dataLength,
					&m_inputLayout
				);

				if(FAILED(createInputLayoutResult))
					BOOST_LOG_TRIVIAL(error) << "Error while creating VertexShader Layout: " << m_filename;
			}
		}
	}

	~VertexShader()
	{
		BOOST_LOG_TRIVIAL(trace) << "Releasing VertexShader for " << m_filename;
		SAFE_RELEASE(m_vertexShader);
	}		

};

