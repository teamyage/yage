#include "stdafx.h"
#include "VoxelTerrain.h"

VoxelTerrain::VoxelTerrain()
{	
	m_pixelShader = NULL;
	m_vertexShader = NULL;

	const int xlimit = 6;
	const int ylimit = 6;
	const int zlimit = 6;

	BOOST_LOG_TRIVIAL(trace) << "Construction of Terrain";

	m_perRenderableConstantBuffer = new ConstantBuffer<PerRenderableConstantBuffer>();
	m_pixelShader = G->m_contentManager->load<PixelShader>("Shaders\\TerrainPixelShader.cso");
	m_vertexShader = G->m_contentManager->load<VertexShader<VertexPositionNormal>>("Shaders\\TerrainVertexShader.cso");
	m_diffuse = G->m_contentManager->load<Texture>("Textures\\Dirt_Diffuse.dds");
	
	BOOST_LOG_TRIVIAL(trace) << "Requesting Terrain Generation...";
	for(int x=0;x<xlimit;x++)
		for(int y=0;y<ylimit;y++)
			for(int z=0;z<zlimit;z++)
				m_cellFactory.Request(CellId(x,y,z));

	int totalCells = xlimit*ylimit*zlimit;
}


VoxelTerrain::~VoxelTerrain()
{	
	BOOST_LOG_TRIVIAL(trace) << "Destruction of Terrain";
	SAFE_RELEASE(m_pixelShader);
	SAFE_RELEASE(m_vertexShader);
	SAFE_DELETE(m_perRenderableConstantBuffer);

	BOOST_FOREACH(Scene::value_type const& pair, m_scene)
	{
		delete pair.second;
	}
}

int VoxelTerrain::Render()
{	
	D3D->devcon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	m_perRenderableConstantBuffer->BindCB(VERTEXSHADER, 1);
	G->m_lightingSystem->m_lightingCB->BindCB(PIXELSHADER, 3);

	m_diffuse->Bind(PIXELSHADER, 5);

	m_pixelShader->Bind();
	m_vertexShader->Bind();

	int polycount = 0;
	BOOST_FOREACH(Scene::value_type const& pair, m_scene)
	{
		Cell* cell = pair.second;
		polycount+=cell->polycount;

		cell->UpdateConstantBuffer();
		m_perRenderableConstantBuffer->SetData(cell->GetConstantBuffer());
		cell->Render();
	}

	Cell* c;
	if(m_cellFactory.m_completedRequests.pop(c))
	{
		c->PrepareForRendering();
		AddCellToScene(c);
	}

	return polycount;
}

void VoxelTerrain::RenderZ()
{	
	D3D->devcon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	m_perRenderableConstantBuffer->BindCB(VERTEXSHADER, 1);

	m_pixelShader->Unbind();
	m_vertexShader->Bind();

	BOOST_FOREACH(Scene::value_type const& pair, m_scene)
	{
		Cell* cell = pair.second;

		cell->UpdateConstantBuffer();
		m_perRenderableConstantBuffer->SetData(cell->GetConstantBuffer());
		cell->Render();
	}
}


inline void VoxelTerrain::AddCellToScene(Cell* c)
{
	m_scene[c->getId()] = c;
}