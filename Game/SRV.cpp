#include "stdafx.h"
#include "SRV.h"

SRV::SRV() : m_SRV(NULL) {}

void SRV::BindNULLSRV(ShaderTypes shaderType, UINT StartSlot=0, UINT NumViews=1)
{
	ID3D11ShaderResourceView** nullsrvs = new ID3D11ShaderResourceView*[NumViews];

	for(UINT i=0;i<NumViews;i++)
		nullsrvs[i] = NULL;

	switch(shaderType){
	case VERTEXSHADER:
		D3D->devcon->VSSetShaderResources(StartSlot, NumViews, nullsrvs);
		break;
	case PIXELSHADER:
		D3D->devcon->PSSetShaderResources(StartSlot, NumViews, nullsrvs);
		break;
	case GEOMETRYSHADER:
		D3D->devcon->GSSetShaderResources(StartSlot, NumViews, nullsrvs);
		break;
	case HULLSHADER:
		D3D->devcon->HSSetShaderResources(StartSlot, NumViews, nullsrvs);
		break;
	case COMPUTESHADER:
		D3D->devcon->CSSetShaderResources(StartSlot, NumViews, nullsrvs);
		break;
	case DOMAINSHADER:
		D3D->devcon->DSSetShaderResources(StartSlot, NumViews, nullsrvs);
		break;
	default:
		BOOST_LOG_TRIVIAL(error) << "Attempted Setting of NULLSRV on non-existant ShaderType";
	}	

	for(UINT i=0;i<NumViews;i++)
		delete nullsrvs[i];

	delete nullsrvs;
}

void SRV::BindSRV(ShaderTypes shaderType, UINT slot)
{
	switch(shaderType){
	case VERTEXSHADER:
		D3D->devcon->VSSetShaderResources(slot, 1, &m_SRV);
		break;
	case PIXELSHADER:
		D3D->devcon->PSSetShaderResources(slot, 1, &m_SRV);
		break;
	case GEOMETRYSHADER:
		D3D->devcon->GSSetShaderResources(slot, 1, &m_SRV);
		break;
	case HULLSHADER:
		D3D->devcon->HSSetShaderResources(slot, 1, &m_SRV);
		break;
	case COMPUTESHADER:
		D3D->devcon->CSSetShaderResources(slot, 1, &m_SRV);
		break;
	case DOMAINSHADER:
		D3D->devcon->DSSetShaderResources(slot, 1, &m_SRV);
		break;
	default:
		BOOST_LOG_TRIVIAL(error) << "Attempted Binding of SRV on non-existant ShaderType";
	}	
}

void SRV::CreateSRV(ID3D11Resource *resource, const D3D11_SHADER_RESOURCE_VIEW_DESC *pDesc)
{
	if(FAILED(D3D->dev->CreateShaderResourceView(resource, pDesc, &m_SRV)))
		BOOST_LOG_TRIVIAL(error) << "Error while creating SRV";
}