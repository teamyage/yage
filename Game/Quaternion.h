#pragma once

#include "stdafx.h"
#include "Float3.h"
#include "Float3x3.h"
#include "Float4x4.h"

using namespace DirectX;

struct Float3;
struct Float3x3;
struct Float4x4;

struct Quaternion : public XMFLOAT4
{
	Quaternion();
	Quaternion(float x, float y, float z, float w);
	Quaternion(const Float3& axis, float angle);
	Quaternion(const Float3x3& m);
	Quaternion(const XMFLOAT4& q);
	Quaternion(FXMVECTOR q);

	Quaternion& operator*=(const Quaternion& other);
	Quaternion operator*(const Quaternion& other) const;

	bool operator==(const Quaternion& other) const;
	bool operator!=(const Quaternion& other) const;

	Float3x3 ToFloat3x3() const;
	Float4x4 ToFloat4x4() const;

	static Quaternion Identity();
	static Quaternion Invert(const Quaternion& q);
	static Quaternion FromAxisAngle(const Float3& axis, float angle);
	static Quaternion Normalize(const Quaternion& q);
	static Float3x3 ToFloat3x3(const Quaternion& q);
	static Float4x4 ToFloat4x4(const Quaternion& q);

	XMVECTOR ToSIMD() const;
};