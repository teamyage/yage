#pragma once

#include "SRV.h"
#include "UAV.h"

struct RWBuffer : public SRV, public UAV
{
	ID3D11Buffer *Buffer;
	UINT Size;
	UINT Stride;
	UINT NumElements;
	BOOL RawBuffer;
	DXGI_FORMAT Format;

	RWBuffer();

	void Initialize(DXGI_FORMAT format, UINT stride, UINT numElements, BOOL rawBuffer = FALSE);
};