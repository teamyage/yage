#pragma once

#include "basecontent.h"
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

class XMLLoader : public BaseContent
{
public:
	XMLLoader(std::string filename);
	virtual ~XMLLoader();

protected:
	boost::property_tree::ptree m_propertyTree;
};

