#include "stdafx.h"
#include "TiledForwardRenderer.h"
#include "Camera.h"

using namespace DirectX;

TiledForwardRenderer::TiledForwardRenderer()
{
	m_perFrameConstantBuffer = NULL;
	m_perRenderableConstantBuffer = NULL;

	BOOST_LOG_TRIVIAL(trace) << "Construction of D3D11 Renderer";

	// create a struct to hold information about the swap chain
	DXGI_SWAP_CHAIN_DESC scd;

	// clear out the struct for use
	ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));

	// fill the swap chain description struct
	scd.BufferCount = 1;                                   // one back buffer
	scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;    // use 32-bit color
	scd.BufferDesc.Width = G->m_windowManager->getWidth();                   // set the back buffer width
	scd.BufferDesc.Height = G->m_windowManager->getHeight();                 // set the back buffer height
	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_UNORDERED_ACCESS ;     // how swap chain is to be used
	scd.OutputWindow = G->m_windowManager->getWindowHandle();                               // the window to be used
	scd.SampleDesc.Count = 1;                              // how many multisamples
	scd.Windowed = TRUE;                                   // windowed/full-screen mode
	scd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;    // allow full-screen switching

	// create a device, device context and swap chain using the information in the scd struct
	HRESULT createDeviceAndSwapChainResult = D3D11CreateDeviceAndSwapChain(NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
		D3D11_CREATE_DEVICE_DEBUG,
		NULL,
		NULL,
		D3D11_SDK_VERSION,
		&scd,
		&D3D->swapchain,
		&D3D->dev,
		NULL,
		&D3D->devcon);

	if (FAILED(createDeviceAndSwapChainResult)) 
		BOOST_LOG_TRIVIAL(error) << "Error while creating the DX11 Device and swapchain";

	m_depthBuffer = new DepthStencilBuffer();
	m_depthBuffer->Initialize(G->m_windowManager->getWidth(), G->m_windowManager->getHeight(),DXGI_FORMAT_D32_FLOAT,true,4);

	// get the address of the back buffer
	ID3D11Texture2D *pBackBuffer;

	if (FAILED(D3D->swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer))) 
		BOOST_LOG_TRIVIAL(error) << "Error while setting back buffer";

	if (FAILED(D3D->dev->CreateRenderTargetView(pBackBuffer, NULL, &D3D->backbuffer))) 
		BOOST_LOG_TRIVIAL(error) << "Error while setting back buffer";

	if (FAILED(D3D->dev->CreateUnorderedAccessView(pBackBuffer, NULL, &D3D->backbufferuav)))
		BOOST_LOG_TRIVIAL(error) << "Error while creating UAV of back buffer";

	pBackBuffer->Release();

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Setup ViewPort
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	{
		ZeroMemory(&m_normalViewPort, sizeof(D3D11_VIEWPORT));

		m_normalViewPort.TopLeftX = 0.0f;
		m_normalViewPort.TopLeftY = 0.0f;
		m_normalViewPort.Width = (FLOAT)G->m_windowManager->getWidth();
		m_normalViewPort.Height = (FLOAT)G->m_windowManager->getHeight();
		m_normalViewPort.MaxDepth = 1;
		m_normalViewPort.MinDepth = 0;
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Setup States
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	{
		CommonStates *commonStates = new CommonStates(D3D->dev);		

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Blend States
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		{
			m_blendStates[BSOpaque]				= commonStates->Opaque();
			m_blendStates[BSAlphaBlend]			= commonStates->AlphaBlend();
			m_blendStates[BSAdditive]			= commonStates->Additive();
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Rasterizer States
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		{
			m_rasterizerStates[RSCullNone]		= commonStates->CullNone();
			m_rasterizerStates[RSCullCW]		= commonStates->CullClockwise();
			m_rasterizerStates[RSCullCCW]		= commonStates->CullCounterClockwise();
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Sampler States
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		{
			m_samplerStates[SSAnistropicClamp]	= commonStates->AnisotropicClamp();
			m_samplerStates[SSAnistropicWrap]	= commonStates->AnisotropicWrap();
			m_samplerStates[SSLinearClamp]		= commonStates->LinearClamp();
			m_samplerStates[SSLinearWrap]		= commonStates->LinearWrap();
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// Depth States
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		{
			m_depthStensilStates[DSDefault]		= commonStates->DepthDefault();
			m_depthStensilStates[DSEqual]		= commonStates->DepthRead();
		}

	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Setup ConstantBuffers for rendering
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	{
		m_perFrameConstantBuffer = new ConstantBuffer<PerFrameConstantBuffer>();

		m_perRenderableConstantBuffer = new ConstantBuffer<PerRenderableConstantBuffer>();	

		m_renderSettingsConstantBuffer = new ConstantBuffer<RenderSettingsConstantBuffer>();
		m_renderSettings.screensize = XMINT2(G->m_windowManager->getWidth(),G->m_windowManager->getHeight());
		m_renderSettings.numtiles = XMINT2(G->m_windowManager->getNumTilesWidth(), G->m_windowManager->getNumTilesHeight());	
		m_renderSettings.outputType = 0;
		m_renderSettingsConstantBuffer->SetData(m_renderSettings);
	}	

	{
		preProcessingBuffer = new RenderTarget2D();
		preProcessingBuffer->Initialize(G->m_windowManager->getWidth(), G->m_windowManager->getHeight(),DXGI_FORMAT_R8G8B8A8_UNORM,1,4);
	}

	//SHOULD BE PER RENDERABLE
	/////
	
	D3D->devcon->OMSetBlendState(m_blendStates[BSOpaque], Colors::Black, 0xFFFFFFFF);
	
	/////

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Load TiledLighting Shader
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	{
		m_computeShader = G->m_contentManager->load<ComputeShader>("Shaders\\TiledLighting.cso");
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Load PostProcessing Shader
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	{
		m_postProcessingShader = G->m_contentManager->load<ComputeShader>("Shaders\\PostProcessing.cso");
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Create Light Links Structured Buffer
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	{
		m_lightsThisTile = new RWBuffer();
		m_lightsThisTile->Initialize(DXGI_FORMAT_R32_TYPELESS, sizeof(unsigned int),G->m_windowManager->getNumTilesHeight()*G->m_windowManager->getNumTilesWidth()*lightsPerTile,true);

		m_numLightsThisTile = new RWBuffer();
		m_numLightsThisTile->Initialize(DXGI_FORMAT_R32_TYPELESS, sizeof(unsigned int),G->m_windowManager->getNumTilesHeight()*G->m_windowManager->getNumTilesWidth(),true);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Prepare for Shadow Rendering
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	{
		m_shadowRenderer = new CSMRenderer();
	}
}

TiledForwardRenderer::~TiledForwardRenderer()
{
	BOOST_LOG_TRIVIAL(trace) << "Destruction of D3D11 Renderer";
	D3D->backbuffer->Release();  
	D3D->swapchain->Release();
	D3D->devcon->Release();
	D3D->dev->Release();		
}

void TiledForwardRenderer::RenderFrame(std::vector<IRenderable*> &renderables, std::vector<IRenderableSystem*> &renderableSystems)
{	
	PerFrameConstantBuffer perFrameConstantBuffer;
	perFrameConstantBuffer.Camera_View = G->m_camera->GetView().ToSIMD();
	perFrameConstantBuffer.Camera_Projection = G->m_camera->GetProjection().ToSIMD();
	perFrameConstantBuffer.Camera_Position = G->m_camera->GetPosition().ToSIMD();
	m_perFrameConstantBuffer->SetData(perFrameConstantBuffer);

	D3D->devcon->ClearRenderTargetView(D3D->backbuffer, Colors::LightBlue);
	D3D->devcon->ClearRenderTargetView(preProcessingBuffer->m_RTV, Colors::LightBlue);
	m_depthBuffer->Clear(D3D11_CLEAR_DEPTH, 1.0f, 0);

	//Z PREPASS
	{
		D3D->devcon->RSSetState(m_rasterizerStates[RSCullCCW]);
		// set the render target as the back buffer
		RTV::BindRTV(0, NULL, m_depthBuffer);
		D3D->devcon->RSSetViewports(1, &m_normalViewPort);
		D3D->devcon->OMSetDepthStencilState(m_depthStensilStates[DSDefault],1);
		
		m_perFrameConstantBuffer->BindCB(VERTEXSHADER, 0);
		m_perFrameConstantBuffer->BindCB(DOMAINSHADER, 0);
		m_perFrameConstantBuffer->BindCB(HULLSHADER, 0);
		m_perRenderableConstantBuffer->BindCB(VERTEXSHADER, 1);

		D3D->devcon->DSSetSamplers(0, 1, &m_samplerStates[SSLinearWrap]);

		for(IRenderable* r : renderables)
		{
			D3D->devcon->IASetPrimitiveTopology(r->GetPrimitiveType());
			r->UpdateConstantBuffer();
			m_perRenderableConstantBuffer->SetData(r->GetConstantBuffer());
			r->RenderZ();
		}

		for(IRenderableSystem* r : renderableSystems)
			r->RenderZ();
	}

	//Calculate Lighting
	{
		RTV::BindNULLRTV(1);

		m_perFrameConstantBuffer->BindCB(COMPUTESHADER, 0);
		m_renderSettingsConstantBuffer->BindCB(COMPUTESHADER, 2);
		G->m_lightingSystem->m_lightingCB->BindCB(COMPUTESHADER, 3);
		
		//Inputs
		m_depthBuffer->BindSRV(COMPUTESHADER, 0);
		G->m_lightingSystem->m_pointLightsStructuredBuffer->BindSRV(COMPUTESHADER,1);

		//Outputs
		m_numLightsThisTile->BindUAV(0);
		m_lightsThisTile->BindUAV(1);

		m_computeShader->Bind();	
		m_computeShader->Dispatch(G->m_windowManager->getNumTilesWidth(), G->m_windowManager->getNumTilesHeight(),1);

		UAV::BindNULLUAV(2);
		SRV::BindNULLSRV(COMPUTESHADER, 0, 2);
	}

	//Shadow Rendering
	{
		D3D->devcon->RSSetState(m_rasterizerStates[RSCullNone]);
		m_shadowRenderer->RenderFrame(renderables, renderableSystems);
	}

	//Final Pass
	{
		D3D->devcon->RSSetState(m_rasterizerStates[RSCullCCW]);
		
		RTV::BindRTV(&preProcessingBuffer->m_RTV, m_depthBuffer);
		D3D->devcon->RSSetViewports(1, &m_normalViewPort);
		D3D->devcon->OMSetDepthStencilState(m_depthStensilStates[DSEqual],1);

		m_perFrameConstantBuffer->BindCB(VERTEXSHADER, 0);
		m_perFrameConstantBuffer->BindCB(DOMAINSHADER, 0);
		m_perFrameConstantBuffer->BindCB(HULLSHADER, 0);
		m_perRenderableConstantBuffer->BindCB(VERTEXSHADER, 1);

		m_numLightsThisTile->BindSRV(PIXELSHADER, 0);
		m_lightsThisTile->BindSRV(PIXELSHADER, 1);
		G->m_lightingSystem->m_pointLightsStructuredBuffer->BindSRV(PIXELSHADER, 2);
		G->m_lightingSystem->m_directionalLightsStructuredBuffer->BindSRV(PIXELSHADER, 3);
		m_shadowRenderer->m_shadowMaps->BindSRV(PIXELSHADER, 4);
		m_renderSettingsConstantBuffer->BindCB(PIXELSHADER, 2);
		G->m_lightingSystem->m_lightingCB->BindCB(PIXELSHADER, 3);
		m_shadowRenderer->m_shadowConstantBuffer->BindCB(PIXELSHADER, 4);

		D3D->devcon->PSSetSamplers(1, 1, &m_shadowRenderer->m_shadowSampler);
		D3D->devcon->DSSetSamplers(0, 1, &m_samplerStates[SSLinearWrap]);
		
		int polycount = 0;

		D3D->devcon->PSSetSamplers(0, 1, &m_samplerStates[SSLinearWrap]);
		for(IRenderable* r : renderables)
		{
			D3D->devcon->IASetPrimitiveTopology(r->GetPrimitiveType());

			r->UpdateConstantBuffer();
			m_perRenderableConstantBuffer->SetData(r->GetConstantBuffer());
			r->Render();
		}

		for(IRenderableSystem* r : renderableSystems)
			polycount += r->Render();

		RTV::BindNULLRTV(1);
		SRV::BindNULLSRV(PIXELSHADER, 0, 7);
	}

	//PostProcessing
	{
		preProcessingBuffer->BindSRV(COMPUTESHADER, 0);
		m_shadowRenderer->m_shadowMaps->BindSRV(COMPUTESHADER,1);
		D3D->devcon->CSSetUnorderedAccessViews(0, 1, &D3D->backbufferuav, NULL);

		m_postProcessingShader->Bind();	
		m_postProcessingShader->Dispatch(G->m_windowManager->getNumTilesWidth(), G->m_windowManager->getNumTilesHeight(),1);
	}

	D3D->swapchain->Present(0, 0);
}

void TiledForwardRenderer::ToggleRenderOutputMode()
{
	BOOST_LOG_TRIVIAL(trace) << "Toggling Render Output Mode!";

	m_renderSettings.outputType++;
	m_renderSettings.outputType %= 4;

	m_renderSettingsConstantBuffer->SetData(m_renderSettings);
}