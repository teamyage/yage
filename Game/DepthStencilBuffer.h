#pragma once

#include "SRV.h"
#include "DSV.h"

struct DepthStencilBuffer : public SRV, public DSV
{
	ID3D11Texture2D *texture;
	UINT Width;
	UINT Height;
	UINT MultiSamples;
	UINT MSQuality;
	DXGI_FORMAT Format;
	UINT ArraySize;
	std::vector<ID3D11DepthStencilView*> ArraySlices;

	DepthStencilBuffer();

	void ClearAllSlices(UINT ClearFlags, FLOAT Depth, UINT8 Stencil);

	void Initialize(	UINT width,
		UINT height,
		DXGI_FORMAT format = DXGI_FORMAT_D24_UNORM_S8_UINT,
		bool useAsShaderResource = false,
		UINT multiSamples = 1,
		UINT msQuality = 0,
		UINT arraySize = 1);

	void Release();
	void BindAsOnlyRenderTarget(UINT slice=0);
};

