#include "stdafx.h"
#include "GeometryShader.h"

GeometryShader::GeometryShader(std::string filename) : BinaryLoader(filename)
{
	if(m_data)
	{
		BOOST_LOG_TRIVIAL(trace) << "Creating GeometryShader for " << m_filename;
		HRESULT hr = D3D->dev->CreateGeometryShader(m_data, m_dataLength, nullptr, &m_geometryShader);

		if (FAILED(hr)) 
			BOOST_LOG_TRIVIAL(error) << "Error while creating GeometryShader: " << m_filename;
	}
}


GeometryShader::~GeometryShader()
{
	BOOST_LOG_TRIVIAL(trace) << "Releasing GeometryShader for " << m_filename;
	SAFE_RELEASE(m_geometryShader);
}

void GeometryShader::Bind()
{
	D3D->devcon->GSSetShader(m_geometryShader, 0, 0);
}

void GeometryShader::Release()
{
	m_geometryShader->Release();
}