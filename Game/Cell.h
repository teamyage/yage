#pragma once
#include "Renderable.h"
#include "D3DSystem.h"
#include "CellId.h"
#include "gtc/noise.hpp"
#include "Float3.h"
#include "System.h"
#include "PhysicsSystem.h"
#include "Utils.h"
#include "Camera.h"

using namespace DirectX;

#define CELLWIDTH 64
#define CELLWIDTHP1 65

enum offsets
{
	XP=0,
	XN=1,
	YP=2,
	YN=3,
	ZP=4,
	ZN=5
};

class Cell : public D3DSystem, public System
{
public:
	Cell();
	~Cell();

	bool Generate();
	void UpdateConstantBuffer();
	void Render();
	PerRenderableConstantBuffer& GetConstantBuffer();
	void SetCellId(CellId cid);
	inline bool hasDensityChange(BYTE A,BYTE B);
	CellId getId();
	void PrepareForRendering();
	int polycount;

private:	
	XMVECTOR cellWidth;
	CellId m_cid;
	PerRenderableConstantBuffer m_perRenderableConstantBuffer;
	VertexBuffer<VertexPositionNormal> *m_vertexBuffer;
	std::vector<VertexPositionNormal> verts;
	BYTE ***data;
	XMVECTOR Xp1, Xn1, Yp1, Yn1, Zp1, Zn1;
	XMVECTOR offsets[6][6];
	btRigidBody *m_rb;
};
