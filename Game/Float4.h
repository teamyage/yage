#pragma once

#include "stdafx.h"
#include "Float3.h"

using namespace DirectX;

struct Float4 : public XMFLOAT4
{
	Float4();
	Float4(float x);
	Float4(float x, float y, float z, float w);
	Float4(const XMFLOAT3& xyz, float w = 0.0f);
	Float4(const XMFLOAT4& xyzw);
	Float4(FXMVECTOR xyzw);

	Float4& operator+=(const Float4& other);
	Float4 operator+(const Float4& other) const;

	Float4& operator-=(const Float4& other);
	Float4 operator-(const Float4& other) const;

	Float4& operator*=(const Float4& other);
	Float4 operator*(const Float4& other) const;

	Float4& operator/=(const Float4& other);
	Float4 operator/(const Float4& other) const;

	bool operator==(const Float4& other) const;
	bool operator!=(const Float4& other) const;

	Float4 operator-() const;

	XMVECTOR ToSIMD() const;
	Float3 To3D() const;

	static Float4 Clamp(const Float4& val, const Float4& min, const Float4& max);
};

