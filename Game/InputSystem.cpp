#include "stdafx.h"
#include "InputSystem.h"
#include "WindowManager.h"

InputSystem::InputSystem()
{
	memset(m_keysDown,0,256*sizeof(bool));
	memset(m_keysDownThisFrame,0,256*sizeof(bool));
	HideMouseCursor();
}

InputSystem::~InputSystem()
{
	ShowMouseCursor();
}

void InputSystem::SetupRawInput()
{
	RAWINPUTDEVICE Mouse;
	Mouse.usUsage = 0x02;
	Mouse.usUsagePage = 0x01;
	Mouse.dwFlags = NULL;
	Mouse.hwndTarget = G->m_windowManager->getWindowHandle();

	RegisterRawInputDevices(&Mouse, 1, sizeof(RAWINPUTDEVICE));
}

bool InputSystem::IsKeyDown(int key)
{
	return m_keysDown[key];
}

bool InputSystem::IsKeyDownThisFrame(int key)
{
	return m_keysDownThisFrame[key];
}

void InputSystem::Process()
{
	memset(m_keysDownThisFrame,0,256*sizeof(bool));
}

void InputSystem::RecenterMouseCursor()
{
	int x = G->m_windowManager->getWidth();
	int y = G->m_windowManager->getHeight();
	SetCursorPos(x/2, y/2);
}
void InputSystem::ShowMouseCursor()
{
	ShowCursor(true);
	m_cursorShowing = true;
}
void InputSystem::HideMouseCursor()
{
	ShowCursor(false);
	m_cursorShowing = false;
}
void InputSystem::ToggleMouseCursor()
{
	ShowCursor(m_cursorShowing);
	m_cursorShowing = !m_cursorShowing;	
}

LRESULT InputSystem::WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INPUT:
		{						
			RAWINPUT InputData;
			UINT DataSize = sizeof(RAWINPUT);
			GetRawInputData((HRAWINPUT)lParam,RID_INPUT,&InputData,&DataSize,sizeof(RAWINPUTHEADER));
			if (InputData.data.mouse.usFlags == MOUSE_MOVE_RELATIVE)
			{
				Float2 mousedata((float)InputData.data.mouse.lLastX, (float)-InputData.data.mouse.lLastY);
				G->m_camera->MoveMouse(mousedata);	
				RecenterMouseCursor();
			}			
			return 0;
		}
	

		case WM_SYSCOMMAND:
		{
			if(wParam == SC_MONITORPOWER || wParam == SC_SCREENSAVE)
				return 0;
			break;
		}
			
		case WM_CLOSE:		
		case WM_DESTROY:
		{
			PostQuitMessage(0);				
			return 0;	
		}								

		case WM_KEYDOWN:	
		{
			m_keysDown[wParam] = TRUE;	

			if(!(lParam&(1<<30)))
				m_keysDownThisFrame[wParam] = TRUE;

			return 0;
		}									

		case WM_KEYUP:	
		{
			m_keysDown[wParam] = FALSE;			
			return 0;
		}									

		case WM_SIZE:	
		{
			//G->m_windowManager->ReSizeGLScene(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			return 0;
		}											
	}
	return DefWindowProc (hWnd, message, wParam, lParam);
}
