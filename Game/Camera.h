#pragma once

#include "System.h"
#include <DirectXMath.h>
#include <boost/algorithm/clamp.hpp>
#include "Directxmath.h"
#include "WindowManager.h"
#include "Float2.h"
#include "Float3.h"
#include "Float4x4.h"

using namespace DirectX;

class Camera : public System
{
public:
	Camera();

	//Processing
	void RebuildViewMatrix();
	virtual void RebuildProjectionMatrix() = NULL;

	//Mutators
	void SetLookDirection(float yaw, float pitch, float roll=0);
	void SetLookDirectionDelta(float yaw, float pitch, float roll=0);
	void SetLookAt(Float3 &eye, Float3 &lookAt, Float3 &up);
	void SetNearPlane(float nearPlane);
	void SetFarPlane(float farPlane);
	void SetPosition(Float3 position);		

	//Accessors
	Float4x4& GetView();
	Float4x4& GetProjection();
	Float4x4& GetViewProjection();
	Float3 GetPosition();
	Float3 GetLookAt();
	float GetNearClipPlane();
	float GetFarClipPlane();
	float GetPitch();
	float GetYaw();
	float GetRoll();
	float GetNearPlaneDistance();
	float GetFarPlaneDistance();

protected:
	Float4x4	m_viewMatrix;
	Float4x4	m_projectionMatrix;
	Float4x4	m_viewProjectionMatrix;
	Float3		m_position;
	Float3		m_lookAt;
	Float3		m_up;
	float		m_nearPlane;
	float		m_farPlane;
	float		m_pitch;
	float		m_yaw;
	float		m_roll;
};

class PerspectiveCamera : public Camera
{
public:
	PerspectiveCamera();
	void SetFieldOfView(float fov);
	void SetAspectRatio(float aspectRatio);
	virtual void RebuildProjectionMatrix();

protected:
	float		m_fieldOfView;
	float		m_aspectRatio;
};

class FirstPersonCamera : public PerspectiveCamera
{
public:
	void MoveMouse(Float2 mousedata);

private:

};

class OrthographicCamera : public Camera
{
public:
	OrthographicCamera(float minx, float miny, float maxx, float maxy, float nearPlane, float farPlane);
	~OrthographicCamera();

	virtual void RebuildProjectionMatrix();
	void SetProjection(const Float4x4 &newProjection);

protected:
	float m_xmin;
	float m_xmax;
	float m_ymin;
	float m_ymax;
};
