#pragma once

#include "stdafx.h"

using namespace DirectX;

struct Float2 : public XMFLOAT2
{
	Float2();
	Float2(float x);
	Float2(float x, float y);
	Float2(const XMFLOAT2& xy);
	Float2(FXMVECTOR xy);
	Float2& operator+=(const Float2& other);
	Float2 operator+(const Float2& other) const;
	Float2& operator-=(const Float2& other);
	Float2 operator-(const Float2& other) const;
	Float2& operator*=(const Float2& other);
	Float2 operator*(const Float2& other) const;
	Float2& operator/=(const Float2& other);
	Float2 operator/(const Float2& other) const;
	bool operator==(const Float2& other) const;
	bool operator!=(const Float2& other) const;
	Float2 operator-() const;

	XMVECTOR ToSIMD() const;

	static Float2 Clamp(const Float2& val, const Float2& min, const Float2& max);
	static float Length(const Float2& val);
};