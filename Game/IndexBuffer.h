#pragma once
#include "d3dsystem.h"

template <class T>
class IndexBuffer : public D3DSystem
{
public:
	IndexBuffer(T* indices, UINT numIndices, DXGI_FORMAT format)
	{
		Initialize(indices, numIndices, format);
	}
	
	IndexBuffer(std::vector<T> &indices, DXGI_FORMAT format)
	{		
		Initialize(&indices[0], (UINT)indices.size(), format);
	}

	~IndexBuffer()
	{
		SAFE_RELEASE(m_buffer);
	}

	void Bind()
	{
		D3D->devcon->IASetIndexBuffer(m_buffer, m_format, 0);
	}

	void Release()
	{
		m_buffer->Release();
	}

	UINT getNumIndices()
	{
		return m_numIndices;
	}

private:
	void Initialize(T* indices, UINT numIndices, DXGI_FORMAT format)
	{
		this->m_startIndex = 0;
		this->m_numIndices = numIndices;
		this->m_format = format;

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_DYNAMIC;
		bd.ByteWidth = (UINT)(sizeof(T) * m_numIndices);
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		bd.MiscFlags = 0;

		D3D->dev->CreateBuffer(&bd, NULL, &m_buffer);

		D3D11_MAPPED_SUBRESOURCE ms;
		D3D->devcon->Map(m_buffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);
		memcpy(ms.pData, indices, m_numIndices*sizeof(T));
		D3D->devcon->Unmap(m_buffer, NULL);
	}

	UINT m_startIndex;
	UINT m_numIndices;
	ID3D11Buffer *m_buffer;
	DXGI_FORMAT m_format;
	IndexBuffer(IndexBuffer const&);
	IndexBuffer& operator= (IndexBuffer const&);
};

