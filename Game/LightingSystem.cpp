#include "stdafx.h"
#include "LightingSystem.h"

LightingSystem::LightingSystem() 
{
	m_lightingConstantBufferData.numDirectionalLights = 0;
	m_lightingConstantBufferData.numPointLights = 0;
	m_lightingCB = new ConstantBuffer<LightingConstantBuffer>(); 
	m_pointLightsStructuredBuffer = new StructuredBuffer<PointLight>(NULL, 0, false, false, false, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
	m_directionalLightsStructuredBuffer = new StructuredBuffer<DirectionalLight>(NULL, 0, false, false, false, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);

	AddDirectionalLight(Float3(.9,.7,.9),Float3(1,1,1),1.0);

	for(int i=0;i<16;i++)
		AddPointLight(Float3(Utils::random01()*768.0f-384,Utils::random01()*64.0f,Utils::random01()*768.0f-384), 100.0f, Float3(Utils::random01(),Utils::random01(),Utils::random01()), 1.0f);
}

LightingSystem::~LightingSystem()
{
}

void LightingSystem::AddPointLight(Float3 position, float radius, Float3 color, float intensity)
{
	PointLight p;

	p.m_position = position;
	p.m_radius = radius;
	p.m_color = color;
	p.m_intensity = intensity;

	m_pointLights.push_back(p);

	m_pointLightsStructuredBuffer->Update(m_pointLights);
	m_lightingConstantBufferData.numPointLights = (UINT)m_pointLights.size();
	m_lightingCB->SetData(m_lightingConstantBufferData);
}

void LightingSystem::AddDirectionalLight(Float3 rayDirection, Float3 color, float intensity)
{
	DirectionalLight d;

	d.m_rayDirection = Float3::Normalize(rayDirection);
	d.m_color = color;
	d.m_intensity = intensity;

	m_directionalLights.push_back(d);

	m_directionalLightsStructuredBuffer->Update(m_directionalLights);
	m_lightingConstantBufferData.numDirectionalLights = (UINT)m_directionalLights.size();
	m_lightingCB->SetData(m_lightingConstantBufferData);
}

void LightingSystem::Process()
{
	//Rotate the directional light with id 0, around the x axis
	{
		static float dlspin = 0.0;
		dlspin=0.25f*(float)G->m_timer->getLastFrameLength();
		XMMATRIX rotationMatrix = XMMatrixRotationRollPitchYaw(0,dlspin,0);
		XMFLOAT3 destDirection; 
		XMStoreFloat3(&destDirection, XMVector3Transform(m_directionalLights[0].m_rayDirection.ToSIMD(), rotationMatrix));
		UpdateDirectionalLightDirection(0, destDirection);
	}


	static float plspin = 0.0;
	plspin=0.1f*(float)G->m_timer->getLastFrameLength();
	XMMATRIX rotationTMatrix = XMMatrixRotationRollPitchYaw(0,plspin,0);
	for(unsigned int i=0;i<m_pointLights.size();i++)
	{
		XMVECTOR newPlightLocation = XMVector3Transform(XMLoadFloat3(&m_pointLights[i].m_position), rotationTMatrix);
		XMFLOAT3 newPlightLocation2; 
		XMStoreFloat3(&newPlightLocation2, newPlightLocation);
		m_pointLights[i].m_position = newPlightLocation2;
	}
	if(!m_pointLights.empty())
		m_pointLightsStructuredBuffer->Update(m_pointLights);
}

DirectionalLight& LightingSystem::GetShadowCastingDirectionalLight()
{
	return m_directionalLights[0];
}

void LightingSystem::UpdateDirectionalLightDirection(unsigned int id, Float3 rayDirection)
{
	if(id < m_directionalLights.size())
		UpdateDirectionalLight(id, rayDirection, m_directionalLights[id].m_color, m_directionalLights[id].m_intensity);
}
void LightingSystem::UpdateDirectionalLightColor(unsigned int id, Float3 color)
{
	if(id < m_directionalLights.size())
		UpdateDirectionalLight(id, m_directionalLights[id].m_rayDirection, color, m_directionalLights[id].m_intensity);
}
void LightingSystem::UpdateDirectionalLightIntensity(unsigned int id, float intensity)
{
	if(id < m_directionalLights.size())
		UpdateDirectionalLight(id, m_directionalLights[id].m_rayDirection, m_directionalLights[id].m_color, intensity);
}
void LightingSystem::UpdateDirectionalLightDirectionAndColor(unsigned int id, Float3 rayDirection, Float3 color)
{
	if(id < m_directionalLights.size())
		UpdateDirectionalLight(id, rayDirection, color, m_directionalLights[id].m_intensity);
}
void LightingSystem::UpdateDirectionalLightDirectionAndIntensity(unsigned int id, Float3 rayDirection, float intensity)
{
	if(id < m_directionalLights.size())
		UpdateDirectionalLight(id, rayDirection, m_directionalLights[id].m_color, intensity);
}
void LightingSystem::UpdateDirectionalLightColorAndIntensity(unsigned int id, Float3 color, float intensity)
{
	if(id < m_directionalLights.size())
		UpdateDirectionalLight(id, m_directionalLights[id].m_rayDirection, color, intensity);
}
void LightingSystem::UpdateDirectionalLight(unsigned int id, Float3 rayDirection, Float3 color, float intensity)
{
	if(id < m_directionalLights.size())
	{
		DirectionalLight d = m_directionalLights[id];

		d.m_rayDirection = Float3::Normalize(rayDirection);
		d.m_color = color;
		d.m_intensity = intensity;

		m_directionalLights[id] = d;

		m_directionalLightsStructuredBuffer->Update(m_directionalLights);
	}
}