#pragma once

struct D3DGlobals
{
public:
	D3DGlobals();

	IDXGISwapChain *swapchain;
	ID3D11Device *dev;
	ID3D11DeviceContext *devcon;
	ID3D11RenderTargetView *backbuffer; 
	ID3D11UnorderedAccessView *backbufferuav; 
};
