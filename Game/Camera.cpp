#include "stdafx.h"

#include "Camera.h"


#define MOVEMENTSPEED 15.0f

Camera::Camera() 
{
	//BOOST_LOG_TRIVIAL(trace) << "Construction of Camera";
	m_yaw = 0;
	m_pitch = 0;
	m_roll = 0;
	m_up = Float3(0.0f, 1.0f, 0.0f);;
	m_nearPlane		= 0.25f;
	m_farPlane		= 1000.0;
	m_position = Float3(-12.0f, 10.0f, -12.0f);

	SetLookDirection(.78, -.37);
}

void Camera::RebuildViewMatrix()
{
	XMMATRIX rotationMatrix = XMMatrixRotationRollPitchYaw
	(
		-m_pitch,
		m_yaw,
		m_roll
	);

	m_lookAt = XMVector3Transform
	(
		Float3(0.0f, 0.0f, 1.0f).ToSIMD(),
		rotationMatrix
	);
	
	m_viewMatrix = XMMatrixLookToLH
	(
		m_position.ToSIMD(), 
		m_lookAt.ToSIMD(), 
		m_up.ToSIMD()
	);
	m_viewProjectionMatrix = m_viewMatrix*m_projectionMatrix;
}

void Camera::SetLookDirection(float yaw, float pitch, float roll)
{
	m_yaw = yaw;
	m_pitch = pitch;
	m_roll = roll;
	RebuildViewMatrix();
}

void Camera::SetLookDirectionDelta(float yaw, float pitch, float roll)
{
	m_yaw += yaw;
	m_roll += roll;	
	m_pitch = boost::algorithm::clamp(m_pitch+pitch, -XM_PIDIV2+0.0001f, XM_PIDIV2-0.0001f);
	RebuildViewMatrix();
}

void Camera::SetLookAt(Float3 &eye, Float3 &lookAt, Float3 &up)
{
	m_viewMatrix = XMMatrixLookAtLH(eye.ToSIMD(), lookAt.ToSIMD(), up.ToSIMD());
	m_viewProjectionMatrix = m_viewMatrix * m_projectionMatrix;
}

void Camera::SetNearPlane(float nearPlane)
{
	m_nearPlane = nearPlane;
	RebuildProjectionMatrix();
}
void Camera::SetFarPlane(float farPlane)
{
	m_farPlane = farPlane;
	RebuildProjectionMatrix();
}
void Camera::SetPosition(Float3 position)
{
	m_position = position;
	RebuildViewMatrix();
}	

Float4x4& Camera::GetView() { return m_viewMatrix; }
Float4x4& Camera::GetProjection() {	return m_projectionMatrix; }
Float4x4& Camera::GetViewProjection() {	return m_viewProjectionMatrix; }
Float3 Camera::GetPosition() { return m_position; }
Float3 Camera::GetLookAt() { return m_lookAt; }
float Camera::GetNearClipPlane() { return m_nearPlane; }
float Camera::GetFarClipPlane() { return m_farPlane; }
float Camera::GetPitch() { return m_pitch; }
float Camera::GetYaw() { return m_yaw; }
float Camera::GetRoll() { return m_roll; }

PerspectiveCamera::PerspectiveCamera()
{
	m_fieldOfView	= XM_PIDIV4;
	m_aspectRatio = G->m_windowManager->getAspectRatio();

	RebuildProjectionMatrix();
}


void PerspectiveCamera::RebuildProjectionMatrix()
{
	m_projectionMatrix = XMMatrixPerspectiveFovLH
	(
		m_fieldOfView, 
		m_aspectRatio, 
		m_nearPlane, 
		m_farPlane
	);
	m_viewProjectionMatrix = m_viewMatrix*m_projectionMatrix;
}

void PerspectiveCamera::SetFieldOfView(float fov)
{
	m_fieldOfView = fov;
	RebuildProjectionMatrix();
}
void PerspectiveCamera::SetAspectRatio(float aspectRatio)
{
	m_aspectRatio = aspectRatio;
	RebuildProjectionMatrix();
}


void FirstPersonCamera::MoveMouse(Float2 mousedata)
{
	SetLookDirectionDelta(mousedata.x/500.0f, mousedata.y/500.0f, 0);
}

OrthographicCamera::OrthographicCamera(float xmin, float ymin, float xmax, float ymax, float nearPlane, float farPlane)
{
	m_xmin =	  xmin;
	m_ymin =	  ymin;
	m_xmax =	  xmax;
	m_ymax =	  ymax;
	m_nearPlane = nearPlane;
	m_farPlane =  farPlane;
	RebuildProjectionMatrix();
}

OrthographicCamera::~OrthographicCamera()
{

}

void OrthographicCamera::RebuildProjectionMatrix()
{
	m_projectionMatrix = XMMatrixOrthographicOffCenterLH(m_xmin, m_xmax, m_ymin, m_ymax, m_nearPlane, m_farPlane);
	m_viewProjectionMatrix = m_viewMatrix*m_projectionMatrix;
}

void OrthographicCamera::SetProjection(const Float4x4 &newProjection)
{
	m_projectionMatrix = newProjection;
	m_viewProjectionMatrix = m_viewMatrix*m_projectionMatrix;
}
