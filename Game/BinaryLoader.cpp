#include "stdafx.h"
#include "BinaryLoader.h"

BinaryLoader::BinaryLoader(std::string filename)
{
	m_filename = filename;
	m_data = NULL;
	BOOST_LOG_TRIVIAL(trace) << "Loading binary data from: " << m_filename;

	std::ifstream fstream;  
	fstream.open (m_filename,  std::ifstream::in | std::ifstream::binary);

	if(fstream.fail())
		BOOST_LOG_TRIVIAL(error) << "Missing file - " << m_filename;
	else
	{
		fstream.seekg(0, std::ios::end);  
		m_dataLength = size_t(fstream.tellg());  
		m_data = new char[m_dataLength];
		fstream.seekg(0, std::ios::beg);  
		fstream.read(&m_data[0], m_dataLength);  
	}

	fstream.close();
}

BinaryLoader::~BinaryLoader()
{
	if(m_data)
	{
		delete m_data;
		BOOST_LOG_TRIVIAL(trace) << "Delete Binary Data for: " << m_filename;
	}
}