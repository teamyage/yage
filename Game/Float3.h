#pragma once

#include "stdafx.h"
#include "Float4x4.h"
#include "Float3x3.h"
#include "Quaternion.h"

using namespace DirectX;

struct Quaternion;

struct Float3 : public XMFLOAT3
{
	Float3();
	Float3(float x);
	Float3(float x, float y, float z);
	Float3(const XMFLOAT3& xyz);
	Float3(FXMVECTOR xyz);

	Float3& operator+=(const Float3& other);
	Float3 operator+(const Float3& other) const;

	Float3& operator+=(float other);
	Float3 operator+(float other) const;

	Float3& operator-=(const Float3& other);
	Float3 operator-(const Float3& other) const;

	Float3& operator-=(float other);
	Float3 operator-(float other) const;

	Float3& operator*=(const Float3& s);
	Float3 operator*(const Float3& s) const;

	Float3& operator*=(float other);
	Float3 operator*(float other) const;

	Float3& operator/=(const Float3& other);
	Float3 operator/(const Float3& other) const;

	Float3& operator/=(float other);
	Float3 operator/(float other) const;

	bool operator==(const Float3& other) const;
	bool operator!=(const Float3& other) const;

	Float3 operator-() const;

	XMVECTOR ToSIMD() const;

	float Length() const;

	static float Dot(const Float3& a, const Float3& b);
	static Float3 Cross(const Float3& a, const Float3& b);
	static Float3 Normalize(const Float3& a);
	static Float3 Transform(const Float3& v, const Float3x3& m);
	static Float3 Transform(const Float3& v, const Float4x4& m);
	static Float3 TransformDirection(const Float3&v, const Float4x4& m);
	static Float3 Transform(const Float3& v, const Quaternion& q);
	static Float3 Clamp(const Float3& val, const Float3& min, const Float3& max);
	static Float3 Perpendicular(const Float3& v);
	static float Distance(const Float3& a, const Float3& b);
	static float Length(const Float3& v);
};

// Non-member operators of Float3
Float3 operator*(float a, const Float3& b);


