#include "stdafx.h"
#include "Cube.h"
#include "MaterialLibrary.h"

Cube::Cube()
{
	VERTEXTYPE_CUBE verts[] = {
		VERTEXTYPE_CUBE(XMFLOAT3(-1.0f, -1.0f,  1.0f), XMFLOAT3( 0.0f,  0.0f,  1.0f), XMFLOAT2(0.0f, 0.0f)),	// side 1
		VERTEXTYPE_CUBE(XMFLOAT3( 1.0f, -1.0f,  1.0f), XMFLOAT3( 0.0f,  0.0f,  1.0f), XMFLOAT2(0.0f, 1.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3(-1.0f,  1.0f,  1.0f), XMFLOAT3( 0.0f,  0.0f,  1.0f), XMFLOAT2(1.0f, 0.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3( 1.0f,  1.0f,  1.0f), XMFLOAT3( 0.0f,  0.0f,  1.0f), XMFLOAT2(1.0f, 1.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3( 0.0f,  0.0f, -1.0f), XMFLOAT2(0.0f, 0.0f)),    // side 2
		VERTEXTYPE_CUBE(XMFLOAT3(-1.0f,  1.0f, -1.0f), XMFLOAT3( 0.0f,  0.0f, -1.0f), XMFLOAT2(0.0f, 1.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3( 1.0f, -1.0f, -1.0f), XMFLOAT3( 0.0f,  0.0f, -1.0f), XMFLOAT2(1.0f, 0.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3( 1.0f,  1.0f, -1.0f), XMFLOAT3( 0.0f,  0.0f, -1.0f), XMFLOAT2(1.0f, 1.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3(-1.0f,  1.0f, -1.0f), XMFLOAT3( 0.0f,  1.0f,  0.0f), XMFLOAT2(0.0f, 0.0f)),    // side 3
		VERTEXTYPE_CUBE(XMFLOAT3(-1.0f,  1.0f,  1.0f), XMFLOAT3( 0.0f,  1.0f,  0.0f), XMFLOAT2(0.0f, 1.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3( 1.0f,  1.0f, -1.0f), XMFLOAT3( 0.0f,  1.0f,  0.0f), XMFLOAT2(1.0f, 0.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3( 1.0f,  1.0f,  1.0f), XMFLOAT3( 0.0f,  1.0f,  0.0f), XMFLOAT2(1.0f, 1.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3( 0.0f, -1.0f,  0.0f), XMFLOAT2(0.0f, 0.0f)),    // side 4
		VERTEXTYPE_CUBE(XMFLOAT3( 1.0f, -1.0f, -1.0f), XMFLOAT3( 0.0f, -1.0f,  0.0f), XMFLOAT2(0.0f, 1.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3(-1.0f, -1.0f,  1.0f), XMFLOAT3( 0.0f, -1.0f,  0.0f), XMFLOAT2(1.0f, 0.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3( 1.0f, -1.0f,  1.0f), XMFLOAT3( 0.0f, -1.0f,  0.0f), XMFLOAT2(1.0f, 1.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3( 1.0f, -1.0f, -1.0f), XMFLOAT3( 1.0f,  0.0f,  0.0f), XMFLOAT2(0.0f, 0.0f)),    // side 5
		VERTEXTYPE_CUBE(XMFLOAT3( 1.0f,  1.0f, -1.0f), XMFLOAT3( 1.0f,  0.0f,  0.0f), XMFLOAT2(0.0f, 1.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3( 1.0f, -1.0f,  1.0f), XMFLOAT3( 1.0f,  0.0f,  0.0f), XMFLOAT2(1.0f, 0.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3( 1.0f,  1.0f,  1.0f), XMFLOAT3( 1.0f,  0.0f,  0.0f), XMFLOAT2(1.0f, 1.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(-1.0f,  0.0f,  0.0f), XMFLOAT2(0.0f, 0.0f)),    // side 6
		VERTEXTYPE_CUBE(XMFLOAT3(-1.0f, -1.0f,  1.0f), XMFLOAT3(-1.0f,  0.0f,  0.0f), XMFLOAT2(0.0f, 1.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3(-1.0f,  1.0f, -1.0f), XMFLOAT3(-1.0f,  0.0f,  0.0f), XMFLOAT2(1.0f, 0.0f)),
		VERTEXTYPE_CUBE(XMFLOAT3(-1.0f,  1.0f,  1.0f), XMFLOAT3(-1.0f,  0.0f,  0.0f), XMFLOAT2(1.0f, 1.0f))
	};
	m_vertexBuffer = new VertexBuffer<VERTEXTYPE_CUBE>(verts, 24);

	INDEXTYPE_CUBE indices[] = 
	{
		0, 1, 2, 2, 1, 3,			// side 1
		4, 5, 6, 6, 5, 7,			// side 2'
		8, 9, 10, 10, 9, 11,		// side 3
		12, 13, 14, 14, 13, 15,		// side 4
		16, 17, 18, 18, 17, 19,		// side 5
		20, 21, 22, 22, 21, 23,		// side 6
	};
	m_indexBuffer = new IndexBuffer<INDEXTYPE_CUBE>(indices, 36, INDEXFORMAT_CUBE);

	m_primitiveType = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	btCollisionShape *shape = new btBoxShape(btVector3(1,1,1));

	btDefaultMotionState *defaultMotionState = 
		new btDefaultMotionState(
			btTransform(
				btQuaternion(0,0,0,1),
				Utils::convert(G->m_camera->GetPosition())
			)
		);

	btScalar mass = 1;
	btVector3 fallInertia(0,0,0);
	shape->calculateLocalInertia(mass, fallInertia);

	btRigidBody::btRigidBodyConstructionInfo rigidBodyCI(
												mass, 
												defaultMotionState, 
												shape, 
												fallInertia);

	m_rb = new btRigidBody(rigidBodyCI);

	m_rb->setCcdMotionThreshold(10);

	m_rb->setLinearVelocity(Utils::convert(G->m_camera->GetLookAt()*50));

	G->m_physicsSystem->m_dynamicsWorld->addRigidBody(m_rb);
	
	m_material = G->m_materialsLibrary->m_materials.find("CubeMaterial")->second;
}

Cube::~Cube()
{
	Release();
}

void Cube::UpdateConstantBuffer()
{
	btTransform worldTransform;
	m_rb->getMotionState()->getWorldTransform(worldTransform);

	Float4x4 translate;
	translate.SetTranslation(worldTransform.getOrigin());

	btQuaternion rotationQ = worldTransform.getRotation();

	m_perRenderableConstantBuffer.Renderable_Rotation = 
		XMMatrixRotationQuaternion(Utils::convert(rotationQ).ToSIMD());

	m_perRenderableConstantBuffer.Renderable_Translation = 
		translate.ToSIMD();

	m_perRenderableConstantBuffer.Renderable_Scale = 
		XMMatrixIdentity();
}

int Cube::Render()
{
	m_material->m_pixelShader->Bind();
	m_material->m_vertexShader->Bind();
	m_material->m_diffuse->Bind(PIXELSHADER,5);	

	m_vertexBuffer->Bind();
	m_indexBuffer->Bind();	

	D3D->devcon->DrawIndexed(m_indexBuffer->getNumIndices(), 0, 0);
		
	SRV::BindNULLSRV(PIXELSHADER, 5, 1);

	return m_indexBuffer->getNumIndices()/3;
}

void Cube::RenderZ()
{
	m_material->m_vertexShader->Bind();
	m_material->m_pixelShader->Unbind();

	m_vertexBuffer->Bind();
	m_indexBuffer->Bind();

	D3D->devcon->DrawIndexed(m_indexBuffer->getNumIndices(), 0, 0);
}
