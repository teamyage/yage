#pragma once

#include "d3dsystem.h"
#include "System.h"
#include "Float4x4.h"
#include "Camera.h"
#include "Renderable.h"
#include "DepthStencilBuffer.h"
#include "RTV.h"
#include "LightingSystem.h"

#define NUM_CASCADES 4

class CSMRenderer : public System, public D3DSystem
{
public:
	CSMRenderer();
	~CSMRenderer();
		
	void RenderFrame(std::vector<IRenderable*> &renderables, std::vector<IRenderableSystem*> &renderableSystems);	

	DepthStencilBuffer *m_shadowMaps;
	ConstantBuffer<ShadowsConstantBuffer> *m_shadowConstantBuffer;
	ID3D11SamplerState *m_shadowSampler;

private:
	Float4x4 GetShadowMatrix(Float3 &lightDirection);
	OrthographicCamera *m_shadowCamera;
	ShadowsConstantBuffer m_shadowConstantBufferData;
	ConstantBuffer<PerFrameConstantBuffer> *m_perFrameConstantBuffer;
	PerRenderableConstantBuffer m_perRenderableConstantBufferData;
	ConstantBuffer<PerRenderableConstantBuffer> *m_perRenderableConstantBuffer;
};

