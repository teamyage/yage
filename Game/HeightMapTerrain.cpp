#include "stdafx.h"
#include "HeightMapTerrain.h"


HeightMapTerrain::HeightMapTerrain(std::string filename, Float2 dims) : BinaryLoader(filename)
{
	//std::vector<VertexPositionNormal> verts;
	//for(int i=0;i<2048;i++)
	//{
	//	for(int j=0;j<2048;j++)
	//	{
	//		char &d = m_data[i*2048 + j];
	//		float height = d/255.0f;
	//		verts.push_back(VertexPositionNormal());
	//	}
	//}


	//WireFrame Raster State
	{
		D3D11_RASTERIZER_DESC rasterDesc;

		rasterDesc.AntialiasedLineEnable = false;
		rasterDesc.CullMode = D3D11_CULL_BACK;
		rasterDesc.DepthBias = 0;
		rasterDesc.DepthBiasClamp = 0.0f;
		rasterDesc.DepthClipEnable = true;
		rasterDesc.FillMode = D3D11_FILL_WIREFRAME;
		rasterDesc.FrontCounterClockwise = false;
		rasterDesc.MultisampleEnable = false;
		rasterDesc.ScissorEnable = false;
		rasterDesc.SlopeScaledDepthBias = 0.0f;

		D3D->dev->CreateRasterizerState(&rasterDesc, &m_rasterState);
	}
	
	//VERTEX BUFFER
	{
		VertexPositionTexture verts[] = {
			VertexPositionTexture(XMFLOAT3( 0.0f, 0.0f,  0.0f),  XMFLOAT2(0.0f, 0.0f)),
			VertexPositionTexture(XMFLOAT3( 0.0f,  0.0f,  dims.y),  XMFLOAT2(0.0f, 1.0f)),
			VertexPositionTexture(XMFLOAT3( dims.x,  0.0f,  0.0f),  XMFLOAT2(1.0f, 0.0f)),
			VertexPositionTexture(XMFLOAT3( dims.x,  0.0f,  dims.y),  XMFLOAT2(1.0f, 1.0f))
		};	
		m_vertexBuffer = new VertexBuffer<VertexPositionTexture>(verts, 4);	
	}

	//INDEX BUFFER
	{
		USHORT indices[] = 
		{
			0, 1, 3, 2, 1, 3
		};
		m_indexBuffer = new IndexBuffer<USHORT>(indices, 6, DXGI_FORMAT_R16_UINT);
	}

	//PER RENDERABLE CONSTANT BUFFER
	{
		m_perRenderableConstantBuffer = new ConstantBuffer<PerRenderableConstantBuffer>();
		PerRenderableConstantBuffer cb;
		cb.Renderable_Translation = XMMatrixTranslation(0.0f,0.0f,0.0f);
		cb.Renderable_Scale = XMMatrixIdentity();
		cb.Renderable_Rotation = XMMatrixIdentity();
		m_perRenderableConstantBuffer->SetData(cb);
	}

	//RENDERING ASSETS
	{
		m_pixelShader = G->m_contentManager->load<PixelShader>("Shaders\\HMTerrainPixelShader.cso");
		m_vertexShader = G->m_contentManager->load<VertexShader<VertexPositionTexture>>("Shaders\\HMTerrainVertexShader.cso");
		m_hullShader = G->m_contentManager->load<HullShader>("Shaders\\HMTerrainHullShader.cso");
		m_domainShader = G->m_contentManager->load<DomainShader>("Shaders\\HMTerrainDomainShader.cso");
		m_diffuse = G->m_contentManager->load<Texture>("Textures\\Dirt_Diffuse.dds");
		m_heightMap = G->m_contentManager->load<Texture>("HeightMaps\\heightmap.bmp");
	}
}

HeightMapTerrain::~HeightMapTerrain()
{
}

int HeightMapTerrain::Render()
{
	D3D->devcon->RSSetState(m_rasterState);

	D3D->devcon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST);

	m_pixelShader->Bind();
	m_vertexShader->Bind();
	m_hullShader->Bind();
	m_domainShader->Bind();
	m_heightMap->Bind(DOMAINSHADER, 5);

	m_vertexBuffer->Bind();
	m_indexBuffer->Bind();

	D3D->devcon->DrawIndexed(m_indexBuffer->getNumIndices(), 0, 0);

	m_hullShader->Unbind();
	m_domainShader->Unbind();

	return 0;
}

void HeightMapTerrain::RenderZ()
{
	D3D->devcon->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST);

	m_pixelShader->Unbind();
	m_vertexShader->Bind();
	m_hullShader->Bind();
	m_domainShader->Bind();
	m_heightMap->Bind(DOMAINSHADER, 5);

	m_vertexBuffer->Bind();
	m_indexBuffer->Bind();

	D3D->devcon->DrawIndexed(m_indexBuffer->getNumIndices(), 0, 0);

	m_hullShader->Unbind();
	m_domainShader->Unbind();
}
