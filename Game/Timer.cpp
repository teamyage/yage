#include "stdafx.h"
#include "Timer.h"

Timer::Timer()
{
	QueryPerformanceFrequency(&m_freq);
	QueryPerformanceCounter(&m_start);
	QueryPerformanceCounter(&m_stop);
	QueryPerformanceCounter(&m_startOfGame);
	setFPSLimit(1000);
	m_lastFrameLength = 1.0/60.0;
	Start("FPSDISPLAY");
}

void Timer::Update()
{	
	QueryPerformanceCounter(&m_stop);
	m_lastFrameLength = deltaTime(m_start, m_stop);
	m_lastFrameLengthImmediate = deltaTime(m_start, m_stop);
	while(m_lastFrameLength < m_frameTime)
	{		
		boost::this_thread::sleep(boost::posix_time::microsec(100));
		QueryPerformanceCounter(&m_stop);
		m_lastFrameLength = deltaTime(m_start, m_stop);
	}
	QueryPerformanceCounter(&m_start);
	static int count = 0;
	count++;
	if(deltaTime(m_timers["FPSDISPLAY"], m_start)>=1.0)
	{
		BOOST_LOG_TRIVIAL(trace) << "FrameRate: " << getFPS() << " capable of " << getFPSImmediate() << " with " << count << " Actual Frames"; 
		Restart("FPSDISPLAY");
		count = 0;
	}
}

double Timer::getLastFrameLength() { return m_lastFrameLength; }
double Timer::getFPS() { return 1.0/m_lastFrameLength; }
double Timer::getFPSImmediate() { return 1.0/m_lastFrameLengthImmediate; }
void Timer::setFPSLimit(int fps) { m_frameTime = 1.0/double(fps); }

void Timer::Start(std::string name)
{
	LARGE_INTEGER now;
	QueryPerformanceCounter(&now);
	m_timers[name] = now;
}

double Timer::Stop(std::string name)
{
	LARGE_INTEGER now;
	QueryPerformanceCounter(&now);
	return deltaTime(m_timers[name], now);
}

double Timer::Restart(std::string name)
{
	LARGE_INTEGER now;
	QueryPerformanceCounter(&now);
	double result = deltaTime(m_timers[name], now);	
	m_timers[name] = now;
	return result;
}

//private:
inline double Timer::deltaTime(LARGE_INTEGER then, LARGE_INTEGER now)
{
	return ((now.QuadPart - then.QuadPart) * 1000.0) / (m_freq.QuadPart * 1000);
}