#include "stdafx.h"
#include "WindowManager.h"

InputSystem* WindowManager::m_inputSystem;

WindowManager::WindowManager()
{
	m_inputSystem = NULL;
	m_width = 1920;
	m_height = 1080;
	m_inputSystem = G->m_inputSystem;

	WNDCLASSEX wc;
	ZeroMemory(&wc, sizeof(WNDCLASSEX));

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = (WNDPROC) ReDirectWndProc;
	wc.hInstance = G->applicationHandle;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
	wc.lpszClassName = L"WindowClass1";

	RegisterClassEx(&wc);

	// calculate the size of the client area
	RECT wr = {0, 0, m_width, m_height};    // set the size, but not the position
	AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);    // adjust the size

	m_screenwidth = GetSystemMetrics(SM_CXSCREEN);
	m_screenheight = GetSystemMetrics(SM_CYSCREEN);

	// create the window and use the result as the handle
	windowHandle = CreateWindowEx(NULL,
		L"WindowClass1",    // name of the window class
		L"",    // title of the window
		WS_OVERLAPPEDWINDOW,    // window style
		(m_screenwidth-wr.right)/2,    // x-position of the window
		(m_screenheight-wr.bottom)/2,    // y-position of the window
		wr.right - wr.left,    // width of the window
		wr.bottom - wr.top,    // height of the window
		NULL,    // we have no parent window, NULL
		NULL,    // we aren't using menus, NULL
		G->applicationHandle,    // application handle
		NULL);    // used with multiple windows, NULL

	// display the window on the screen
	ShowWindow(windowHandle, SW_SHOW);
	SetForegroundWindow(windowHandle);
	SetFocus(windowHandle);		
}


WindowManager::~WindowManager()
{
}

HWND WindowManager::getWindowHandle()
{
	return windowHandle;
}

LRESULT CALLBACK WindowManager::ReDirectWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	return m_inputSystem->WindowProc(hWnd, uMsg, wParam, lParam);
}

int WindowManager::getWidth()
{
	return m_width;
}

int WindowManager::getHeight()
{
	return m_height;
}

int WindowManager::getNumTilesWidth()
{
	return m_width/tileSize+(m_width%tileSize>0?1:0);
}

int WindowManager::getNumTilesHeight()
{
	return m_height/tileSize+(m_height%tileSize>0?1:0);
}

float WindowManager::getAspectRatio()
{
	return (float)m_width/(float)m_height;
}