#include "stdafx.h"
#include "RenderTarget2D.h"

RenderTarget2D::RenderTarget2D() :  m_width(0),
	m_height(0),
	Format(DXGI_FORMAT_UNKNOWN),
	m_numMipLevels(0),
	m_multiSamples(0),
	m_msQuality(0),
	AutoGenMipMaps(false),
	m_arraySize(1),
	m_texture(NULL)
{
}

void RenderTarget2D::Initialize(UINT width,
								UINT height,
								DXGI_FORMAT format,
								UINT numMipLevels,
								UINT multiSamples,
								UINT msQuality,
								bool autoGenMipMaps,
								bool createUAV,
								UINT arraySize)
{
	D3D11_TEXTURE2D_DESC desc;
	desc.Width = width;
	desc.Height = height;
	desc.ArraySize = arraySize;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE|D3D11_BIND_RENDER_TARGET;
	if(createUAV)
		desc.BindFlags |= D3D11_BIND_UNORDERED_ACCESS;

	desc.CPUAccessFlags = 0;
	desc.Format = format;
	desc.MipLevels = numMipLevels;
	desc.MiscFlags = (autoGenMipMaps && numMipLevels > 1) ? D3D11_RESOURCE_MISC_GENERATE_MIPS : 0;
	desc.SampleDesc.Count = multiSamples;
	desc.SampleDesc.Quality = msQuality;
	desc.Usage = D3D11_USAGE_DEFAULT;

	if(FAILED(D3D->dev->CreateTexture2D(&desc, NULL, &m_texture)))
		BOOST_LOG_TRIVIAL(error) << "Error while creating texture for RenderTarget2D";

	D3D11_RENDER_TARGET_VIEW_DESC rtDesc;
	rtDesc.Format = format;

	rtDesc.ViewDimension = multiSamples > 1 ? D3D11_RTV_DIMENSION_TEXTURE2DMS : D3D11_RTV_DIMENSION_TEXTURE2D;
	rtDesc.Texture2D.MipSlice = 0;  

	CreateRTV(m_texture, &rtDesc);
	CreateSRV(m_texture);

	m_width = width;
	m_height = height;
	m_numMipLevels = numMipLevels;
	m_multiSamples = multiSamples;
	Format = format;
	AutoGenMipMaps = autoGenMipMaps;
	m_arraySize = arraySize;

	if (createUAV)
		CreateUAV(m_texture);

};

void RenderTarget2D::Release()
{	
	//for(ID3D11RenderTargetView* slice : ArraySlices)
	//	SAFE_RELEASE(slice);

	//SAFE_RELEASE(texture);
	//SAFE_RELEASE(RTView);
	//SAFE_RELEASE(SRView);
	//SAFE_RELEASE(UAView);

	//ArraySlices.clear();
}