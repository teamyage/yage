#include "stdafx.h"
#include "Cell.h"

Cell::Cell()
{
	m_vertexBuffer = NULL;
	m_perCellConstantBuffer.Renderable_Translation = XMMatrixTranslation(0.0f,0.0f,0.0f);
	cellWidth = XMLoadUInt3(&XMUINT3(CELLWIDTH,CELLWIDTH,CELLWIDTH));

	data = new BYTE**[CELLWIDTHP1];
	for(int x=0;x<CELLWIDTHP1;x++)
	{
		data[x] = new BYTE*[CELLWIDTHP1];
		for(int j=0;j<CELLWIDTHP1;j++)
			data[x][j] = new BYTE[CELLWIDTHP1];
	}
	polycount = 0;
	indices.reserve(55000);
	verts.reserve(40000);
}

Cell::~Cell()
{
	SAFE_RELEASE_DELETE(m_vertexBuffer);
		
	for(int x=0;x<CELLWIDTHP1;x++)
	{		
		for(int j=0;j<CELLWIDTHP1;j++)
			delete data[x][j];
		delete data[x];
	}
	delete data;
}

bool Cell::Generate()
{	
	unsigned int posx = m_cid.position_x*CELLWIDTH;
	unsigned int posy = m_cid.position_y*CELLWIDTH;
	unsigned int posz = m_cid.position_z*CELLWIDTH;

	for(int x=0;x<CELLWIDTHP1;x++)
	{
		double worldx = posx+x;
		for(int z=0;z<CELLWIDTHP1;z++)
		{
			double worldz = posz+z;
			double heightAtXZ = 64*(1+glm::perlin(glm::vec2(worldx/60.0, worldz/60.0)))/2.0;
			for(int y=0;y<CELLWIDTHP1;y++)
			{
				BYTE &d = data[x][y][z];
				d = (posy+y<heightAtXZ)?1:0;
			}	
		}
	}
	
	for(int x=0;x<CELLWIDTH;x++)
	for(int y=0;y<CELLWIDTH;y++)
	for(int z=0;z<CELLWIDTH;z++)
	{
		BYTE& here = data[x][y][z];
		BYTE& nextX = data[x+1][y][z];
		BYTE& nextY = data[x][y+1][z];
		BYTE& nextZ = data[x][y][z+1];
		

		if(hasDensityChange(here, nextX))
			AddQuad(x,y,z,(here==0)?XP:XN);

		if(hasDensityChange(here, nextY))
			AddQuad(x,y,z,(here==0)?YP:YN);

		if(hasDensityChange(here, nextZ))
			AddQuad(x,y,z,(here==0)?ZP:ZN);
	}

	//this cell needs recycling if its empty
	return indices.empty();
}

void Cell::UpdateConstantBuffer()
{

}

void Cell::Render()
{
	if(polycount>0)
	{
		m_indexBuffer->Bind();
		m_vertexBuffer->Bind();
		D3D->devcon->DrawIndexed(m_indexBuffer->getNumIndices(), 0, 0);
	}
}

PerCellConstantBuffer& Cell::GetConstantBuffer()
{
	return m_perCellConstantBuffer;
}

void Cell::SetCellId(CellId cid)
{
	XMUINT3 position = XMUINT3(cid.position_x, cid.position_y, cid.position_z);

	m_perCellConstantBuffer.Renderable_Translation = XMMatrixTranslationFromVector(cellWidth*XMLoadUInt3(&position));
	m_cid = cid;
}

inline bool Cell::hasDensityChange(BYTE A,BYTE B)
{
	return (A^B)!=0;
}
CellId Cell::getId()
{
	return m_cid;
}
void Cell::PrepareForRendering()
{
	if(!verts.empty())
	{
		BOOST_LOG_TRIVIAL(trace) << "Cell used "<< verts.size() << " verts and " << indices.size() << " indices!";
		m_indexBuffer = new IndexBuffer<USHORT>(indices, DXGI_FORMAT_R16_UINT);
		m_vertexBuffer = new VertexBuffer<VertexPositionNormal>(verts);
		polycount = (int)indices.size()/3;
	}
	verts.clear();	
	indices.clear();
}

void Cell::AddQuad(unsigned int x, unsigned int y, unsigned int z, iorder direction)
{
#pragma warning( push )
#pragma warning( disable : 4244 )

	static const USHORT indexOrder [6][6] =
	{
		{0,1,3,0,3,2},
		{0,3,1,0,2,3},
				
		{0,3,1,0,2,3},
		{0,1,3,0,3,2},

		{0,1,3,0,3,2},
		{0,3,1,0,2,3},
	};

	static const XMVECTOR voffsets [3][4] =
	{
		{
			XMLoadFloat3(&XMFLOAT3(1, 0, 0)),
			XMLoadFloat3(&XMFLOAT3(1, 0, 1)),
			XMLoadFloat3(&XMFLOAT3(1, 1, 0)),
			XMLoadFloat3(&XMFLOAT3(1, 1, 1))
		},
		{
			XMLoadFloat3(&XMFLOAT3(0, 1, 0)),
			XMLoadFloat3(&XMFLOAT3(0, 1, 1)),
			XMLoadFloat3(&XMFLOAT3(1, 1, 0)),
			XMLoadFloat3(&XMFLOAT3(1, 1, 1))
		},	
		{
			XMLoadFloat3(&XMFLOAT3(0, 0, 1)),
			XMLoadFloat3(&XMFLOAT3(0, 1, 1)),
			XMLoadFloat3(&XMFLOAT3(1, 0, 1)),
			XMLoadFloat3(&XMFLOAT3(1, 1, 1))
		}
	};

	static const XMVECTOR majorAxis[6] = 
	{
		XMLoadFloat3(&XMFLOAT3( 1.0f,  0.0f,  0.0f)),
		XMLoadFloat3(&XMFLOAT3(-1.0f,  0.0f,  0.0f)),
		XMLoadFloat3(&XMFLOAT3( 0.0f,  1.0f,  0.0f)),
		XMLoadFloat3(&XMFLOAT3( 0.0f, -1.0f,  0.0f)),
		XMLoadFloat3(&XMFLOAT3( 0.0f,  0.0f,  1.0f)),
		XMLoadFloat3(&XMFLOAT3( 0.0f,  0.0f, -1.0f))
	};
	
	int currentVertBufferSize = (int)verts.size();
	for(unsigned int i=0;i<4;i++)
		verts.push_back(VertexPositionNormal(XMLoadFloat3(&XMFLOAT3(x, y, z))+voffsets[direction/2][i], majorAxis[direction]));

	for(unsigned int i=0;i<6;i++)
		indices.push_back(currentVertBufferSize+indexOrder[direction][i]);

#pragma warning( pop ) 	

}