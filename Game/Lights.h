#pragma once

#include "stdafx.h"
#include "Float3.h"

using namespace DirectX;

struct PointLight
{
	Float3 m_position;
	float m_radius;
	Float3 m_color;
	float m_intensity;
};

struct DirectionalLight
{
	Float3 m_rayDirection;
	float m_intensity;
	Float3 m_color;
	float padding;
};
