#pragma once

#include "d3dsystem.h"
#include "ShaderTypes.h"

class IConstantBuffer : public D3DSystem
{
protected:
	ID3D11Buffer *m_buffer;

	friend class ComputeShader;
	friend class PixelShader;
	friend class IVertexShader;
	friend class DomainShader;
	friend class GeometryShader;
	friend class HullShader;
};

template <class T>
class ConstantBuffer : public IConstantBuffer
{
public:
	ConstantBuffer()
	{
		m_buffer = NULL;
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		bd.ByteWidth = sizeof(T);
		bd.Usage = D3D11_USAGE_DYNAMIC;
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		if(FAILED(D3D->dev->CreateBuffer(&bd, nullptr, &m_buffer)))
			BOOST_LOG_TRIVIAL(error) << "Error creating Constant Buffer!"; 
	}

	void SetData(T const& value)
	{
		D3D11_MAPPED_SUBRESOURCE ms;
		D3D->devcon->Map(m_buffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);
		memcpy(ms.pData, &value, sizeof(T));
		D3D->devcon->Unmap(m_buffer, NULL);
	}

	void BindCB(ShaderTypes shaderType, unsigned int startSlot, unsigned int numBuffers=1)
	{
		switch(shaderType){
		case VERTEXSHADER:
			D3D->devcon->VSSetConstantBuffers(startSlot, numBuffers, &m_buffer);
			break;
		case PIXELSHADER:
			D3D->devcon->PSSetConstantBuffers(startSlot, numBuffers, &m_buffer);
			break;
		case GEOMETRYSHADER:
			D3D->devcon->GSSetConstantBuffers(startSlot, numBuffers, &m_buffer);
			break;
		case HULLSHADER:
			D3D->devcon->HSSetConstantBuffers(startSlot, numBuffers, &m_buffer);
			break;
		case COMPUTESHADER:
			D3D->devcon->CSSetConstantBuffers(startSlot, numBuffers, &m_buffer);
			break;
		case DOMAINSHADER:
			D3D->devcon->DSSetConstantBuffers(startSlot, numBuffers, &m_buffer);
			break;
		default:
			BOOST_LOG_TRIVIAL(error) << "Attempted binding of constant buffer to non-existant ShaderType";
		}
	}


private:
	ConstantBuffer(ConstantBuffer const&);
	ConstantBuffer& operator= (ConstantBuffer const&);
};
