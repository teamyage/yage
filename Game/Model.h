#pragma once
#include "Renderable.h"
#include "VertexTypes.h"
#include "ContentManager.h"
#include "BinaryLoader.h"
#include "Model.h"
#include "ModelData.h"
#include <algorithm>

class Model : public IRenderable, public BinaryLoader
{
public:
	Model(std::string filename, bool ccw=true, bool pmalpha=false);
	~Model();

	int Render();
	void RenderZ();
private:
	std::vector<Mesh*>			m_meshes;
	std::wstring				m_name;
};
