#pragma once

class Timer
{
public:
	Timer();

	void Update();
	double getLastFrameLength();
	double getFPS();
	double getFPSImmediate();
	void setFPSLimit(int fps);
	void Start(std::string name);
	double Stop(std::string name);	
	double Restart(std::string name);

private:
	inline double deltaTime(LARGE_INTEGER then, LARGE_INTEGER now);

	boost::unordered_map<std::string, LARGE_INTEGER> m_timers;
	double m_frameTime;	
	double m_lastFrameLength;
	double m_lastFrameLengthImmediate;
	LARGE_INTEGER m_start;
	LARGE_INTEGER m_stop;
	LARGE_INTEGER m_freq;
	LARGE_INTEGER m_startOfGame;
};

