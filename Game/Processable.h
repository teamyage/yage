#pragma once

#include "stdafx.h"

class IProcessableSystem
{
public:
	virtual void Process() = 0;
};