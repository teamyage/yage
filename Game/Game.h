#pragma once

#include "basegame.h"
#include "MenuState.h"
#include "PlayingState.h"
#include "TiledForwardRenderer.h"
#include "VertexShader.h"
#include "PixelShader.h"
#include "GeometryShader.h"
#include "DomainShader.h"
#include "HullShader.h"
#include "ComputeShader.h"
#include "Texture.h"

class Game : public BaseGame
{
public:
	Game();
	~Game();

	void Initialize();
	void Loop();
	void Release();
};
