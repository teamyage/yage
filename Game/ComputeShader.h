#pragma once
#include "BinaryLoader.h"
#include "ConstantBuffer.h"
#include "SRV.h"
#include "UAV.h"

class ComputeShader : public BinaryLoader, public D3DSystem
{
public:
	ComputeShader(std::string filename);
	~ComputeShader();

	void Bind();

	void Release();
	void Dispatch(unsigned int x, unsigned int y, unsigned int z);

private:
	ID3D11ComputeShader *m_computeShader;
};

