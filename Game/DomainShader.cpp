#include "stdafx.h"
#include "DomainShader.h"

DomainShader::DomainShader(std::string filename) : BinaryLoader(filename)
{
	if(m_data)
	{
		BOOST_LOG_TRIVIAL(trace) << "Creating DomainShader for " << m_filename;
		HRESULT hr = D3D->dev->CreateDomainShader(m_data, m_dataLength, nullptr, &m_domainShader);

		if (FAILED(hr)) 
			BOOST_LOG_TRIVIAL(error) << "Error while creating DomainShader: " << m_filename;
	}
}


DomainShader::~DomainShader()
{
	BOOST_LOG_TRIVIAL(trace) << "Releasing DomainShader for " << m_filename;
	SAFE_RELEASE(m_domainShader);
}

void DomainShader::Bind()
{
	D3D->devcon->DSSetShader(m_domainShader, 0, 0);
}

void DomainShader::Unbind()
{
	D3D->devcon->DSSetShader(NULL, 0, 0);
}

void DomainShader::Release()
{
	m_domainShader->Release();
}