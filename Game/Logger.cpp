#include "stdafx.h"
#include "Logger.h"

VOID Logger::InitializeLogger(std::string p_LogLevel)
{
	using namespace boost::log;
	using namespace boost::log::sinks;
	using namespace boost::log::expressions;
	using namespace boost::log::trivial;
	using namespace boost::posix_time;

	bool BadLogLevel = false;

	// Needed for "TimeStamp", also provides LineID, ThreadID, ProcessID
	add_common_attributes();

	boost::shared_ptr<core> core = core::get();
	boost::shared_ptr<synchronous_sink<debug_output_backend>> sink(new synchronous_sink<debug_output_backend>());
	 
	sink->set_filter(is_debugger_present());	
	{
		std::transform(p_LogLevel.begin(), p_LogLevel.end(), p_LogLevel.begin(), ::tolower);
		severity_level LogLevel;

		if(p_LogLevel.compare("trace") == 0) LogLevel = trivial::trace;
		else if (p_LogLevel.compare("debug") == 0) LogLevel = trivial::debug;
		else if (p_LogLevel.compare("info") == 0) LogLevel = trivial::info;
		else if (p_LogLevel.compare("warning") == 0) LogLevel = trivial::warning;
		else if (p_LogLevel.compare("error") == 0) LogLevel = trivial::error;
		else if (p_LogLevel.compare("fatal") == 0) LogLevel = trivial::fatal;
		else 
		{
			LogLevel = trivial::warning;
			BadLogLevel = true;
		}
		core->set_filter(severity >= LogLevel);
	}	
	sink->set_formatter (stream<<"["<<format_date_time<ptime >("TimeStamp", "%H:%M:%S.%f")<<"] <"<<attr<severity_level>("Severity")<<"> : "<<smessage<<"\n");	
	core->add_sink(sink);

	if(BadLogLevel)
		BOOST_LOG_TRIVIAL(fatal) << "UNKNOWN LOG LEVEL REQUESTED!";
}
