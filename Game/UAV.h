#pragma once

#include "D3DSystem.h"

struct UAV : public D3DSystem
{
	UAV();
	static void BindNULLUAV(UINT NumViews);
 	void BindUAV(UINT slot, UINT initialCounterValues=NULL);
	void CreateUAV(ID3D11Resource *resource, const D3D11_UNORDERED_ACCESS_VIEW_DESC *pDesc=NULL);
private:
	ID3D11UnorderedAccessView *m_UAV;

	friend class ComputeShader;
};