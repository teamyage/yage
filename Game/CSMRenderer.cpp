#include "stdafx.h"
#include "CSMRenderer.h"

CSMRenderer::CSMRenderer()
{
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Initialize CSM's
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	{
		unsigned int ShadowMapSize = 2048;
		m_shadowMaps = new DepthStencilBuffer();
		m_shadowMaps->Initialize(ShadowMapSize, ShadowMapSize, DXGI_FORMAT_D32_FLOAT, true, 1, 0, NUM_CASCADES);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Prepare Orthogonal Shadow Camera with fixed Frustum
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	{
		m_shadowCamera = new OrthographicCamera(-0.5f, -0.5f, 0.5f, 0.5f, 0.0f, 1.0f);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Setup ConstantBuffers for rendering
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	{
		m_perFrameConstantBuffer = new ConstantBuffer<PerFrameConstantBuffer>();
		m_perRenderableConstantBuffer = new ConstantBuffer<PerRenderableConstantBuffer>();	
		m_shadowConstantBuffer = new ConstantBuffer<ShadowsConstantBuffer>();
	}

	D3D11_SAMPLER_DESC sampDesc;

	sampDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	sampDesc.MipLODBias = 0.0f;
	sampDesc.MaxAnisotropy = 1;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_LESS_EQUAL;
	sampDesc.BorderColor[0] = sampDesc.BorderColor[1] = sampDesc.BorderColor[2] = sampDesc.BorderColor[3] = 0;
	sampDesc.MinLOD = 0;
	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;

	D3D->dev->CreateSamplerState(&sampDesc, &m_shadowSampler);
}

CSMRenderer::~CSMRenderer()
{
}

void CSMRenderer::RenderFrame(std::vector<IRenderable*> &renderables, std::vector<IRenderableSystem*> &renderableSystems)
{
	const float sMapSize = 2048.0f;

	const float MinDistance = 0.0f;
	const float MaxDistance = 1.0f;

	// Compute the split distances based on the partitioning mode
	float CascadeSplits[4] = { 0.05f, 0.15f, 0.5f, 1.0f };

	Float4x4 globalShadowMatrix = GetShadowMatrix(G->m_lightingSystem->GetShadowCastingDirectionalLight().m_rayDirection);

	m_shadowConstantBufferData.CameraPosWS = G->m_camera->GetPosition();
	m_shadowConstantBufferData.ShadowMatrix = globalShadowMatrix;

	m_perRenderableConstantBuffer->BindCB(VERTEXSHADER, 1);

	m_shadowMaps->ClearAllSlices(D3D11_CLEAR_DEPTH|D3D11_CLEAR_STENCIL, 1.0f, 0);

	// Render the meshes to each cascade
	for(unsigned int cascadeIdx = 0; cascadeIdx < NUM_CASCADES; cascadeIdx++)
	{
		// Set the viewport
		D3D11_VIEWPORT viewport;
		viewport.TopLeftX = 0.0f;
		viewport.TopLeftY = 0.0f;
		viewport.Width = sMapSize;
		viewport.Height = sMapSize;
		viewport.MinDepth = 0.0f;
		viewport.MaxDepth = 1.0f;
		D3D->devcon->RSSetViewports(1, &viewport);

		m_shadowMaps->BindAsOnlyRenderTarget(cascadeIdx);

		// Get the 8 points of the view frustum in world space
		Float3 frustumCornersWS[8] =
		{
			Float3(-1.0f,  1.0f, 0.0f),
			Float3( 1.0f,  1.0f, 0.0f),
			Float3( 1.0f, -1.0f, 0.0f),
			Float3(-1.0f, -1.0f, 0.0f),
			Float3(-1.0f,  1.0f, 1.0f),
			Float3( 1.0f,  1.0f, 1.0f),
			Float3( 1.0f, -1.0f, 1.0f),
			Float3(-1.0f, -1.0f, 1.0f),
		};

		float prevSplitDist = cascadeIdx == 0 ? MinDistance : CascadeSplits[cascadeIdx - 1];
		float splitDist = CascadeSplits[cascadeIdx];
		Float4x4 invViewProj = Float4x4::Invert(G->m_camera->GetViewProjection());
		for(UINT i = 0; i < 8; ++i)
			frustumCornersWS[i] = Float3::Transform(frustumCornersWS[i], invViewProj);
		// Get the corners of the current cascade slice of the view frustum
		for(UINT i = 0; i < 4; ++i)
		{
			Float3 cornerRay = frustumCornersWS[i + 4] - frustumCornersWS[i];
			Float3 nearCornerRay = cornerRay * prevSplitDist;
			Float3 farCornerRay = cornerRay * splitDist;
			frustumCornersWS[i + 4] = frustumCornersWS[i] + farCornerRay;
			frustumCornersWS[i] = frustumCornersWS[i] + nearCornerRay;
		}

		// Calculate the centroid of the view frustum slice
		Float3 frustumCenter = 0.0f;
		for(UINT i = 0; i < 8; ++i)
			frustumCenter = frustumCenter + frustumCornersWS[i];
		frustumCenter *=  1.0f / 8.0f;

		// Pick the up vector to use for the light camera
		Float3 upDir = Float3(0.0f, 1.0f, 0.0f);

		Float3 minExtents;
		Float3 maxExtents;

		// Calculate the radius of a bounding sphere surrounding the frustum corners
		float sphereRadius = 0.0f;
		for(unsigned int i = 0; i < 8; ++i)
		{
			float dist = Float3::Length(frustumCornersWS[i] - frustumCenter);
			sphereRadius = std::max(sphereRadius, dist);
		}

		sphereRadius = std::ceil(sphereRadius * 16.0f) / 16.0f;

		maxExtents = Float3(sphereRadius, sphereRadius, sphereRadius);
		minExtents = -maxExtents;
		

		Float3 cascadeExtents = maxExtents - minExtents;

		// Get position of the shadow camera
		Float3 shadowCameraPos = frustumCenter + G->m_lightingSystem->GetShadowCastingDirectionalLight().m_rayDirection * -minExtents.z;

		// Come up with a new orthographic camera for the shadow caster
		OrthographicCamera shadowCamera(minExtents.x, minExtents.y, maxExtents.x,
			maxExtents.y, 0.0f, cascadeExtents.z);
		shadowCamera.SetLookAt(shadowCameraPos, frustumCenter, upDir);

		// Create the rounding matrix, by projecting the world-space origin and determining
		// the fractional offset in texel space
		XMMATRIX shadowMatrix = shadowCamera.GetViewProjection().ToSIMD();
		XMVECTOR shadowOrigin = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
		shadowOrigin = XMVector4Transform(shadowOrigin, shadowMatrix);
		shadowOrigin = XMVectorScale(shadowOrigin, sMapSize / 2.0f);

		XMVECTOR roundedOrigin = XMVectorRound(shadowOrigin);
		XMVECTOR roundOffset = XMVectorSubtract(roundedOrigin, shadowOrigin);
		roundOffset = XMVectorScale(roundOffset, 2.0f / sMapSize);
		roundOffset = XMVectorSetZ(roundOffset, 0.0f);
		roundOffset = XMVectorSetW(roundOffset, 0.0f);

		XMMATRIX shadowProj = shadowCamera.GetProjection().ToSIMD();
		shadowProj.r[3] = XMVectorAdd(shadowProj.r[3], roundOffset);
		shadowCamera.SetProjection(shadowProj);
		
		PerFrameConstantBuffer perFrameConstantBuffer;
		perFrameConstantBuffer.Camera_View = shadowCamera.GetView().ToSIMD();
		perFrameConstantBuffer.Camera_Projection = shadowCamera.GetProjection().ToSIMD();
		perFrameConstantBuffer.Camera_Position = G->m_camera->GetPosition().ToSIMD();
		m_perFrameConstantBuffer->SetData(perFrameConstantBuffer);
		m_perFrameConstantBuffer->BindCB(VERTEXSHADER, 0);		

		for(IRenderable* r : renderables)
		{
			D3D->devcon->IASetPrimitiveTopology(r->GetPrimitiveType());
			r->UpdateConstantBuffer();
			m_perRenderableConstantBuffer->SetData(r->GetConstantBuffer());
			m_perRenderableConstantBuffer->BindCB(VERTEXSHADER, 1);
			r->RenderZ();
		}

		for(IRenderableSystem* r : renderableSystems)
			r->RenderZ();

		// Apply the scale/offset matrix, which transforms from [-1,1]
		// post-projection space to [0,1] UV space
		XMMATRIX texScaleBias;
		texScaleBias.r[0] = XMVectorSet(0.5f,  0.0f, 0.0f, 0.0f);
		texScaleBias.r[1] = XMVectorSet(0.0f, -0.5f, 0.0f, 0.0f);
		texScaleBias.r[2] = XMVectorSet(0.0f,  0.0f, 1.0f, 0.0f);
		texScaleBias.r[3] = XMVectorSet(0.5f,  0.5f, 0.0f, 1.0f);

		shadowMatrix = shadowCamera.GetViewProjection().ToSIMD();
		shadowMatrix = XMMatrixMultiply(shadowMatrix, texScaleBias);

		// Store the split distance in terms of view space depth
		const float clipDist = G->m_camera->GetFarClipPlane() - G->m_camera->GetNearClipPlane();
		m_shadowConstantBufferData.CascadeSplits[cascadeIdx] = G->m_camera->GetNearClipPlane() + splitDist * clipDist;

		// Calculate the position of the lower corner of the cascade partition, in the UV space
		// of the first cascade partition
		Float4x4 invCascadeMat = Float4x4::Invert(shadowMatrix);
		Float3 cascadeCorner = Float3::Transform(Float3(0.0f, 0.0f, 0.0f), invCascadeMat);
		cascadeCorner = Float3::Transform(cascadeCorner, globalShadowMatrix);

		// Do the same for the upper corner
		Float3 otherCorner = Float3::Transform(Float3(1.0f, 1.0f, 1.0f), invCascadeMat);
		otherCorner = Float3::Transform(otherCorner, globalShadowMatrix);

		// Calculate the scale and offset
		Float3 cascadeScale = Float3(1.0f, 1.0f, 1.0f) / (otherCorner - cascadeCorner);
		m_shadowConstantBufferData.CascadeOffsets[cascadeIdx] = Float4(-cascadeCorner, 0.0f);
		m_shadowConstantBufferData.CascadeScales[cascadeIdx] = Float4(cascadeScale, 1.0f);
	}
	m_shadowConstantBuffer->SetData(m_shadowConstantBufferData);
}

Float4x4 CSMRenderer::GetShadowMatrix(Float3 &lightDirection)
{
	// Get the 8 points of the view frustum in world space
	Float3 frustumCorners[8] =
	{
		Float3(-1.0f,  1.0f, 0.0f),
		Float3( 1.0f,  1.0f, 0.0f),
		Float3( 1.0f, -1.0f, 0.0f),
		Float3(-1.0f, -1.0f, 0.0f),
		Float3(-1.0f,  1.0f, 1.0f),
		Float3( 1.0f,  1.0f, 1.0f),
		Float3( 1.0f, -1.0f, 1.0f),
		Float3(-1.0f, -1.0f, 1.0f),
	};

	Float4x4 invViewProj = Float4x4::Invert(G->m_camera->GetViewProjection());
	Float3 frustumCenter = 0.0f;
	for(unsigned int i = 0; i < 8; ++i)
	{
		frustumCorners[i] = Float3::Transform(frustumCorners[i], invViewProj);
		frustumCenter += frustumCorners[i];
	}

	frustumCenter /= 8.0f;

	// Pick the up vector to use for the light camera
	Float3 upDir = Float3(0.0f, 1.0f, 0.0f);

	// Create a temporary view matrix for the light
	Float3 lightCameraPos = frustumCenter;
	Float3 lookAt = frustumCenter - lightDirection;
	Float4x4 lightView = XMMatrixLookAtLH(lightCameraPos.ToSIMD(), lookAt.ToSIMD(), upDir.ToSIMD());

	// Get position of the shadow camera
	Float3 shadowCameraPos = frustumCenter + lightDirection * -0.5f;

	m_shadowCamera->SetLookAt(shadowCameraPos, frustumCenter, upDir);

	Float4x4 texScaleBias = Float4x4::ScaleMatrix(Float3(0.5f, -0.5f, 1.0f));
	texScaleBias.SetTranslation(Float3(0.5f, 0.5f, 0.0f));
	return m_shadowCamera->GetViewProjection() * texScaleBias;
}
