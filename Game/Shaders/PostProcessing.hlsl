#define tileSize 16

Texture2DMS<float4> preProcessingBuffer : register(t0);
Texture2DArray<float> shadowMap0 : register(t1);
RWTexture2D<float4> backBuffer : register(u0);

[numthreads(tileSize, tileSize, 1)]
void CShader(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex )
{
	backBuffer[DTid.xy] = preProcessingBuffer[DTid.xy];
}