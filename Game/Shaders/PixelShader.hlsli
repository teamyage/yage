#include "Shader.hlsli"
#include "RenderSettings.hlsli"
#include "Lighting.hlsli"
#include "Shadows.hlsli"
#include "Lights.hlsli"
#include "TiledLightingDefines.hlsli"

SamplerState ss; 
SamplerComparisonState ShadowSampler : register(s1);

ByteAddressBuffer HeadBuffer : register(t0);
ByteAddressBuffer LightIndexBuffer : register(t1);
StructuredBuffer<PointLight> PointLights: register(t2);
StructuredBuffer<DirectionalLight> DirectionalLights: register(t3);
Texture2DArray ShadowMap : register(t4);

#define FilterSize_ 3

static const float W[3][3] =
{
    { 0.5,1.0,0.5, },
    { 1.0,1.0,1.0, },
    { 0.5,1.0,0.5, }
};

//-------------------------------------------------------------------------------------------------
// Samples the shadow map with a fixed-size PCF kernel optimized with GatherCmp. Uses code
// from "Fast Conventional Shadow Filtering" by Holger Gruen, in GPU Pro.
//-------------------------------------------------------------------------------------------------
float SampleShadowMapFixedSizePCF(in float3 shadowPos, in float3 shadowPosDX,
                         in float3 shadowPosDY, in uint cascadeIdx) {
    float2 shadowMapSize;
    float numSlices;
    ShadowMap.GetDimensions(shadowMapSize.x, shadowMapSize.y, numSlices);

    float lightDepth = shadowPos.z;

    const float bias = 0.002;

    lightDepth -= bias;

    const int FS_2 = FilterSize_ / 2;

    float2 tc = shadowPos.xy;

    float4 s = 0.0f;
    float2 stc = (shadowMapSize * tc.xy) + float2(0.5f, 0.5f);
    float2 tcs = floor(stc);
    float2 fc;
    int row;
    int col;
    float w = 0.0f;
    float4 v1[FS_2 + 1];
    float2 v0[FS_2 + 1];

    fc.xy = stc - tcs;
    tc.xy = tcs / shadowMapSize;

    for(row = 0; row < FilterSize_; ++row)
        for(col = 0; col < FilterSize_; ++col)
            w += W[row][col];

    // -- loop over the rows
    [unroll]
    for(row = -FS_2; row <= FS_2; row += 2)
    {
        [unroll]
        for(col = -FS_2; col <= FS_2; col += 2)
        {
            float value = W[row + FS_2][col + FS_2];

            if(col > -FS_2)
                value += W[row + FS_2][col + FS_2 - 1];

            if(col < FS_2)
                value += W[row + FS_2][col + FS_2 + 1];

            if(row > -FS_2) {
                value += W[row + FS_2 - 1][col + FS_2];

                if(col < FS_2)
                    value += W[row + FS_2 - 1][col + FS_2 + 1];

                if(col > -FS_2)
                    value += W[row + FS_2 - 1][col + FS_2 - 1];
            }

            if(value != 0.0f)
            {
                float sampleDepth = lightDepth;

                v1[(col + FS_2) / 2] = ShadowMap.GatherCmp(ShadowSampler, float3(tc.xy, cascadeIdx),
                                                                sampleDepth, int2(col, row));
            }
            else
                v1[(col + FS_2) / 2] = 0.0f;

            if(col == -FS_2)
            {
                s.x += (1.0f - fc.y) * (v1[0].w * (W[row + FS_2][col + FS_2]
                                        - W[row + FS_2][col + FS_2] * fc.x)
                                        + v1[0].z * (fc.x * (W[row + FS_2][col + FS_2]
                                        - W[row + FS_2][col + FS_2 + 1.0f])
                                        + W[row + FS_2][col + FS_2 + 1]));
                s.y += fc.y * (v1[0].x * (W[row + FS_2][col + FS_2]
                                        - W[row + FS_2][col + FS_2] * fc.x)
                                        + v1[0].y * (fc.x * (W[row + FS_2][col + FS_2]
                                        - W[row + FS_2][col + FS_2 + 1])
                                        +  W[row + FS_2][col + FS_2 + 1]));
                if(row > -FS_2)
                {
                    s.z += (1.0f - fc.y) * (v0[0].x * (W[row + FS_2 - 1][col + FS_2]
                                            - W[row + FS_2 - 1][col + FS_2] * fc.x)
                                            + v0[0].y * (fc.x * (W[row + FS_2 - 1][col + FS_2]
                                            - W[row + FS_2 - 1][col + FS_2 + 1])
                                            + W[row + FS_2 - 1][col + FS_2 + 1]));
                    s.w += fc.y * (v1[0].w * (W[row + FS_2 - 1][col + FS_2]
                                        - W[row + FS_2 - 1][col + FS_2] * fc.x)
                                        + v1[0].z * (fc.x * (W[row + FS_2 - 1][col + FS_2]
                                        - W[row + FS_2 - 1][col + FS_2 + 1])
                                        + W[row + FS_2 - 1][col + FS_2 + 1]));
                }
            }
            else if(col == FS_2)
            {
                s.x += (1 - fc.y) * (v1[FS_2].w * (fc.x * (W[row + FS_2][col + FS_2 - 1]
                                        - W[row + FS_2][col + FS_2]) + W[row + FS_2][col + FS_2])
                                        + v1[FS_2].z * fc.x * W[row + FS_2][col + FS_2]);
                s.y += fc.y * (v1[FS_2].x * (fc.x * (W[row + FS_2][col + FS_2 - 1]
                                        - W[row + FS_2][col + FS_2] ) + W[row + FS_2][col + FS_2])
                                        + v1[FS_2].y * fc.x * W[row + FS_2][col + FS_2]);
                if(row > -FS_2) {
                    s.z += (1 - fc.y) * (v0[FS_2].x * (fc.x * (W[row + FS_2 - 1][col + FS_2 - 1]
                                        - W[row + FS_2 - 1][col + FS_2])
                                        + W[row + FS_2 - 1][col + FS_2])
                                        + v0[FS_2].y * fc.x * W[row + FS_2 - 1][col + FS_2]);
                    s.w += fc.y * (v1[FS_2].w * (fc.x * (W[row + FS_2 - 1][col + FS_2 - 1]
                                        - W[row + FS_2 - 1][col + FS_2])
                                        + W[row + FS_2 - 1][col + FS_2])
                                        + v1[FS_2].z * fc.x * W[row + FS_2 - 1][col + FS_2]);
                }
            }
            else
            {
                s.x += (1 - fc.y) * (v1[(col + FS_2) / 2].w * (fc.x * (W[row + FS_2][col + FS_2 - 1]
                                    - W[row + FS_2][col + FS_2 + 0] ) + W[row + FS_2][col + FS_2 + 0])
                                    + v1[(col + FS_2) / 2].z * (fc.x * (W[row + FS_2][col + FS_2 - 0]
                                    - W[row + FS_2][col + FS_2 + 1]) + W[row + FS_2][col + FS_2 + 1]));
                s.y += fc.y * (v1[(col + FS_2) / 2].x * (fc.x * (W[row + FS_2][col + FS_2-1]
                                    - W[row + FS_2][col + FS_2 + 0]) + W[row + FS_2][col + FS_2 + 0])
                                    + v1[(col + FS_2) / 2].y * (fc.x * (W[row + FS_2][col + FS_2 - 0]
                                    - W[row + FS_2][col + FS_2 + 1]) + W[row + FS_2][col + FS_2 + 1]));
                if(row > -FS_2) {
                    s.z += (1 - fc.y) * (v0[(col + FS_2) / 2].x * (fc.x * (W[row + FS_2 - 1][col + FS_2 - 1]
                                            - W[row + FS_2 - 1][col + FS_2 + 0]) + W[row + FS_2 - 1][col + FS_2 + 0])
                                            + v0[(col + FS_2) / 2].y * (fc.x * (W[row + FS_2 - 1][col + FS_2 - 0]
                                            - W[row + FS_2 - 1][col + FS_2 + 1]) + W[row + FS_2 - 1][col + FS_2 + 1]));
                    s.w += fc.y * (v1[(col + FS_2) / 2].w * (fc.x * (W[row + FS_2 - 1][col + FS_2 - 1]
                                            - W[row + FS_2 - 1][col + FS_2 + 0]) + W[row + FS_2 - 1][col + FS_2 + 0])
                                            + v1[(col + FS_2) / 2].z * (fc.x * (W[row + FS_2 - 1][col + FS_2 - 0]
                                            - W[row + FS_2 - 1][col + FS_2 + 1]) + W[row + FS_2 - 1][col + FS_2 + 1]));
                }
            }

            if(row != FS_2)
                v0[(col + FS_2) / 2] = v1[(col + FS_2) / 2].xy;
        }
    }

    return dot(s, 1.0f) / w;
}

//-------------------------------------------------------------------------------------------------
// Samples the appropriate shadow map cascade
//-------------------------------------------------------------------------------------------------
float SampleShadowCascade(in float3 shadowPosition, in float3 shadowPosDX,
                           in float3 shadowPosDY, in uint cascadeIdx,
                           in uint2 screenPos)
{
    shadowPosition += CascadeOffsets[cascadeIdx].xyz;
    shadowPosition *= CascadeScales[cascadeIdx].xyz;

    shadowPosDX *= CascadeScales[cascadeIdx].xyz;
    shadowPosDY *= CascadeScales[cascadeIdx].xyz;

    return SampleShadowMapFixedSizePCF(shadowPosition, shadowPosDX, shadowPosDY, cascadeIdx);
}

//-------------------------------------------------------------------------------------------------
// Computes the visibility term by performing the shadow test
//-------------------------------------------------------------------------------------------------
float ShadowVisibility(in float3 positionWS, in float depthVS, in float nDotL, in float3 normal,
                        in uint2 screenPos)
{
	uint cascadeIdx = 0;

	// Figure out which cascade to sample from
	[unroll]
	for(uint i = 0; i < 3; ++i)
	{
		[flatten]
		if(depthVS > CascadeSplits[i])
			cascadeIdx = i + 1;
	}

    // Project into shadow space
	float3 shadowPosition = mul(ShadowMatrix, float4(positionWS, 1.0f)).xyz;

	float3 shadowPosDX = ddx_fine(shadowPosition);
    float3 shadowPosDY = ddy_fine(shadowPosition);

	return SampleShadowCascade(shadowPosition, shadowPosDX, shadowPosDY, cascadeIdx, screenPos);
}