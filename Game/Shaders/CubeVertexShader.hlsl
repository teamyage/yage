#include "VertexShader.hlsli"

VSOutputPoNoDTx VShader(VSInputNmTx input)
{
	VSOutputPoNoDTx output;

	//float4x4 sr = mul(Renderable_Rotation, Renderable_Scale);
	float4x4 srt = mul(Renderable_Translation, Renderable_Rotation);
	float4x4 vp = mul(Camera_Projection, Camera_View);
	output.Position = mul(srt, input.Position);
	output.PositionPS = mul(vp, output.Position);
	output.Depth    = output.PositionPS.w;
	output.Normal = mul(Renderable_Rotation, float4(input.Normal,0)).xyz;
    output.TexCoord = input.TexCoord;

	return output;
}
