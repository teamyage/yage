#include "VertexShader.hlsli"

VSOutputPoNoDTx VShader(VSInputNm input)
{
	VSOutputPoNoDTx output;
	
	float4x4 vp = mul(Camera_Projection, Camera_View);

	output.Position = mul(Renderable_Translation, input.Position);
	output.PositionPS = mul(vp, output.Position);
	output.Depth    = output.PositionPS.w;
	output.Normal = input.Normal;
	output.TexCoord = float2(0,0);

	return output;
}
