cbuffer ShadowsConstantBuffer : register(b4)
{
	float3		CameraPosWS			: packoffset(c0);
	float4x4	ShadowMatrix		: packoffset(c1);
	float4		CascadeSplits		: packoffset(c5);
	float4		CascadeOffsets[4]	: packoffset(c6);
	float4		CascadeScales[4]	: packoffset(c10);
};