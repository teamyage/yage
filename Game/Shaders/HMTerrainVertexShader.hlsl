#include "VertexShader.hlsli"

struct HullInputType
{
    float4 position : POSITION;
};

HullInputType VShader(VSInputTx input)
{
	HullInputType output;
	
	output.position = input.Position;

	return output;
}
