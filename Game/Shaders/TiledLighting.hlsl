#include "Lights.hlsli"
#include "PerFrame.hlsli"
#include "Lighting.hlsli"
#include "RenderSettings.hlsli"

float LinearDepth(in float zw)
{
    return Camera_Projection._34 / (zw - Camera_Projection._33);
}

//Input
Texture2DMS<float4> DepthTexture : register(t0);
StructuredBuffer<PointLight> PointLights: register(t1);

//Output
RWByteAddressBuffer HeadBuffer : register(u0);
RWByteAddressBuffer LightIndexBuffer : register(u1);

// Shared memory
groupshared uint TileMinZ;
groupshared uint TileMaxZ;

groupshared uint ldsPointIdxCounter;
groupshared uint ldsPointIdx[64];

[numthreads(tileSize, tileSize, 1)]
void CShader(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex )
{
	const uint groupThreadId = (Gid.y * numtiles.x) + Gid.x;
	const uint numThreads = tileSize*tileSize;
		
	// Work out Z bounds for our samples
	float linearZ = LinearDepth(DepthTexture.Load((int2)DTid.xy,0).r);	

	float minZSample = min(0x7F7FFFFF, linearZ);
    float maxZSample = max(0, linearZ);

	if(GI == 0)
	{
		ldsPointIdxCounter = 0;
		TileMinZ = 0x7F7FFFFF;      // Max float
		TileMaxZ = 0;
	}

	GroupMemoryBarrierWithGroupSync();

	if(maxZSample >= minZSample)
	{
		InterlockedMin(TileMinZ, asuint(minZSample));
		InterlockedMax(TileMaxZ, asuint(maxZSample));
	}

	GroupMemoryBarrierWithGroupSync();

	float minTileZ = asfloat(TileMinZ);
    float maxTileZ = asfloat(TileMaxZ);

	// Derive frustum planes
	float4 frustumPlanes[6];
	float2 tileScale = screensize * rcp(2 * tileSize);
	float2 tileBias = tileScale - float2(Gid.xy);

	// Create composite projection matrix.
	float4 c1 = float4(Camera_Projection._11 * tileScale.x, 0.0f, tileBias.x, 0.0f);
	float4 c2 = float4(0.0f, -Camera_Projection._22 * tileScale.y, tileBias.y, 0.0f);
	float4 c4 = float4(0.0f, 0.0f, 1.0f, 0.0f);
		
	// Sides
	frustumPlanes[0] = c4 - c1;
	frustumPlanes[1] = c4 + c1;
	frustumPlanes[2] = c4 - c2;
	frustumPlanes[3] = c4 + c2;

	// Near/far
	frustumPlanes[4] = float4(0.0f, 0.0f, 1.0f, -minTileZ);
	frustumPlanes[5] = float4(0.0f, 0.0f, -1.0f, maxTileZ);

	// Normalize frustum planes. Near and far are already normalized.
	[unroll]
	for (uint f = 0; f < 4; ++f) 
		frustumPlanes[f] *= rcp(length(frustumPlanes[f].xyz));

	uint lightIndex = 0;
	for(lightIndex = GI; lightIndex < numPointLights; lightIndex+=numThreads)
	{
		bool inFrustum = true;			
		float r = -PointLights[lightIndex].m_radius;
		float4 viewTransformedPosition = float4(mul(Camera_View, float4(PointLights[lightIndex].m_position,1)).xyz, 1.0f);
			
		float d[6];

		[unroll]
		for(unsigned int i=0;i<6;i++)
			d[i] = dot(frustumPlanes[i], viewTransformedPosition);
				
		//Satisfy all to be inFrustum
		inFrustum = (d[0] >= r) && (d[1] >= r) && (d[2] >= r) && 
					(d[3] >= r) && (d[4] >= r) && (d[5] >= r);		

		[branch]
		if(inFrustum)
		{
			uint linkId = 0;
			InterlockedAdd( ldsPointIdxCounter, 1, linkId );
			ldsPointIdx[linkId] = lightIndex;
		}		
	}

	GroupMemoryBarrierWithGroupSync();

	if(GI == 0)
	{
		//write count of lights for this tile into the head buffer
		HeadBuffer.Store(groupThreadId*4, ldsPointIdxCounter);
	}

	//store array in tile compartment of lights
	for(lightIndex = GI; lightIndex < ldsPointIdxCounter; lightIndex+=numThreads)
	{
		LightIndexBuffer.Store((64*groupThreadId+lightIndex)*4, ldsPointIdx[lightIndex]);
	}
}