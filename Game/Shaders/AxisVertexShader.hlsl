#include "VertexShader.hlsli"

VSOutputNoFog VShader(VSInputVc input)
{
	VSOutputNoFog output;

	float4x4 sr = mul(Renderable_Rotation, Renderable_Scale);
	float4x4 srt = mul(Renderable_Translation, sr);
	float4x4 srtv = mul(Camera_View, srt);
	float4x4 srtvp = mul(Camera_Projection, srtv);

	output.PositionPS = mul(srtvp, input.Position);
	output.Diffuse = input.Color;

	return output;
}
