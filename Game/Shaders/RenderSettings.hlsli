cbuffer RenderSettingsConstantBuffer : register(b2)
{
	int2 screensize;
	int2 numtiles;
	uint outputType;
};