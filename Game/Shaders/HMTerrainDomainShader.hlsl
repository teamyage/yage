#include "DomainShader.hlsli"

struct PixelInputType
{
    float4 position : SV_POSITION;
};
struct ConstantOutputType
{
    float edges[4] : SV_TessFactor;
    float inside[2] : SV_InsideTessFactor;
};
struct HullOutputType
{
    float4 position : POSITION;
};

Texture2D Texture : register(t5);

[domain("quad")]
PixelInputType DShader(ConstantOutputType input, float2 UV : SV_DomainLocation, const OutputPatch<HullOutputType, 4> patch)
{
    PixelInputType output;
 
	float3 topMidpoint = lerp(patch[0].position.xyz, patch[1].position.xyz, UV.x);
	float3 bottomMidpoint = lerp(patch[3].position.xyz, patch[2].position.xyz, UV.x);                               
	float3 position = lerp(topMidpoint, bottomMidpoint, UV.y);

	float4 textureColor = float4(Texture.SampleLevel(ss, position.xz/128.0f, 1));

	position.y = textureColor.r*32;

	float4x4 vp = mul(Camera_Projection, Camera_View);

	output.position = mul(vp, float4(position, 1.0f));

    return output;
}