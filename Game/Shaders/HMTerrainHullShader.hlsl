#include "HullShader.hlsli"

struct HullOutputType
{
    float4 position : POSITION;
};
struct ConstantOutputType
{
    float edges[4] : SV_TessFactor;
    float inside[2] : SV_InsideTessFactor;
};
struct HullInputType
{
    float4 position : POSITION;
};

ConstantOutputType ColorPatchConstantFunction(InputPatch<HullInputType, 4> inputPatch, uint patchId : SV_PrimitiveID)
{    
    ConstantOutputType output;
	
	float vertex0 = lerp(128, 16, (saturate((distance(Camera_Position, (float3)inputPatch[0].position)) / 700.0f)));
	float vertex1 = lerp(128, 16, (saturate((distance(Camera_Position, (float3)inputPatch[1].position)) / 700.0f)));
	float vertex2 = lerp(128, 16, (saturate((distance(Camera_Position, (float3)inputPatch[2].position)) / 700.0f)));
	float vertex3 = lerp(128, 16, (saturate((distance(Camera_Position, (float3)inputPatch[3].position)) / 700.0f)));

	output.edges[0] = min(vertex0, vertex3);
	output.edges[1] = min(vertex0, vertex1);
	output.edges[2] = min(vertex1, vertex2);
	output.edges[3] = min(vertex2, vertex3);
	
	float minTess = min(output.edges[1], output.edges[3]);

	output.inside[0] = minTess;
	output.inside[1] = minTess;

	return output;
}

[domain("quad")]
[partitioning("fractional_odd")]
[outputtopology("triangle_cw")]
[outputcontrolpoints(4)]
[patchconstantfunc("ColorPatchConstantFunction")]

HullOutputType HShader(InputPatch<HullInputType, 4> patch, uint pointId : SV_OutputControlPointID, uint patchId : SV_PrimitiveID)
{
    HullOutputType output;
	
    // Set the position for this control point as the output position.
    output.position = patch[pointId].position;

    return output;
}