#include "PixelShader.hlsli"

Texture2D<float4> Texture : register(t5);

float4 PShader(VSOutputPoNoDTx input, float4 pixelsscoord : SV_POSITION ) : SV_TARGET
{
	uint2 pixelLocation = uint2(pixelsscoord.xy-float2(.5,.5));
	uint2 tileLocation2D = uint2(pixelLocation.x/tileSize, pixelLocation.y/tileSize);
	uint tileLocation = tileLocation2D.y*numtiles.x+tileLocation2D.x;

	float3 Xvec = float3(1,0,0);
	float3 Yvec = float3(0,1,0);
	float3 Zvec = float3(0,0,1);
	float dotwithX = dot(Xvec, normalize(input.Normal));
	float dotwithY = dot(Yvec, normalize(input.Normal));
	float dotwithZ = dot(Zvec, normalize(input.Normal));
	float absdotwithX = abs(dotwithX);
	float absdotwithY = abs(dotwithY);
	float absdotwithZ = abs(dotwithZ);	
	float2 TexCoord = absdotwithY*input.Position.xz + absdotwithX*input.Position.yz + absdotwithZ*input.Position.xy;	
	float4 textureColor = float4(Texture.Sample(ss, TexCoord));

	float3 pointLightColor = float3(0,0,0);	
	float3 directionalLightColor = float3(0,0,0);
	uint numPointLights = HeadBuffer.Load(tileLocation*4);

	for(uint plid=0;plid<numPointLights;plid++)
	{
		PointLight p = PointLights[LightIndexBuffer.Load((64*tileLocation+plid)*4)];

		float3 lightVector = p.m_position - input.Position.xyz;
		float dist = length(lightVector);
		float NdL = max(0.0f, dot(input.Normal, lightVector*rcp(dist)));
		float attenuation = 1.0f - saturate(dist * rcp(p.m_radius + 0.1f));
		float3 diffuse = NdL * p.m_color * p.m_intensity;
		pointLightColor += attenuation * diffuse;
	}

	for(uint dlid=0;dlid<numDirectionalLights;dlid++)
	{
		DirectionalLight d = DirectionalLights[dlid];
		float NdL = max(0.0f, dot(input.Normal, d.m_rayDirection));
		directionalLightColor += NdL * d.m_color * d.m_intensity;
	}

	float shadowVisibility = ShadowVisibility(	input.Position.xyz,
									input.Depth, 
									max(0.0f, dot(input.Normal, DirectionalLights[0].m_rayDirection)),
									input.Normal, 
									pixelLocation);

	//normal color output
	if(outputType==0)
		return textureColor * 
			(
				float4(pointLightColor, 1) + 
				float4(0.20f, 0.20f, 0.20f, 1.0f) + 
				float4(shadowVisibility*directionalLightColor, 1)
			);
	else if(outputType==1)
		return float4(numPointLights/64.0f, numPointLights/64.0f, numPointLights/64.0f, 1)+float4(.04,.04,.04,1);
	else if(outputType==2)
		return input.Depth/1000.0f;
	else if(outputType==3)
	{
		uint cascadeIdx = 0;

		// Figure out which cascade to sample from
		[unroll]
		for(uint i = 0; i < 3; ++i)
		{
			[flatten]
			if(input.Depth > CascadeSplits[i])
				cascadeIdx = i + 1;
		}	
		
		const float3 CascadeColors[4] =
        {
            float3(1.0f, 0.0, 0.0f),
            float3(0.0f, 1.0f, 0.0f),
            float3(0.0f, 0.0f, 1.0f),
            float3(1.0f, 1.0f, 0.0f)
        };

        return float4(shadowVisibility*CascadeColors[cascadeIdx],1);

	}
	return float4(1,1,0,1);
}