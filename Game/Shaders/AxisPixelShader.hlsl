#include "PixelShader.hlsli"

float4 PShader(PSInputNoFog input) : SV_TARGET
{
    return input.Diffuse;
}