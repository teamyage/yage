cbuffer PerFrameConstantBuffer : register(b0)
{
	float4x4 Camera_View			: packoffset(c0);
	float4x4 Camera_Projection		: packoffset(c4);
	float3	 Camera_Position		: packoffset(c8);
};
