struct PointLight
{
	float3 m_position;
	float m_radius;
	float3 m_color;
	float m_intensity;
};

struct DirectionalLight
{
	float3 m_rayDirection;
	float m_intensity;
	float3 m_color;
	float padding;
};

#define tileSize 16