#include "stdafx.h"
#include "XMLLoader.h"

XMLLoader::XMLLoader(std::string filename)
{
	std::ifstream ifs(filename);
	boost::property_tree::read_xml(ifs, m_propertyTree);
	ifs.close();
}

XMLLoader::~XMLLoader()
{
}