#pragma once

enum BlendStates
{
	BSOpaque=0,
	BSAlphaBlend=1,
	BSAdditive,
	BSLAST
};

enum RasterizerStates
{
	RSCullNone=0,
	RSCullCW=1,
	RSCullCCW=2,
	RSLAST=3
};

enum SamplerStates
{
	SSAnistropicClamp=0,
	SSAnistropicWrap=1,
	SSLinearClamp,
	SSLinearWrap,
	SSLAST
};

enum DepthStensilStates
{
	DSDefault=0,
	DSEqual=1,
	DSLAST
};