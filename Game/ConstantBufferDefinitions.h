#pragma once

#include "DirectXMath.h"
#include "Float3.h"
#include "Float4.h"
#include "values.h"

using namespace DirectX;

// For aligning to float4 boundaries
#define Float4Align __declspec(align(16))

struct PerFrameConstantBuffer
{
	XMMATRIX Camera_View;			
	XMMATRIX Camera_Projection;	
	XMVECTOR Camera_Position;
};

struct PerRenderableConstantBuffer
{
	XMMATRIX Renderable_Rotation;	
	XMMATRIX Renderable_Translation;
	XMMATRIX Renderable_Scale;
};

struct RenderSettingsConstantBuffer
{
	XMINT2 screensize;
	XMINT2 numtiles;
	Float4Align UINT outputType;
};

struct LightingConstantBuffer
{
	UINT numPointLights;
	UINT numDirectionalLights;
	int padding2;
	int padding3;
};

struct ShadowsConstantBuffer
{
	Float4Align Float3 CameraPosWS;
	Float4Align Float4x4 ShadowMatrix;
	Float4Align float CascadeSplits[4];
	Float4Align Float4 CascadeOffsets[4];
	Float4Align Float4 CascadeScales[4];
};
