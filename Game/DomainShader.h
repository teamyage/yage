#pragma once
#include "BinaryLoader.h"

class DomainShader : public BinaryLoader, public D3DSystem
{
public:
	DomainShader(std::string filename);
	~DomainShader();

	void Bind();
	void Unbind();
	void Release();

private:
	ID3D11DomainShader *m_domainShader;
};

