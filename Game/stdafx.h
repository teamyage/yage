//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
//                                            MICROSOFT                                             //
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
//WINDOWS
#include "windows.h"
#include <windowsx.h>

//DIRECTX 11
#include <d3d11.h>
#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include "DirectXColors.h"
#include "DirectXCollision.h"

//DIRECTXTK
#include "DDSTextureLoader.h"
#include "CommonStates.h"
#include "VertexTypes.h"

//ASSERTION
#include "Assert.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
//                                               STD                                                //
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
//STRING
#include "String.h"

//FILE STREAMS
#include <fstream>

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
//                                              BOOST                                               //
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
//BOOST THREAD
#include <boost/thread.hpp>

//BOOST LOGGER
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/debug_output_backend.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

//BOOST_FOREACH
#include <boost/foreach.hpp>

//PROPERTY LOADER
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>

//IS_BASE_OF
#include <boost/type_traits/is_base_of.hpp>
#include <boost/static_assert.hpp>

//BOOST DATASTRUCTURES
#include <boost/unordered_map.hpp>

//ALGORITHMS
#include <boost/algorithm/clamp.hpp>

//RANDOM
#include <boost/random.hpp>

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
//                                             INTERNAL                                             //
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
#include "Macros.h"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
//                                               GLM                                                //
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
#include "glm.hpp"
#include "gtc/noise.hpp"

//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
//                                              BULLET                                              //
//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"