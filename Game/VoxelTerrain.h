#pragma once
#include "renderable.h"
#include "CellId.h"
#include "Cell.h"
#include <boost/foreach.hpp>
#include "ConstantBuffer.h"
#include "ConstantBufferDefinitions.h"
#include "System.h"
#include "PixelShader.h"
#include "VertexShader.h"
#include "ContentManager.h"
#include "D3DSystem.h"
#include "CellFactory.h"
#include "LightingSystem.h"

using namespace DirectX;

typedef boost::unordered_map<CellId, Cell*,CellIdHasher> Scene;

class VoxelTerrain : public IRenderableSystem, public System
{
public:
	VoxelTerrain();
	~VoxelTerrain();
	int Render();
	void RenderZ();

private:
	inline void AddCellToScene(Cell* c);

	Scene m_scene;	
	CellFactory m_cellFactory;
	ConstantBuffer<PerRenderableConstantBuffer> *m_perRenderableConstantBuffer;
	PixelShader *m_pixelShader;
	VertexShader<VertexPositionNormal> *m_vertexShader;	
	Texture *m_diffuse;
};

