#pragma once
#include "renderable.h"
#include "VertexTypes.h"
#include "ContentManager.h"
#include "PixelShader.h"
#include "VertexShader.h"
#include "Texture.h"
#include "PhysicsSystem.h"
#include "Utils.h"
#include "Material.h"
#include "Camera.h"
#include "SRV.h"

using namespace DirectX;

#define VERTEXTYPE_CUBE VertexPositionNormalTexture
#define INDEXTYPE_CUBE USHORT
#define INDEXFORMAT_CUBE DXGI_FORMAT_R16_UINT

class Cube : public Renderable<VERTEXTYPE_CUBE, INDEXTYPE_CUBE>
{
public:
	Cube();
	~Cube();

	void UpdateConstantBuffer();

	int Render();
	void RenderZ();

private:
	btRigidBody *m_rb;
	Material *m_material;
};

