#include "stdafx.h"
#include "UAV.h"

UAV::UAV() : m_UAV(NULL) {}

void UAV::BindNULLUAV(UINT NumViews)
{
	ID3D11UnorderedAccessView** nulluavs = new ID3D11UnorderedAccessView*[NumViews];

	for(UINT i=0;i<NumViews;i++)
		nulluavs[i] = NULL;

	D3D->devcon->CSSetUnorderedAccessViews(0, NumViews, nulluavs, NULL);
}

void UAV::BindUAV(UINT slot, UINT initialCounterValue/* =NULL */)
{
	D3D->devcon->CSSetUnorderedAccessViews(slot, 1, &m_UAV, &initialCounterValue);
}

void UAV::CreateUAV(ID3D11Resource *resource, const D3D11_UNORDERED_ACCESS_VIEW_DESC *pDesc)
{
	if(FAILED(D3D->dev->CreateUnorderedAccessView(resource, pDesc, &m_UAV)))
		BOOST_LOG_TRIVIAL(error) << "Error while creating UAV";
}