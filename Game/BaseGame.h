#pragma once

#include "System.h"
#include "D3DSystem.h"
#include "Logger.h"
#include "BaseGameState.h"
#include "TiledForwardRenderer.h"
#include "Timer.h"
#include "InputSystem.h"
#include "WindowManager.h"
#include "ContentManager.h"
#include "Camera.h"
#include "PhysicsSystem.h"

class BaseGame : public System, public D3DSystem
{
public:
	BaseGame();
	~BaseGame();

	void setCurrentGameState(GameState state);
	void Run();

	virtual void Initialize()	= 0;
	virtual void Loop()			= 0;
	virtual void Release()		= 0;

	virtual void InitializeEngine();
	virtual void LoopEngine();
	virtual void ReleaseEngine();

protected: 	
	TiledForwardRenderer *m_renderer;
	BaseGameState *m_gameStates[2];
	GameState m_currentGameState;
};

