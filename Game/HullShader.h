#pragma once
#include "BinaryLoader.h"

class HullShader : public BinaryLoader, public D3DSystem
{
public:
	HullShader(std::string filename);
	~HullShader();

	void Bind();
	void Unbind();
	void Release();

private:
	ID3D11HullShader *m_hullShader;
};

