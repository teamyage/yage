#include "stdafx.h"
#include "Float3.h"
#include "Float4x4.h"
#include "Float3x3.h"

Float3::Float3()
{
	x = y = z = 0.0f;
}

Float3::Float3(float x_)
{
	x = y = z = x_;
}

Float3::Float3(float x_, float y_, float z_)
{
	x = x_;
	y = y_;
	z = z_;
}

Float3::Float3(const XMFLOAT3& xyz)
{
	x = xyz.x;
	y = xyz.y;
	z = xyz.z;
}

Float3::Float3(FXMVECTOR xyz)
{
	XMStoreFloat3(this, xyz);
}

Float3& Float3::operator+=(const Float3& other)
{
	x += other.x;
	y += other.y;
	z += other.z;
	return *this;
}

Float3 Float3::operator+(const Float3& other) const
{
	Float3 result;
	result.x = x + other.x;
	result.y = y + other.y;
	result.z = z + other.z;
	return result;
}

Float3& Float3::operator+=(float s)
{
	x += s;
	y += s;
	z += s;
	return *this;
}

Float3 Float3::operator+(float s) const
{
	Float3 result;
	result.x = x + s;
	result.y = y + s;
	result.z = z + s;
	return result;
}

Float3& Float3::operator-=(const Float3& other)
{
	x -= other.x;
	y -= other.y;
	z -= other.z;
	return *this;
}

Float3 Float3::operator-(const Float3& other) const
{
	Float3 result;
	result.x = x - other.x;
	result.y = y - other.y;
	result.z = z - other.z;
	return result;
}

Float3& Float3::operator-=(float s)
{
	x -= s;
	y -= s;
	z -= s;
	return *this;
}

Float3 Float3::operator-(float s) const
{
	Float3 result;
	result.x = x - s;
	result.y = y - s;
	result.z = z - s;
	return result;
}


Float3& Float3::operator*=(const Float3& other)
{
	x *= other.x;
	y *= other.y;
	z *= other.z;
	return *this;
}

Float3 Float3::operator*(const Float3& other) const
{
	Float3 result;
	result.x = x * other.x;
	result.y = y * other.y;
	result.z = z * other.z;
	return result;
}

Float3& Float3::operator*=(float s)
{
	x *= s;
	y *= s;
	z *= s;
	return *this;
}

Float3 Float3::operator*(float s) const
{
	Float3 result;
	result.x = x * s;
	result.y = y * s;
	result.z = z * s;
	return result;
}

Float3& Float3::operator/=(const Float3& other)
{
	x /= other.x;
	y /= other.y;
	z /= other.z;
	return *this;
}

Float3 Float3::operator/(const Float3& other) const
{
	Float3 result;
	result.x = x / other.x;
	result.y = y / other.y;
	result.z = z / other.z;
	return result;
}

Float3& Float3::operator/=(float s)
{
	x /= s;
	y /= s;
	z /= s;
	return *this;
}

Float3 Float3::operator/(float s) const
{
	Float3 result;
	result.x = x / s;
	result.y = y / s;
	result.z = z / s;
	return result;
}

bool Float3::operator==(const Float3& other) const
{
	return x == other.x && y == other.y && z == other.z;
}

bool Float3::operator!=(const Float3& other) const
{
	return x != other.x || y != other.y || z != other.z;
}

Float3 Float3::operator-() const
{
	Float3 result;
	result.x = -x;
	result.y = -y;
	result.z = -z;

	return result;
}

Float3 operator*(float a, const Float3& b)
{
	return Float3(a * b.x, a * b.y, a * b.z);
}

XMVECTOR Float3::ToSIMD() const
{
	return XMLoadFloat3(this);
}

float Float3::Length() const
{
	return Float3::Length(*this);
}

float Float3::Dot(const Float3& a, const Float3& b)
{
	return XMVectorGetX(XMVector3Dot(a.ToSIMD(), b.ToSIMD()));
}

Float3 Float3::Cross(const Float3& a, const Float3& b)
{
	Float3 result;
	XMStoreFloat3(&result, XMVector3Cross(a.ToSIMD(), b.ToSIMD()));
	return result;
}

Float3 Float3::Normalize(const Float3& a)
{
	Float3 result;
	XMStoreFloat3(&result, XMVector3Normalize(a.ToSIMD()));
	return result;
}

Float3 Float3::Transform(const Float3& v, const Float3x3& m)
{
	XMVECTOR vec = v.ToSIMD();
	vec = XMVector3TransformCoord(vec, m.ToSIMD());
	return Float3(vec);
}

Float3 Float3::Transform(const Float3& v, const Float4x4& m)
{
	XMVECTOR vec = v.ToSIMD();
	vec = XMVector3TransformCoord(vec, m.ToSIMD());
	return Float3(vec);
}

Float3 Float3::TransformDirection(const Float3&v, const Float4x4& m)
{
	XMVECTOR vec = v.ToSIMD();
	vec = XMVector3TransformNormal(vec, m.ToSIMD());
	return Float3(vec);
}

Float3 Float3::Transform(const Float3& v, const Quaternion& q)
{
	return Float3::Transform(v, q.ToFloat3x3());
}

Float3 Float3::Clamp(const Float3& val, const Float3& min, const Float3& max)
{
	Float3 retVal;
	retVal.x = glm::clamp(val.x, min.x, max.x);
	retVal.y = glm::clamp(val.y, min.y, max.y);
	retVal.z = glm::clamp(val.z, min.z, max.z);
	return retVal;
}

Float3 Float3::Perpendicular(const Float3& vec)
{
	Float3 perp;

	float x = std::abs(vec.x);
	float y = std::abs(vec.y);
	float z = std::abs(vec.z);
	float minVal = std::min(x, y);
	minVal = std::min(minVal, z);

	if(minVal == x)
		perp = Float3::Cross(vec, Float3(1.0f, 0.0f, 0.0f));
	else if(minVal == y)
		perp = Float3::Cross(vec, Float3(0.0f, 1.0f, 0.0f));
	else
		perp = Float3::Cross(vec, Float3(0.0f, 0.0f, 1.0f));

	return Float3::Normalize(perp);
}

float Float3::Distance(const Float3& a, const Float3& b)
{
	XMVECTOR x = XMLoadFloat3(&a);
	XMVECTOR y = XMLoadFloat3(&b);
	XMVECTOR length = XMVector3Length(XMVectorSubtract(x, y));
	return XMVectorGetX(length);
}

float Float3::Length(const Float3& v)
{
	XMVECTOR x =  XMLoadFloat3(&v);
	XMVECTOR length = XMVector3Length(x);
	return XMVectorGetX(length);
}