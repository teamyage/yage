#pragma once

#include "stdafx.h"

#include "SRV.h"
#include "UAV.h"

template <class T>
class StructuredBuffer : public SRV, public UAV
{
public:
	StructuredBuffer(T* elements, UINT numElements, BOOL useAsUAV=false, BOOL appendConsume=false, BOOL useAsDrawIndirect=false, D3D11_CPU_ACCESS_FLAG cpuFlags=D3D11_CPU_ACCESS_WRITE, D3D11_USAGE usageFlags=D3D11_USAGE_DEFAULT)
	{
		m_buffer = NULL;
		m_useAsUAV = useAsUAV;
		m_appendConsume = appendConsume;
		m_useAsDrawIndirect = useAsDrawIndirect;
		m_cpuFlags = cpuFlags;
		m_usage = usageFlags;
		m_numElements = numElements;

		if(numElements!=0)
			Initialize(elements, numElements);
	}

	StructuredBuffer(std::vector<T> &elements, BOOL useAsUAV=false, BOOL appendConsume=false, BOOL useAsDrawIndirect=false, D3D11_CPU_ACCESS_FLAG cpuFlags=D3D11_CPU_ACCESS_WRITE, D3D11_USAGE usageFlags=D3D11_USAGE_DEFAULT)
	{		
		m_buffer = NULL;
		m_useAsUAV = useAsUAV;
		m_appendConsume = appendConsume;
		m_useAsDrawIndirect = useAsDrawIndirect;
		m_cpuFlags = cpuFlags;
		m_usage = usageFlags;
		m_numElements = (UINT)elements.size();

		if(!elements.empty())
			Initialize(&elements[0],(UINT)elements.size());
	}

	~StructuredBuffer()
	{
		SAFE_RELEASE_DELETE(m_buffer);
	}

	void Update(std::vector<T> &elements)
	{
		Update(&elements[0], (UINT)elements.size());
	}

	void Update(T* elements, UINT numElements)
	{
		if(m_numElements == numElements)
		{
			D3D11_MAPPED_SUBRESOURCE ms;
			ZeroMemory(&ms, sizeof(D3D11_MAPPED_SUBRESOURCE));
			D3D->devcon->Map(m_buffer, 0, D3D11_MAP_WRITE_DISCARD, NULL, &ms);
			memcpy(ms.pData, &elements[0], numElements*sizeof(T));
			D3D->devcon->Unmap(m_buffer, 0);
		}
		else
		{
			SAFE_RELEASE(m_buffer);
			m_buffer = NULL;
			Initialize(elements, numElements);
		}
	}

	void Release()
	{
		SAFE_RELEASE(m_buffer);
	}	

private:
	void Initialize(T* elements, UINT numElements)
	{		
		m_stride = sizeof(T);
		m_numElements = numElements;
		m_size = m_stride * m_numElements;		

		if(m_appendConsume || m_useAsDrawIndirect)
			m_useAsUAV = true;

		D3D11_BUFFER_DESC bufferDesc;
		ZeroMemory(&bufferDesc,sizeof(D3D11_BUFFER_DESC));
		bufferDesc.ByteWidth = m_stride * m_numElements;
		bufferDesc.Usage = m_usage;
		bufferDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		bufferDesc.BindFlags |= m_useAsUAV ? D3D11_BIND_UNORDERED_ACCESS : 0;
		bufferDesc.CPUAccessFlags = m_cpuFlags;
		bufferDesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
		bufferDesc.MiscFlags |= m_useAsDrawIndirect ? D3D11_RESOURCE_MISC_DRAWINDIRECT_ARGS : 0;
		bufferDesc.StructureByteStride = m_stride;

		D3D11_SUBRESOURCE_DATA subresourceData;
		subresourceData.pSysMem = elements;
		subresourceData.SysMemPitch = 0;
		subresourceData.SysMemSlicePitch = 0;

		HRESULT response = D3D->dev->CreateBuffer(&bufferDesc, &subresourceData, &m_buffer);
		
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		srvDesc.Format = DXGI_FORMAT_UNKNOWN;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
		srvDesc.Buffer.ElementOffset = 0;
		srvDesc.Buffer.ElementWidth = numElements;
		CreateSRV(m_buffer, &srvDesc);

		if(m_useAsUAV)
		{
			D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
			uavDesc.Format = DXGI_FORMAT_UNKNOWN;
			uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
			uavDesc.Buffer.FirstElement = 0;
			uavDesc.Buffer.Flags = D3D11_BUFFER_UAV_FLAG_COUNTER;// || m_appendConsume ? D3D11_BUFFER_UAV_FLAG_APPEND : 0;
			uavDesc.Buffer.NumElements = numElements;
			CreateUAV(m_buffer, &uavDesc);
		}
	}
	BOOL m_useAsUAV;
	BOOL m_appendConsume;
	BOOL m_useAsDrawIndirect;
	UINT m_stride;
	UINT m_size;
	UINT m_numElements;
	D3D11_CPU_ACCESS_FLAG m_cpuFlags;
	D3D11_USAGE m_usage;
	ID3D11Buffer *m_buffer;
	StructuredBuffer(StructuredBuffer const&);
	StructuredBuffer& operator= (StructuredBuffer const&);
};
