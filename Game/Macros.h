#pragma once

#define BT_NO_SIMD_OPERATOR_OVERLOADS

#define SAFE_DELETE(x) if(x) { delete x; x = NULL; }
#define SAFE_ARRAY_DELETE(x) if (x) { delete [] x; x = NULL; }
#define SAFE_RELEASE(x) if(x) { x->Release(); }
#define SAFE_RELEASE_DELETE(x) if(x) { x->Release(); delete x; x = NULL; }