#include "stdafx.h"
#include "DepthStencilBuffer.h"

DepthStencilBuffer::DepthStencilBuffer() :  Width(0),
	Height(0),
	MultiSamples(0),
	MSQuality(0),
	Format(DXGI_FORMAT_UNKNOWN),
	ArraySize(1),
	texture(NULL)
{}

void DepthStencilBuffer::ClearAllSlices(UINT ClearFlags, FLOAT Depth, UINT8 Stencil)
{
	for(UINT i=0;i<ArraySlices.size();i++)
		D3D->devcon->ClearDepthStencilView(ArraySlices[i], ClearFlags, Depth, Stencil);
}

void DepthStencilBuffer::Initialize(UINT width,
									UINT height,
									DXGI_FORMAT format,
									bool useAsShaderResource,
									UINT multiSamples,
									UINT msQuality,
									UINT arraySize)
{
	UINT bindFlags = D3D11_BIND_DEPTH_STENCIL;
	if (useAsShaderResource)
		bindFlags |= D3D11_BIND_SHADER_RESOURCE;

	DXGI_FORMAT dsTexFormat;
	if (!useAsShaderResource)
		dsTexFormat = format;
	else if (format == DXGI_FORMAT_D16_UNORM)
		dsTexFormat = DXGI_FORMAT_R16_TYPELESS;
	else if(format == DXGI_FORMAT_D24_UNORM_S8_UINT)
		dsTexFormat = DXGI_FORMAT_R24G8_TYPELESS;
	else
		dsTexFormat = DXGI_FORMAT_R32_TYPELESS;

	D3D11_TEXTURE2D_DESC desc;
	desc.Width = width;
	desc.Height = height;
	desc.ArraySize = arraySize;
	desc.BindFlags = bindFlags;
	desc.CPUAccessFlags = 0;
	desc.Format = dsTexFormat;
	desc.MipLevels = 1;
	desc.MiscFlags = 0;
	desc.SampleDesc.Count = multiSamples;
	desc.SampleDesc.Quality = msQuality;
	desc.Usage = D3D11_USAGE_DEFAULT;
	D3D->dev->CreateTexture2D(&desc, NULL, &texture);

	ArraySlices.clear();
	for (UINT i = 0; i < arraySize; ++i)
	{
		D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
		dsvDesc.Format = format;

		if (arraySize == 1)
		{
			dsvDesc.ViewDimension = multiSamples > 1 ? D3D11_DSV_DIMENSION_TEXTURE2DMS : D3D11_DSV_DIMENSION_TEXTURE2D;
			dsvDesc.Texture2D.MipSlice = 0;        
		}
		else
		{
			dsvDesc.ViewDimension = multiSamples > 1 ? D3D11_DSV_DIMENSION_TEXTURE2DMSARRAY : D3D11_DSV_DIMENSION_TEXTURE2DARRAY;
			dsvDesc.Texture2DArray.ArraySize = 1;
			dsvDesc.Texture2DArray.FirstArraySlice = i;
			dsvDesc.Texture2DArray.MipSlice = 0;
		}

		dsvDesc.Flags = 0;
		CreateDSV(texture, &dsvDesc);
		ArraySlices.push_back(m_DSV);

		//if (i == 0)
		//{
		//    // Also create a read-only DSV
		//    dsvDesc.Flags = D3D11_DSV_READ_ONLY_DEPTH;
		//    if (format == DXGI_FORMAT_D24_UNORM_S8_UINT || format == DXGI_FORMAT_D32_FLOAT_S8X24_UINT)
		//        dsvDesc.Flags |= D3D11_DSV_READ_ONLY_STENCIL;
		//    D3D->dev->CreateDepthStencilView(texture, &dsvDesc, &ReadOnlyDSView);
		//    dsvDesc.Flags = 0;
		//}
	}

	m_DSV = ArraySlices[0];    

	if (useAsShaderResource)
	{
		DXGI_FORMAT dsSRVFormat;
		if (format == DXGI_FORMAT_D16_UNORM)
			dsSRVFormat = DXGI_FORMAT_R16_UNORM;
		else if(format == DXGI_FORMAT_D24_UNORM_S8_UINT)
			dsSRVFormat = DXGI_FORMAT_R24_UNORM_X8_TYPELESS ;
		else
			dsSRVFormat = DXGI_FORMAT_R32_FLOAT;

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		srvDesc.Format = dsSRVFormat;

		if (arraySize == 1)
		{
			srvDesc.ViewDimension = multiSamples > 1 ? D3D11_SRV_DIMENSION_TEXTURE2DMS : D3D11_SRV_DIMENSION_TEXTURE2D;
			srvDesc.Texture2D.MipLevels = 1;
			srvDesc.Texture2D.MostDetailedMip = 0;
		}
		else
		{
			srvDesc.ViewDimension = multiSamples > 1 ? D3D11_SRV_DIMENSION_TEXTURE2DMSARRAY : D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
			srvDesc.Texture2DArray.ArraySize = arraySize;            
			srvDesc.Texture2DArray.FirstArraySlice = 0;
			srvDesc.Texture2DArray.MipLevels = 1;
			srvDesc.Texture2DArray.MostDetailedMip = 0;
		}

		CreateSRV(texture, &srvDesc);
	}

	Width = width;
	Height = height;
	MultiSamples = multiSamples;
	Format = format;
	ArraySize = arraySize;
}

void DepthStencilBuffer::Release()
{
	//for(ID3D11RenderTargetView* slice : ArraySlices)
	//	SAFE_RELEASE(slice);

	//SAFE_RELEASE(texture);
	//SAFE_RELEASE(RTView);
	//SAFE_RELEASE(SRView);
	//SAFE_RELEASE(UAView);

	//ArraySlices.clear();
}

void DepthStencilBuffer::BindAsOnlyRenderTarget(UINT slice)
{
	ID3D11RenderTargetView* nullRenderTargets[D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT] = { nullptr };
	D3D->devcon->OMSetRenderTargets(D3D11_SIMULTANEOUS_RENDER_TARGET_COUNT, nullRenderTargets, ArraySlices[slice]);
}