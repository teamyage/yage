#pragma once

#include "D3DSystem.h"
#include "System.h"
#include "Renderable.h"
#include <string.h>
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "VertexTypes.h"
#include "PixelShader.h"
#include "VertexShader.h"
#include "HullShader.h"
#include "DomainShader.h"
#include "ContentManager.h"
#include "Float2.h"

class HeightMapTerrain : public IRenderableSystem, public System, public BinaryLoader
{
public:
	HeightMapTerrain(std::string filename, Float2 dims);
	~HeightMapTerrain();

	int Render();
	void RenderZ();

	VertexBuffer<VertexPositionTexture> *m_vertexBuffer;
	IndexBuffer<USHORT> *m_indexBuffer;

	Texture *m_heightMap;

	ConstantBuffer<PerRenderableConstantBuffer> *m_perRenderableConstantBuffer;
	PixelShader *m_pixelShader;
	VertexShader<VertexPositionTexture> *m_vertexShader;
	HullShader *m_hullShader;
	DomainShader *m_domainShader;

	Texture *m_diffuse;

	ID3D11RasterizerState *m_rasterState;
};

