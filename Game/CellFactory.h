#pragma once

#include "boost/lockfree/queue.hpp"
#include "Cell.h"
#include "CellPool.h"
#include "boost/thread.hpp"

class CellFactory 
{
public:
	CellFactory();
	~CellFactory();

	void Request(CellId cid);

	//move this back down
	boost::lockfree::queue<Cell*> m_completedRequests;
private:
	void Loop();
	boost::thread_group m_workers;
	boost::lockfree::queue<CellId> m_requests;
	//here
	CellPool m_pool;
};
