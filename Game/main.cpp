#include "stdafx.h"

#include "Game.h"

int WINAPI WinMain(HINSTANCE applicationHandle,
				   HINSTANCE hPrevInstance,
				   LPSTR lpCmdLine,
				   int nCmdShow)
{
	Game game;

	game.G->applicationHandle = applicationHandle;
	game.G->nCmdShow = nCmdShow;

	game.Run();
}
