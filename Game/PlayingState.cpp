#include "stdafx.h"
#include "PlayingState.h"
#include "TiledForwardRenderer.h"

PlayingState::PlayingState()
{
	BOOST_LOG_TRIVIAL(trace) << "Construction of Playing";
}

PlayingState::~PlayingState()
{
	BOOST_LOG_TRIVIAL(trace) << "Destruction of Playing";
}

void PlayingState::Initialize()
{

}

void PlayingState::Render()
{
	G->m_renderer->RenderFrame(m_renderables, m_renderableSystems);
}

void PlayingState::Process()
{

}

void PlayingState::ProcessInput()
{

}