#pragma once

class CellId
{
public:
	bool operator==(const CellId &rhs) const
	{
		return position_x == rhs.position_x && position_y == rhs.position_y && position_z == rhs.position_z;
	}
	CellId()
	{

	}

	CellId(unsigned int x, unsigned int y, unsigned int z)
	{
		position_x = x;
		position_y = y;
		position_z = z;
	}

	CellId(const CellId& rhs) 
	{ 
		position_x = rhs.position_x;
		position_y = rhs.position_y;
		position_z = rhs.position_z;
	}
	unsigned int position_x, position_y, position_z;
};

struct CellIdHasher : std::unary_function<CellId, std::size_t>
{
	std::size_t operator()(CellId const &C) const
	{
		return ((size_t)C.position_x<<40)+((size_t)C.position_y<<24)+((size_t)C.position_z);
	}
};