#pragma once

#include "D3DSystem.h"

struct DSV : public D3DSystem
{
	DSV();

	void CreateDSV(ID3D11Resource *pResource, const D3D11_DEPTH_STENCIL_VIEW_DESC *pDesc);
	void Clear(UINT ClearFlags, FLOAT Depth, UINT8 Stencil);
//private:
	ID3D11DepthStencilView *m_DSV;

	friend struct RTV;
	friend struct DepthStencilBuffer;
};
