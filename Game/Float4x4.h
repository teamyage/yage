#pragma once

#include "stdafx.h"
#include "Float3.h"

using namespace DirectX;

struct Float3;

struct Float4x4 : public XMFLOAT4X4
{
	Float4x4();
	Float4x4(const XMFLOAT4X4& m);
	Float4x4(CXMMATRIX m);
		
	Float4x4& operator*=(const Float4x4& other);
	Float4x4 operator*(const Float4x4& other) const;
	
	Float3 Up() const;
	Float3 Down() const;
	Float3 Left() const;
	Float3 Right() const;
	Float3 Forward() const;
	Float3 Back() const;
	
	Float3 Translation() const;
	void SetTranslation(const Float3& t);
	void SetTranslation(const btVector3& t);

	void SetXBasis(const Float3& x);
	void SetYBasis(const Float3& y);
	void SetZBasis(const Float3& z);
		
	static Float4x4 Transpose(const Float4x4& m);
	static Float4x4 Invert(const Float4x4& m);
	static Float4x4 ScaleMatrix(float s);
	static Float4x4 ScaleMatrix(const Float3& s);
	static Float4x4 TranslationMatrix(const Float3& t);
	static Float4x4 Identity();
	
	XMMATRIX ToSIMD() const;
};

