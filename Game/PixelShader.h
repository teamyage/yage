#pragma once
#include "BinaryLoader.h"
#include "ShaderResource.h"
#include "ConstantBuffer.h"

class PixelShader : public BinaryLoader, public D3DSystem
{
public:
	PixelShader(std::string filename);
	~PixelShader();

	void Bind();
	static void Unbind();
	void Release();

private:
	//std::vector<ShaderResource> m_shaderResources;
	ID3D11ShaderResourceView *m_srv;
	ID3D11PixelShader *m_pixelShader;
};

