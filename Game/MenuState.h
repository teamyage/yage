#pragma once

#include "basegamestate.h"
#include "Cube.h"
#include "ContentManager.h"
#include "Model.h"
#include "MaterialLibrary.h"
#include "InputSystem.h"
#include "TiledForwardRenderer.h"
#include "ContentManager.h"
#include "VoxelTerrain.h"
#include "HeightMapTerrain.h" 
#include "axis.h"
#include "LightingSystem.h"
#include "PhysicsSystem.h"

class MenuState : public BaseGameState
{
public:
	MenuState();
	~MenuState();
	
	void Initialize();
	void Render();
	void Process();
	void ProcessInput();
};

