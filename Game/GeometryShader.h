#pragma once
#include "BinaryLoader.h"

class GeometryShader : public BinaryLoader, public D3DSystem
{
public:
	GeometryShader(std::string filename);
	~GeometryShader();

	void Bind();
	void Release();

private:
	ID3D11GeometryShader *m_geometryShader;
};

