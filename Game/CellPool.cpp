#include "stdafx.h"
#include "CellPool.h"

CellPool::CellPool() : m_pool(128)
{
}

CellPool::~CellPool()
{

}

Cell* CellPool::Request()
{
	Cell* result = NULL;

	if(!m_pool.pop(result))
	{
		//BOOST_LOG_TRIVIAL(trace) << "No cells in pool newing one!";
		result = new Cell();
	}
	//else
	{
		//BOOST_LOG_TRIVIAL(trace) << "Reusing Cell!";
	}

	return result;
}
void CellPool::Recycle(Cell* c)
{
	//BOOST_LOG_TRIVIAL(trace) << "Cell being recycled!";

	if(size < 100)
		while(!m_pool.push(c));
	else
		delete c;
}