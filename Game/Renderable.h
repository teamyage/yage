#pragma once
#include "System.h"
#include "d3dsystem.h"
#include "Vertex.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"
#include "PixelShader.h"
#include "VertexShader.h"
#include "Texture.h"
#include "ConstantBufferDefinitions.h"
#include "Timer.h"

class IRenderable : public D3DSystem
{
public:
	virtual int Render() = NULL;
	virtual void RenderZ() = NULL;
	virtual void UpdateConstantBuffer() = NULL;
	virtual PerRenderableConstantBuffer& GetConstantBuffer() = NULL;
	virtual D3D_PRIMITIVE_TOPOLOGY GetPrimitiveType() = NULL;
};

class IRenderableSystem : public D3DSystem
{
public:
	virtual int Render() = NULL;
	virtual void RenderZ() = NULL;
};

template<class V, class I>
class Renderable : public System, public IRenderable
{
public:
	Renderable()
	{
		m_vertexBuffer = NULL;
		m_indexBuffer = NULL;
		m_pixelShader = NULL;
		m_vertexShader = NULL;
		m_perRenderableConstantBuffer.Renderable_Translation = XMMatrixTranslation(0.0f,0.0f,0.0f);
		m_perRenderableConstantBuffer.Renderable_Scale = XMMatrixIdentity();
		m_perRenderableConstantBuffer.Renderable_Rotation = XMMatrixIdentity();
		m_primitiveType = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
		m_texture = NULL;
	}
	virtual int Render() = 0;
	virtual void RenderZ() = 0;
	virtual void UpdateConstantBuffer() {}
	PerRenderableConstantBuffer& GetConstantBuffer()
	{
		return m_perRenderableConstantBuffer;
	}

	D3D_PRIMITIVE_TOPOLOGY GetPrimitiveType()
	{
		return m_primitiveType;
	}

	void Release()
	{
		m_indexBuffer->Release();
		m_vertexBuffer->Release();
		m_pixelShader->Release();
		m_vertexShader->Release();
		m_texture->Release();
	}

	PixelShader *m_pixelShader;
	VertexShader<V> *m_vertexShader;	
	VertexBuffer<V> *m_vertexBuffer;
	IndexBuffer<I> *m_indexBuffer;	

protected:
	PerRenderableConstantBuffer m_perRenderableConstantBuffer;
	D3D_PRIMITIVE_TOPOLOGY m_primitiveType;
	Texture *m_texture;
};
