#pragma once
#include "d3dsystem.h"

template <class T>
class VertexBuffer : public D3DSystem
{
public:
	VertexBuffer(T* vertices, UINT numVertices)
	{
		Initialize(vertices, numVertices);
	}

	VertexBuffer(std::vector<T> &vertices)
	{		
		if(!vertices.empty())
			Initialize(&vertices[0],(UINT)vertices.size());
		else
			BOOST_LOG_TRIVIAL(trace) << "Attempted creation of empty VertexBuffer";
	}

	~VertexBuffer()
	{
		SAFE_RELEASE_DELETE(m_buffer);
	}

	void Bind()
	{
		UINT offset = 0;
		D3D->devcon->IASetVertexBuffers(0, 1, &m_buffer, &m_stride, &offset);
	}

	void Release()
	{
		SAFE_RELEASE(m_buffer);
	}

	UINT getNumVertices()
	{
		return m_numVertices;
	}
	
private:
	void Initialize(T* vertices, UINT numvertices)
	{		
		m_stride = sizeof(T);
		m_numVertices = numvertices;

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_DYNAMIC;
		bd.ByteWidth = (UINT)(sizeof(T) * m_numVertices);
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		D3D->dev->CreateBuffer(&bd, NULL, &m_buffer);

		D3D11_MAPPED_SUBRESOURCE ms;
		D3D->devcon->Map(m_buffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms);
		memcpy(ms.pData, &vertices[0], m_numVertices*sizeof(T));
		D3D->devcon->Unmap(m_buffer, NULL);
	}

	UINT m_stride;
	UINT m_numVertices;
	ID3D11Buffer *m_buffer;

	VertexBuffer(VertexBuffer const&);
	VertexBuffer& operator= (VertexBuffer const&);
};

