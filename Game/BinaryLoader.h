#pragma once

#include "BaseContent.h"
#include "D3DSystem.h"
#include "Macros.h"

class BinaryLoader : public BaseContent
{
public:
	BinaryLoader(std::string filename);
	virtual ~BinaryLoader();

protected:
	char* m_data;
	size_t m_dataLength;
	std::string m_filename;
};

