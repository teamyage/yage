#pragma once

#include "System.h"
#include "Renderable.h"
#include "Processable.h"

enum GameState
{
	menu=0,
	playing=1
};

class BaseGameState : public System
{
public:
	BaseGameState();
	virtual ~BaseGameState();

	virtual void Initialize() = NULL;
	virtual void Render() = NULL;
	virtual void Process() = NULL;
	virtual void ProcessInput() = NULL;
	void ProcessSystems();

	std::vector<IRenderable *> m_renderables;
	std::vector<IRenderableSystem *> m_renderableSystems;
	std::vector<IProcessableSystem *> m_processableSystems;
};

