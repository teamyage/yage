#include "stdafx.h"
#include "ComputeShader.h"

ComputeShader::ComputeShader(std::string filename) : BinaryLoader(filename)
{
	if(m_data)
	{
		BOOST_LOG_TRIVIAL(trace) << "Creating ComputeShader for " << m_filename;
		HRESULT hr = D3D->dev->CreateComputeShader(m_data, m_dataLength, nullptr, &m_computeShader);

		if (FAILED(hr)) 
			BOOST_LOG_TRIVIAL(error) << "Error while creating ComputeShader: " << m_filename;
	}
}


ComputeShader::~ComputeShader()
{
	BOOST_LOG_TRIVIAL(trace) << "Releasing ComputeShader for " << m_filename;
	SAFE_RELEASE(m_computeShader);
}

void ComputeShader::Bind()
{
	D3D->devcon->CSSetShader(m_computeShader, 0, 0);
}

void ComputeShader::Release()
{
	m_computeShader->Release();
}

void ComputeShader::Dispatch(unsigned int x, unsigned int y, unsigned int z)
{
	D3D->devcon->Dispatch(x,y,z);
}