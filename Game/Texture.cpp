#include "stdafx.h"
#include "Texture.h"

using namespace DirectX;

Texture::Texture(std::string filename)
{
	m_filename = filename;

	BOOST_LOG_TRIVIAL(trace) << "Creating Texture for " << m_filename;

	HRESULT hr = CreateDDSTextureFromFile( D3D->dev, std::wstring(filename.begin(), filename.end()).c_str(), nullptr, &m_srv );

	if (FAILED(hr)) 
	{
		BOOST_LOG_TRIVIAL(warning) << "Error while loading texture " << m_filename << " attempting to load as WIC type!";
		hr = CreateWICTextureFromFile( D3D->dev, D3D->devcon, std::wstring(filename.begin(), filename.end()).c_str(), nullptr, &m_srv );
		if(FAILED(hr))
			BOOST_LOG_TRIVIAL(error) << "Could not load " << m_filename;
		else
			BOOST_LOG_TRIVIAL(info) << "Success loading previously errored texture " << m_filename;
	}	
}

Texture::~Texture()
{
	BOOST_LOG_TRIVIAL(trace) << "Releasing Texture for " << m_filename;
	SAFE_RELEASE(m_srv);
}

void Texture::Bind(ShaderTypes shaderType, UINT startslot)
{
	switch(shaderType){
	case VERTEXSHADER:
		D3D->devcon->VSSetShaderResources(startslot, 1, &m_srv);
		break;
	case PIXELSHADER:
		D3D->devcon->PSSetShaderResources(startslot, 1, &m_srv);
		break;
	case GEOMETRYSHADER:
		D3D->devcon->GSSetShaderResources(startslot, 1, &m_srv);
		break;
	case HULLSHADER:
		D3D->devcon->HSSetShaderResources(startslot, 1, &m_srv);
		break;
	case COMPUTESHADER:
		D3D->devcon->CSSetShaderResources(startslot, 1, &m_srv);
		break;
	case DOMAINSHADER:
		D3D->devcon->DSSetShaderResources(startslot, 1, &m_srv);
		break;
	default:
		BOOST_LOG_TRIVIAL(error) << "Attempted Binding of Texture to non-existant ShaderType";
	}	
}

void Texture::Release()
{
	m_srv->Release();
}