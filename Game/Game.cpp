#include "stdafx.h"
#include "Game.h"

Game::Game()
{
	BOOST_LOG_TRIVIAL(trace) << "Construction of Game";
	
	m_gameStates[menu] = new MenuState();
	m_gameStates[playing] = new PlayingState();

	setCurrentGameState(menu);
}

Game::~Game()
{
	BOOST_LOG_TRIVIAL(trace) << "Destruction of Game";
}

void Game::Initialize()
{
	BOOST_LOG_TRIVIAL(trace) << "Entering Game Initialization";

	G->m_renderer = new TiledForwardRenderer();

	m_gameStates[menu]->Initialize();
	m_gameStates[playing]->Initialize();

	BOOST_LOG_TRIVIAL(trace) << "Exiting Game Initialization";
}

void Game::Loop()
{	
}

void Game::Release()
{
	BOOST_LOG_TRIVIAL(trace) << "Entering Game Release";

	SAFE_DELETE(m_gameStates[menu]);
	SAFE_DELETE(m_gameStates[playing]);
	SAFE_DELETE(G->m_renderer);

	BOOST_LOG_TRIVIAL(trace) << "Exiting Game Release";
}