#pragma once

#include "System.h"
#include "InputSystem.h"
#include "values.h"

class WindowManager : public System
{
public:	
	WindowManager();
	~WindowManager();

	HWND getWindowHandle();
	int getWidth();
	int getHeight();
	int getNumTilesWidth();
	int getNumTilesHeight();
	float getAspectRatio();
	
private:
	static LRESULT CALLBACK ReDirectWndProc(HWND hWnd,UINT uMsg, WPARAM wParam, LPARAM lParam);	

	static InputSystem *m_inputSystem;
	HWND windowHandle;
	int	m_width, m_height;
	int m_screenwidth, m_screenheight;
};

