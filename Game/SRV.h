#pragma once

#include "D3DSystem.h"
#include "ShaderTypes.h"

struct SRV : public D3DSystem
{
	SRV();	
	static void BindNULLSRV(ShaderTypes shaderType, UINT StartSlot, UINT NumViews);
	void BindSRV(ShaderTypes shaderType, UINT slot);
	void CreateSRV(ID3D11Resource *resource, const D3D11_SHADER_RESOURCE_VIEW_DESC *pDesc=NULL);
	//private: TODO
	ID3D11ShaderResourceView *m_SRV;

	friend class ComputeShader;
};
