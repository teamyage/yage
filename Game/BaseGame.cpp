#include "stdafx.h"
#include "BaseGame.h"

BaseGame::BaseGame()
{
	m_renderer = NULL;

	Logger::InitializeLogger("trace");
	BOOST_LOG_TRIVIAL(trace) << "Construction of BaseGame";
	G = new Globals();	
	D3D = new D3DGlobals();
}

BaseGame::~BaseGame()
{	
	SAFE_DELETE(G);
	SAFE_DELETE(D3D);
	BOOST_LOG_TRIVIAL(trace) << "Destruction of BaseGame";
}

void BaseGame::setCurrentGameState(GameState state)
{
	m_currentGameState = state;
}

void BaseGame::Run()
{
	InitializeEngine();
	Initialize();

	// this struct holds Windows event messages
	MSG msg = {0};

	// Enter the infinite message loop
	while(TRUE)
	{
		// Check to see if any messages are waiting in the queue
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			// translate keystroke messages into the right format
			TranslateMessage(&msg);

			// send the message to the WindowProc function
			DispatchMessage(&msg);

			// check to see if it's time to quit
			if(msg.message == WM_QUIT)
				break;
		}
		else
		{
			LoopEngine();
			Loop();	
		}		
	}
	
	Release();
	ReleaseEngine();
}

void BaseGame::InitializeEngine()
{
	G->m_timer = new Timer();
	G->m_inputSystem = new InputSystem();
	G->m_windowManager = new WindowManager();
	G->m_inputSystem->SetupRawInput();
	G->m_contentManager = new ContentManager();
	G->m_camera = new FirstPersonCamera();
}

void BaseGame::LoopEngine()
{
	G->m_timer->Update();

	m_gameStates[m_currentGameState]->ProcessInput();
	m_gameStates[m_currentGameState]->Render();
	m_gameStates[m_currentGameState]->Process();
	m_gameStates[m_currentGameState]->ProcessSystems();
}

void BaseGame::ReleaseEngine()
{
	SAFE_DELETE(G->m_camera);
	SAFE_DELETE(G->m_timer);
	SAFE_DELETE(G->m_inputSystem);
	SAFE_DELETE(G->m_contentManager);
	SAFE_DELETE(G->m_windowManager);
}

