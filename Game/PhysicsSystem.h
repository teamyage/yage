#pragma once

#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"
#include "Processable.h"
#include "Timer.h"
#include "System.h"

class PhysicsSystem : public IProcessableSystem, public System
{
public:
	PhysicsSystem();
	void Process();

	btDiscreteDynamicsWorld* m_dynamicsWorld;
};

