#include "stdafx.h"
#include "Float4.h"

Float4::Float4()
{
	x = y = z = w = 0.0f;
}

Float4::Float4(float x_)
{
	x = y = z = w = x_;
}

Float4::Float4(float x_, float y_, float z_, float w_)
{
	x = x_;
	y = y_;
	z = z_;
	w = w_;
}

Float4::Float4(const XMFLOAT3& xyz, float w_)
{
	x = xyz.x;
	y = xyz.y;
	z = xyz.z;
	w = w_;
}

Float4::Float4(const XMFLOAT4& xyzw)
{
	x = xyzw.x;
	y = xyzw.y;
	z = xyzw.z;
	w = xyzw.w;
}

Float4::Float4(FXMVECTOR xyzw)
{
	XMStoreFloat4(this, xyzw);
}

Float4& Float4::operator+=(const Float4& other)
{
	x += other.x;
	y += other.y;
	z += other.z;
	w += other.w;
	return *this;
}

Float4 Float4::operator+(const Float4& other) const
{
	Float4 result;
	result.x = x + other.x;
	result.y = y + other.y;
	result.z = z + other.z;
	result.w = w + other.w;
	return result;
}

Float4& Float4::operator-=(const Float4& other)
{
	x -= other.x;
	y -= other.y;
	z -= other.z;
	w -= other.w;
	return *this;
}

Float4 Float4::operator-(const Float4& other) const
{
	Float4 result;
	result.x = x - other.x;
	result.y = y - other.y;
	result.z = z - other.z;
	result.w = w - other.w;
	return result;
}

Float4& Float4::operator*=(const Float4& other)
{
	x *= other.x;
	y *= other.y;
	z *= other.z;
	w *= other.w;
	return *this;
}

Float4 Float4::operator*(const Float4& other) const
{
	Float4 result;
	result.x = x * other.x;
	result.y = y * other.y;
	result.z = z * other.z;
	result.w = w * other.w;
	return result;
}

Float4& Float4::operator/=(const Float4& other)
{
	x /= other.x;
	y /= other.y;
	z /= other.z;
	w /= other.w;
	return *this;
}

Float4 Float4::operator/(const Float4& other) const
{
	Float4 result;
	result.x = x / other.x;
	result.y = y / other.y;
	result.z = z / other.z;
	result.w = w / other.w;
	return result;
}

bool Float4::operator==(const Float4& other) const
{
	return x == other.x && y == other.y && z == other.z && w == other.w;
}

bool Float4::operator!=(const Float4& other) const
{
	return x != other.x || y != other.y || z != other.z || w != other.w;
}


Float4 Float4::operator-() const
{
	Float4 result;
	result.x = -x;
	result.y = -y;
	result.z = -z;
	result.w = -w;

	return result;
}

XMVECTOR Float4::ToSIMD() const
{
	return XMLoadFloat4(this);
}

Float3 Float4::To3D() const
{
	return Float3(x, y, z);
}

Float4 Float4::Clamp(const Float4& val, const Float4& min, const Float4& max)
{
	Float4 retVal;
	retVal.x = glm::clamp(val.x, min.x, max.x);
	retVal.y = glm::clamp(val.y, min.y, max.y);
	retVal.z = glm::clamp(val.z, min.z, max.z);
	retVal.w = glm::clamp(val.w, min.w, max.w);
	return retVal;
}