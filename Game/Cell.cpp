#include "stdafx.h"
#include "Cell.h"

Cell::Cell()
{
	m_vertexBuffer = NULL;
	m_perRenderableConstantBuffer.Renderable_Scale = XMMatrixIdentity();
	m_perRenderableConstantBuffer.Renderable_Rotation = XMMatrixIdentity();
	m_perRenderableConstantBuffer.Renderable_Translation = XMMatrixTranslation(0.0f,0.0f,0.0f);
	cellWidth = XMLoadUInt3(&XMUINT3(CELLWIDTH,CELLWIDTH,CELLWIDTH));

	data = new BYTE**[CELLWIDTHP1];
	for(int x=0;x<CELLWIDTHP1;x++)
	{
		data[x] = new BYTE*[CELLWIDTHP1];
		for(int j=0;j<CELLWIDTHP1;j++)
			data[x][j] = new BYTE[CELLWIDTHP1];
	}
	polycount = 0;
	verts.reserve(55000);

#pragma region //OFFSETS
	Xp1 = Float3( 1.0f,  0.0f,  0.0f).ToSIMD();
	Xn1 = Float3(-1.0f,  0.0f,  0.0f).ToSIMD();
	Yp1 = Float3( 0.0f,  1.0f,  0.0f).ToSIMD();
	Yn1 = Float3( 0.0f, -1.0f,  0.0f).ToSIMD();
	Zp1 = Float3( 0.0f,  0.0f,  1.0f).ToSIMD();
	Zn1 = Float3( 0.0f,  0.0f, -1.0f).ToSIMD();

	offsets[XP][0] = Float3(1.0f, 0.0f, 0.0f).ToSIMD();
	offsets[XP][1] = Float3(1.0f, 0.0f, 1.0f).ToSIMD();
	offsets[XP][2] = Float3(1.0f, 1.0f, 1.0f).ToSIMD();
	offsets[XP][3] = Float3(1.0f, 0.0f, 0.0f).ToSIMD();
	offsets[XP][4] = Float3(1.0f, 1.0f, 1.0f).ToSIMD();
	offsets[XP][5] = Float3(1.0f, 1.0f, 0.0f).ToSIMD();

	offsets[XN][0] = Float3(1.0f, 0.0f, 0.0f).ToSIMD();
	offsets[XN][1] = Float3(1.0f, 1.0f, 1.0f).ToSIMD();
	offsets[XN][2] = Float3(1.0f, 0.0f, 1.0f).ToSIMD();
	offsets[XN][3] = Float3(1.0f, 0.0f, 0.0f).ToSIMD();
	offsets[XN][4] = Float3(1.0f, 1.0f, 0.0f).ToSIMD();
	offsets[XN][5] = Float3(1.0f, 1.0f, 1.0f).ToSIMD();

	offsets[YP][0] = Float3(0.0f, 1.0f, 0.0f).ToSIMD();
	offsets[YP][1] = Float3(1.0f, 1.0f, 1.0f).ToSIMD();
	offsets[YP][2] = Float3(0.0f, 1.0f, 1.0f).ToSIMD();	
	offsets[YP][3] = Float3(0.0f, 1.0f, 0.0f).ToSIMD();
	offsets[YP][4] = Float3(1.0f, 1.0f, 0.0f).ToSIMD();
	offsets[YP][5] = Float3(1.0f, 1.0f, 1.0f).ToSIMD();	

	offsets[YN][0] = Float3(0.0f, 1.0f, 0.0f).ToSIMD();
	offsets[YN][1] = Float3(0.0f, 1.0f, 1.0f).ToSIMD();
	offsets[YN][2] = Float3(1.0f, 1.0f, 1.0f).ToSIMD();
	offsets[YN][3] = Float3(0.0f, 1.0f, 0.0f).ToSIMD();
	offsets[YN][4] = Float3(1.0f, 1.0f, 1.0f).ToSIMD();
	offsets[YN][5] = Float3(1.0f, 1.0f, 0.0f).ToSIMD();		

	offsets[ZP][0] = Float3(0.0f, 0.0f, 1.0f).ToSIMD();
	offsets[ZP][1] = Float3(0.0f, 1.0f, 1.0f).ToSIMD();
	offsets[ZP][2] = Float3(1.0f, 1.0f, 1.0f).ToSIMD();
	offsets[ZP][3] = Float3(0.0f, 0.0f, 1.0f).ToSIMD();
	offsets[ZP][4] = Float3(1.0f, 1.0f, 1.0f).ToSIMD();
	offsets[ZP][5] = Float3(1.0f, 0.0f, 1.0f).ToSIMD();		

	offsets[ZN][0] = Float3(0.0f, 0.0f, 1.0f).ToSIMD();
	offsets[ZN][1] = Float3(1.0f, 1.0f, 1.0f).ToSIMD();
	offsets[ZN][2] = Float3(0.0f, 1.0f, 1.0f).ToSIMD();
	offsets[ZN][3] = Float3(0.0f, 0.0f, 1.0f).ToSIMD();
	offsets[ZN][4] = Float3(1.0f, 0.0f, 1.0f).ToSIMD();
	offsets[ZN][5] = Float3(1.0f, 1.0f, 1.0f).ToSIMD();
#pragma endregion
}

Cell::~Cell()
{
	SAFE_RELEASE_DELETE(m_vertexBuffer);
		
	for(int x=0;x<CELLWIDTHP1;x++)
	{		
		for(int j=0;j<CELLWIDTHP1;j++)
			delete data[x][j];
		delete data[x];
	}
	delete data;
}

bool Cell::Generate()
{	
	unsigned int posx = m_cid.position_x*CELLWIDTH;
	unsigned int posy = m_cid.position_y*CELLWIDTH;
	unsigned int posz = m_cid.position_z*CELLWIDTH;

	for(int x=0;x<CELLWIDTHP1;x++)
	{
		double worldx = posx+x;
		for(int z=0;z<CELLWIDTHP1;z++)
		{
			double worldz = posz+z;
			double heightAtXZ = 64*(1+glm::perlin(glm::vec2(worldx/60.0, worldz/60.0)))/2.0;
			for(int y=0;y<CELLWIDTHP1;y++)
			{
				BYTE &d = data[x][y][z];
				d = (posy+y<heightAtXZ)?1:0;
			}	
		}
	}
	
//disregard int to float loss of data warning
#pragma warning( push )
#pragma warning( disable : 4244 )

	for(int x=0;x<CELLWIDTH;x++)
	for(int y=0;y<CELLWIDTH;y++)
	for(int z=0;z<CELLWIDTH;z++)
	{
		BYTE& here = data[x][y][z];
		BYTE& nextX = data[x+1][y][z];
		BYTE& nextY = data[x][y+1][z];
		BYTE& nextZ = data[x][y][z+1];
		if(hasDensityChange(here, nextX))
		{
			if(here==0)
				for(unsigned int i=0;i<6;i++)
					verts.push_back(VertexPositionNormal(Float3(x, y, z).ToSIMD()+offsets[XP][i], Xn1));
			else
				for(unsigned int i=0;i<6;i++)
					verts.push_back(VertexPositionNormal(Float3(x, y, z).ToSIMD()+offsets[XN][i], Xp1));
		}
		if(hasDensityChange(here, nextY))
		{
			if(here==0)
				for(unsigned int i=0;i<6;i++)
					verts.push_back(VertexPositionNormal(Float3(x, y, z).ToSIMD()+offsets[YP][i], Yn1));
			else
				for(unsigned int i=0;i<6;i++)
					verts.push_back(VertexPositionNormal(Float3(x, y, z).ToSIMD()+offsets[YN][i], Yp1));
		}		
		if(hasDensityChange(here, nextZ))
		{
			if(here==0)
				for(unsigned int i=0;i<6;i++)
					verts.push_back(VertexPositionNormal(Float3(x, y, z).ToSIMD()+offsets[ZP][i], Zn1));
			else
				for(unsigned int i=0;i<6;i++)
					verts.push_back(VertexPositionNormal(Float3(x, y, z).ToSIMD()+offsets[ZN][i], Zp1));
		}
	}


	if(!verts.empty())
	{
		polycount = (int)verts.size()/3;

		Float3 celloffset = 
			Float3(
				m_cid.position_x*CELLWIDTH, 
				m_cid.position_y*CELLWIDTH, 
				m_cid.position_z*CELLWIDTH
			);
		
		btTriangleMesh* ptrimesh = new btTriangleMesh();

		for (UINT i=0; i < verts.size(); i+=3) 
			ptrimesh->addTriangle(
				Utils::convert(celloffset+verts[i].position), 
				Utils::convert(celloffset+verts[i+1].position), 
				Utils::convert(celloffset+verts[i+2].position)
			);

		btBvhTriangleMeshShape *shape = 
					new btBvhTriangleMeshShape(ptrimesh,true,true);

		btRigidBody::btRigidBodyConstructionInfo rigidBodyCI(0, NULL, shape);

		m_rb = new btRigidBody(rigidBodyCI);
	}

#pragma warning( pop ) 	
	
	//this cell needs recycling if its empty
	return verts.empty();
}

void Cell::UpdateConstantBuffer()
{

}

void Cell::Render()
{
	if(polycount>0)
	{
		m_vertexBuffer->Bind();
		D3D->devcon->Draw(m_vertexBuffer->getNumVertices(), 0);
	}
}

PerRenderableConstantBuffer& Cell::GetConstantBuffer()
{
	return m_perRenderableConstantBuffer;
}

void Cell::SetCellId(CellId cid)
{
	XMUINT3 position = XMUINT3(cid.position_x, cid.position_y, cid.position_z);

	m_perRenderableConstantBuffer.Renderable_Translation = XMMatrixTranslationFromVector(cellWidth*XMLoadUInt3(&position));
	m_cid = cid;
}

inline bool Cell::hasDensityChange(BYTE A,BYTE B)
{
	return (A^B)!=0;
}
CellId Cell::getId()
{
	return m_cid;
}
void Cell::PrepareForRendering()
{
	if(!verts.empty())
	{
		G->m_physicsSystem->m_dynamicsWorld->addRigidBody(m_rb);
		m_vertexBuffer = new VertexBuffer<VertexPositionNormal>(verts);
	}
	verts.clear();	
}