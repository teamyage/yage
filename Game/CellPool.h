#pragma once

#include "Cell.h"
#include "boost/lockfree/queue.hpp"

class CellPool
{
public:
	CellPool();
	~CellPool();

	Cell* Request();
	void Recycle(Cell* c);

private:
	boost::lockfree::queue<Cell*> m_pool;
	std::atomic<unsigned int> size;
};