#pragma once

#include "stdafx.h"
#include "Float3.h"

using namespace DirectX;

struct Float3;

struct Float3x3 : public XMFLOAT3X3
{
	Float3x3();
	Float3x3(const XMFLOAT3X3& m);
	Float3x3(CXMMATRIX m);
	
	Float3x3& operator*=(const Float3x3& other);
	Float3x3 operator*(const Float3x3& other) const;
	
	Float3 Up() const;
	Float3 Down() const;
	Float3 Left() const;
	Float3 Right() const;
	Float3 Forward() const;
	Float3 Back() const;
	
	void SetXBasis(const Float3& x);
	void SetYBasis(const Float3& y);
	void SetZBasis(const Float3& z);
	
	static Float3x3 Transpose(const Float3x3& m);
	static Float3x3 Invert(const Float3x3& m);
	static Float3x3 ScaleMatrix(float s);
	static Float3x3 ScaleMatrix(const Float3& s);
	
	XMMATRIX ToSIMD() const;
};

