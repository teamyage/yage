#include "stdafx.h"
#include "MaterialLibrary.h"
#include "Material.h"
#include "Texture.h"
#include "PixelShader.h"
#include "VertexShader.h"
#include "ContentManager.h"

using namespace boost::property_tree;

MaterialLibrary::MaterialLibrary(std::string filename) : XMLLoader(filename)
{
	BOOST_FOREACH( ptree::value_type const& materialElement, m_propertyTree.get_child("Materials")) 
	{
		std::string materialName = materialElement.second.get<std::string>("<xmlattr>.name", "NO_MATERIAL_NAME_SET");
		BOOST_LOG_TRIVIAL(trace) << "MaterialLibary Loading: " << materialName;
		Material *material = new Material(materialName);		
		
		material->m_diffuse = G->m_contentManager->load<Texture>(materialElement.second.get<std::string>("Textures.Diffuse", "Textures\\NULL_DIFFUSE.dds"));
		material->m_normal = G->m_contentManager->load<Texture>(materialElement.second.get<std::string>("Textures.Normal", "Textures\\NULL_NORMAL.dds"));
		material->m_specular = G->m_contentManager->load<Texture>(materialElement.second.get<std::string>("Textures.Specular", "Textures\\NULL_SPECULAR.dds"));
		material->m_pixelShader = G->m_contentManager->load<PixelShader>(materialElement.second.get<std::string>("Shaders.PixelShader", "Shaders\\PixelShader.cso"));

		std::string vertexShaderParamType = materialElement.second.get<std::string>("Shaders.VertexShader.<xmlattr>.type", "VertexPositionNormalTangentColorTexture");

		if(vertexShaderParamType == "VertexPositionNormalTangentColorTexture")
			material->m_vertexShader = G->m_contentManager->load<VertexShader<VertexPositionNormalTangentColorTexture>>(materialElement.second.get<std::string>("Shaders.VertexShader", "Shaders\\VertexShader.cso"));
		
		else if (vertexShaderParamType == "VertexPositionNormalTangentColorTextureSkinning")
			material->m_vertexShader = G->m_contentManager->load<VertexShader<VertexPositionNormalTangentColorTextureSkinning>>(materialElement.second.get<std::string>("Shaders.VertexShader", "Shaders\\VertexShader.cso"));

		else if (vertexShaderParamType == "VertexPositionColor")
			material->m_vertexShader = G->m_contentManager->load<VertexShader<VertexPositionColor>>(materialElement.second.get<std::string>("Shaders.VertexShader", "Shaders\\VertexShader.cso"));

		else if (vertexShaderParamType == "VertexPositionTexture")
			material->m_vertexShader = G->m_contentManager->load<VertexShader<VertexPositionTexture>>(materialElement.second.get<std::string>("Shaders.VertexShader", "Shaders\\VertexShader.cso"));

		else if (vertexShaderParamType == "VertexPositionNormal")
			material->m_vertexShader = G->m_contentManager->load<VertexShader<VertexPositionNormal>>(materialElement.second.get<std::string>("Shaders.VertexShader", "Shaders\\VertexShader.cso"));

		else if (vertexShaderParamType == "VertexPositionColorTexture")
			material->m_vertexShader = G->m_contentManager->load<VertexShader<VertexPositionColorTexture>>(materialElement.second.get<std::string>("Shaders.VertexShader", "Shaders\\VertexShader.cso"));

		else if (vertexShaderParamType == "VertexPositionNormalColor")
			material->m_vertexShader = G->m_contentManager->load<VertexShader<VertexPositionNormalColor>>(materialElement.second.get<std::string>("Shaders.VertexShader", "Shaders\\VertexShader.cso"));

		else if (vertexShaderParamType == "VertexPositionNormalTexture")
			material->m_vertexShader = G->m_contentManager->load<VertexShader<VertexPositionNormalTexture>>(materialElement.second.get<std::string>("Shaders.VertexShader", "Shaders\\VertexShader.cso"));

		else if (vertexShaderParamType == "VertexPositionNormalColorTexture")
			material->m_vertexShader = G->m_contentManager->load<VertexShader<VertexPositionNormalColorTexture>>(materialElement.second.get<std::string>("Shaders.VertexShader", "Shaders\\VertexShader.cso"));
		
		else
			BOOST_LOG_TRIVIAL(error) << "Unhandled Vertex Shader Vertex Type: " << vertexShaderParamType;

		m_materials[materialName] = material;
	}
}

MaterialLibrary::~MaterialLibrary()
{

}
