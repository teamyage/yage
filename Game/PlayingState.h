#pragma once

#include "basegamestate.h"

class PlayingState : public BaseGameState
{
public:
	PlayingState();
	~PlayingState();

	void Initialize();
	void Render();
	void Process();
	void ProcessInput();
};

