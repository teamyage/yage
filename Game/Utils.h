#pragma once

#include "stdafx.h"
#include "Float4.h"

class Utils
{
public:
	static float random01();
	static DirectX::XMMATRIX convert(const btTransform& transform);
	static btVector3 convert(const DirectX::XMFLOAT3 &v);
	static Quaternion convert(const btQuaternion &q);
};

