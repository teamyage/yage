#include "stdafx.h"
#include "DSV.h"

DSV::DSV() : m_DSV(NULL) {}

void DSV::CreateDSV(ID3D11Resource *pResource, const D3D11_DEPTH_STENCIL_VIEW_DESC *pDesc)
{
	if (FAILED(D3D->dev->CreateDepthStencilView(pResource, pDesc, &m_DSV))) 
		BOOST_LOG_TRIVIAL(error) << "Error while creating the DSV";
}

void DSV::Clear(UINT ClearFlags, FLOAT Depth, UINT8 Stencil)
{
	D3D->devcon->ClearDepthStencilView(m_DSV, ClearFlags, Depth, Stencil);
}