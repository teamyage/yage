#include "stdafx.h"
#include "CellFactory.h"

CellFactory::CellFactory() : m_requests(2048), m_completedRequests(2048)
{
	for(unsigned int i=0;i<boost::thread::hardware_concurrency()-1;i++)
	{
		boost::thread t(boost::bind(&CellFactory::Loop, this));
		m_workers.add_thread(&t);
	}
}

CellFactory::~CellFactory()
{
}

void CellFactory::Request(CellId cid)
{
	while(!m_requests.push(cid));
}

void CellFactory::Loop()
{
	while(true)
	{
		CellId cid;
		if(m_requests.pop(cid))
		{
			Cell* c = m_pool.Request();
			c->SetCellId(cid);
			bool needsrecycled = c->Generate();

			if(needsrecycled)
			{
				m_pool.Recycle(c);
			}
			else
				while(!m_completedRequests.push(c));
		}
		else
			boost::this_thread::sleep(boost::posix_time::milliseconds(1));

		boost::this_thread::interruption_point();
	}
}