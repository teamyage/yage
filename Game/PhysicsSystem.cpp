#include "stdafx.h"
#include "PhysicsSystem.h"

PhysicsSystem::PhysicsSystem()
{
	// Build the broadphase
	btBroadphaseInterface* broadphase = 
							new btDbvtBroadphase();

	// Set up the collision configuration and dispatcher
	btDefaultCollisionConfiguration* collisionConfiguration = 
							new btDefaultCollisionConfiguration();

	btCollisionDispatcher* dispatcher = 
							new btCollisionDispatcher(collisionConfiguration);

	// The actual physics solver
	btSequentialImpulseConstraintSolver* solver = 
							new btSequentialImpulseConstraintSolver;

	// The world.
	m_dynamicsWorld =		new btDiscreteDynamicsWorld(
									dispatcher,
									broadphase,
									solver,
									collisionConfiguration);

	m_dynamicsWorld->setGravity(btVector3(0,-9.81f,0));
}

void PhysicsSystem::Process()
{
	m_dynamicsWorld->stepSimulation(
						(float)G->m_timer->getLastFrameLength(), 1);
}




