#include "stdafx.h"
#include "RTV.h"

RTV::RTV() : m_RTV(NULL) {}

void RTV::BindRTV(UINT NumViews, ID3D11RenderTargetView *const *ppRenderTargetViews, DSV *dsv)
{
	D3D->devcon->OMSetRenderTargets(NumViews, ppRenderTargetViews, dsv->m_DSV);
}

void RTV::BindRTV(ID3D11RenderTargetView *const *ppRenderTargetView, DSV *dsv)
{
	if(ppRenderTargetView == NULL)
		BOOST_LOG_TRIVIAL(error) << "NULL passed as rtv, please specify 0 views!";
	D3D->devcon->OMSetRenderTargets(1, ppRenderTargetView, dsv->m_DSV);
}

void RTV::BindRTV(DSV *dsv)
{
	D3D->devcon->OMSetRenderTargets(1, &m_RTV, dsv->m_DSV);
}

void RTV::BindNULLRTV(UINT NumViews)
{
	ID3D11RenderTargetView **nullrtv = new ID3D11RenderTargetView*[NumViews];

	for(UINT i=0;i<NumViews;i++)
		nullrtv[i] = NULL;

	D3D->devcon->OMSetRenderTargets(NumViews, nullrtv, NULL);

	for(UINT i=0;i<NumViews;i++)
		delete nullrtv[i];

	delete nullrtv;

}

void RTV::CreateRTV(ID3D11Resource *pResource, const D3D11_RENDER_TARGET_VIEW_DESC *pDesc)
{
	if(FAILED(D3D->dev->CreateRenderTargetView(pResource, pDesc, &m_RTV)))
		BOOST_LOG_TRIVIAL(error) << "Error while creating RTV";
}
