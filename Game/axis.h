#pragma once

#include "renderable.h"
#include "VertexTypes.h"
#include "ContentManager.h"
#include "PixelShader.h"
#include "VertexShader.h"
#include "Texture.h"

using namespace DirectX;
using namespace DirectX::Colors;

#define VERTEXTYPE_AXIS VertexPositionColor
#define INDEXTYPE_AXIS USHORT
#define INDEXFORMAT_AXIS DXGI_FORMAT_R16_UINT

#define AXIS_LENGTH 10.0f
#define LETTER_SIZE 0.5f
#define AXIS_X_COLOR Red
#define AXIS_Y_COLOR Green
#define AXIS_Z_COLOR Blue

class Axis : public Renderable<VERTEXTYPE_AXIS, INDEXTYPE_AXIS>
{
public:
	Axis();
	~Axis();

	int Render();
	void RenderZ();
};