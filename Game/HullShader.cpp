#include "stdafx.h"
#include "HullShader.h"

HullShader::HullShader(std::string filename) : BinaryLoader(filename)
{
	if(m_data)
	{
		BOOST_LOG_TRIVIAL(trace) << "Creating HullShader for " << m_filename;
		HRESULT hr = D3D->dev->CreateHullShader(m_data, m_dataLength, nullptr, &m_hullShader);

		if (FAILED(hr)) 
			BOOST_LOG_TRIVIAL(error) << "Error while creating HullShader: " << m_filename;
	}
}


HullShader::~HullShader()
{
	BOOST_LOG_TRIVIAL(trace) << "Releasing HullShader for " << m_filename;
	SAFE_RELEASE(m_hullShader);
}

void HullShader::Bind()
{
	D3D->devcon->HSSetShader(m_hullShader, 0, 0);
}

void HullShader::Unbind()
{
	D3D->devcon->HSSetShader(NULL, 0, 0);
}

void HullShader::Release()
{
	m_hullShader->Release();
}