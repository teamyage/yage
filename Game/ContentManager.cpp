#include "stdafx.h"
#include "ContentManager.h"

ContentManager::ContentManager()
{
	BOOST_LOG_TRIVIAL(trace) << "Construction of ContentManager";
}

ContentManager::~ContentManager()
{
	BOOST_LOG_TRIVIAL(trace) << "Destruction of ContentManager";

	BOOST_LOG_TRIVIAL(trace) << "Releasing content";
	for(auto c : content)
		delete c.second;
	content.empty();

	BOOST_LOG_TRIVIAL(trace) << "Content Released";
}